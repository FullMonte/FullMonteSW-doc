var structLineFixture =
[
    [ "LineFixture", "structLineFixture.html#abe9f2bfb96c02609c95339bef58dd047", null ],
    [ "LineFixture", "structLineFixture.html#a5eb46be315a84fbca84f7e530fea8c9d", null ],
    [ "testPosition", "structLineFixture.html#a0a047ec3e7bd60c3240f26ec27cc1738", null ],
    [ "m_displacement", "structLineFixture.html#a2e761ffaed5365c31d07d512fadcbb82", null ],
    [ "m_displacementUnit", "structLineFixture.html#aa20ed34929f0b6c1de3ae8ab5f390606", null ],
    [ "m_length", "structLineFixture.html#a5f5a7495a344ea7c1e2e17840b34d21e", null ],
    [ "m_maxDistanceToLine", "structLineFixture.html#a9dede7d0bd2ea2d809efe62a2c57fb03", null ],
    [ "m_p0", "structLineFixture.html#ab4873335bb35eafe897942262ddf95a7", null ],
    [ "m_p1", "structLineFixture.html#a68ac51d1194e931e1996650b14b7f522", null ]
];