var structPacketDirection =
[
    [ "PacketDirection", "structPacketDirection.html#a3b498fe558a44272b9dd459b8166b78e", null ],
    [ "PacketDirection", "structPacketDirection.html#ab3c40fd79d72e4d42952853fdf3c13be", null ],
    [ "PacketDirection", "structPacketDirection.html#ac762a4a6c285122f370131a8fecdc41e", null ],
    [ "PacketDirection", "structPacketDirection.html#a74a3b2ad1482cf96d351d242fd0104a5", null ],
    [ "checkOrthonormal", "structPacketDirection.html#acb78a003fc20212e982556f30ad4b3d7", null ],
    [ "checkOrthonormalVerbose", "structPacketDirection.html#abf0d0ecc3bee0f647667b5df584ab9e4", null ],
    [ "scatter", "structPacketDirection.html#a65dbed557b740bcfa6908c5df2a6b53f", null ],
    [ "scatter", "structPacketDirection.html#a2858e59b983828d232dc3574a7e169e7", null ],
    [ "a", "structPacketDirection.html#a5876fcb53302e8da2984428663661b42", null ],
    [ "b", "structPacketDirection.html#a0a2e033de284b5658273a00de1bff221", null ],
    [ "d", "structPacketDirection.html#a8ed3614b6fcf8887a06272cc3e11195c", null ]
];