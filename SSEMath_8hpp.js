var SSEMath_8hpp =
[
    [ "SSEBase", "classSSE_1_1SSEBase.html", "classSSE_1_1SSEBase" ],
    [ "Scalar", "classSSE_1_1Scalar.html", "classSSE_1_1Scalar" ],
    [ "Vector", "classSSE_1_1Vector.html", "classSSE_1_1Vector" ],
    [ "UnitVector", "classSSE_1_1UnitVector.html", "classSSE_1_1UnitVector" ],
    [ "Point2", "SSEMath_8hpp.html#a8cf30a4d58545da70715d6114a739266", null ],
    [ "Point3", "SSEMath_8hpp.html#a171002b1c0f030326d1c716759bcb8e4", null ],
    [ "UnitVector2", "SSEMath_8hpp.html#af6e5d2edd553d07f987f30a677d4f5b3", null ],
    [ "UnitVector3", "SSEMath_8hpp.html#ae8a5cb13c038f6bc21cab8f7db636ea4", null ],
    [ "Vector2", "SSEMath_8hpp.html#ae3b867e17c636ac331190779da9e1cd7", null ],
    [ "Vector3", "SSEMath_8hpp.html#a0d488d1b3890a5949a6b867d0b15a363", null ],
    [ "Checking", "SSEMath_8hpp.html#a7cb9f996841e11d97124c15698a281e3", [
      [ "NoCheck", "SSEMath_8hpp.html#a7cb9f996841e11d97124c15698a281e3aa639570e84a0e3aed450882b4c91d9b4", null ],
      [ "Assert", "SSEMath_8hpp.html#a7cb9f996841e11d97124c15698a281e3ad69bf1b94e5904318e96dbe1a30ffe71", null ],
      [ "Except", "SSEMath_8hpp.html#a7cb9f996841e11d97124c15698a281e3a38d9f7dc171ed1f1dc1183fcef026026", null ],
      [ "Normalize", "SSEMath_8hpp.html#a7cb9f996841e11d97124c15698a281e3a432cdd054645de67471c45583db8537c", null ],
      [ "Silent", "SSEMath_8hpp.html#a7cb9f996841e11d97124c15698a281e3ab37da0c76e1a98b357a75c65463e3acb", null ]
    ] ],
    [ "cross", "SSEMath_8hpp.html#a19d04c2f16cc54a75b3e14ac731831e3", null ],
    [ "dot", "SSEMath_8hpp.html#a5dac6534a8f7edcb17dfdb1b35add403", null ],
    [ "norm", "SSEMath_8hpp.html#a69cdc83929688025138038a31fa478d2", null ],
    [ "norm2", "SSEMath_8hpp.html#a2b678c723c5501a5e255808996c0cfcf", null ],
    [ "normalize", "SSEMath_8hpp.html#ad6364d1b57c8c7c7e1b1606c33641ef6", null ],
    [ "normalsTo", "SSEMath_8hpp.html#a55b71c352b71a1db477437a433e600b4", null ],
    [ "normSquared", "SSEMath_8hpp.html#ac7337c63b14dfdd28a62c29bf34a47fd", null ]
];