var structMemcopyWED =
[
    [ "addr_from", "d7/d69/structMemcopyWED.html#a80e47b15d0ace5d340699d92c25e110f", null ],
    [ "addr_from0", "d7/d69/structMemcopyWED.html#a058618c81682f534c015fbbcb79cfda1", null ],
    [ "addr_from1", "d7/d69/structMemcopyWED.html#a2f35f5998a69d492b07187e0bf0d82a5", null ],
    [ "addr_to", "d7/d69/structMemcopyWED.html#a49b9d5b52923ba5610d4178cad04f7fc", null ],
    [ "buffered", "d7/d69/structMemcopyWED.html#a7b3a0067ff5baaa67aab593ae8d15c86", null ],
    [ "pad", "d7/d69/structMemcopyWED.html#ab5bf8c6ced6a4cde23f91e4f8317a3f3", null ],
    [ "pDst", "d7/d69/structMemcopyWED.html#a0a7e44867766a92b367d339956f6c031", null ],
    [ "pSrc", "d7/d69/structMemcopyWED.html#a26563800a39c822ddda68c647c97d797", null ],
    [ "resv", "d7/d69/structMemcopyWED.html#a06487ec071e9599de65e485c7719ef88", null ],
    [ "size", "d7/d69/structMemcopyWED.html#a5f69b98570713a3715da789398476613", null ]
];