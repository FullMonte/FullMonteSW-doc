var structFullMonteHW_1_1MaterialStruct =
[
    [ "calculateFromProperties", "d7/d59/structFullMonteHW_1_1MaterialStruct.html#a69c72b8091bfbdab868e6691e401db78", null ],
    [ "printHex", "d7/d59/structFullMonteHW_1_1MaterialStruct.html#a3f4a72a25c8b0dde0b78b3a33f738a2f", null ],
    [ "serialize", "d7/d59/structFullMonteHW_1_1MaterialStruct.html#aac31d1b32872d02ee6ac699ea095a5b5", null ],
    [ "operator<<", "d7/d59/structFullMonteHW_1_1MaterialStruct.html#a95b5d9cc92bc01d7dec03812dfd31289", null ],
    [ "absfrac", "d7/d59/structFullMonteHW_1_1MaterialStruct.html#a9b8b251a1c21abd8f2c5ab91ca081404", null ],
    [ "bits", "d7/d59/structFullMonteHW_1_1MaterialStruct.html#a2d9cfcaf6f1bb6ba03ef818a2f5bdb1f", null ],
    [ "hg_struct", "d7/d59/structFullMonteHW_1_1MaterialStruct.html#ab73feb6ed51fed6892a617ecbbd16531", null ],
    [ "matID", "d7/d59/structFullMonteHW_1_1MaterialStruct.html#a39880bfb7a2f7d6958882b96e9cf5db5", null ],
    [ "mu_t", "d7/d59/structFullMonteHW_1_1MaterialStruct.html#a584638f7913bd17aa812f19e27a0ffa4", null ],
    [ "recip_mu_t", "d7/d59/structFullMonteHW_1_1MaterialStruct.html#abb73bbba17d5a1541b057389e5bc28da", null ]
];