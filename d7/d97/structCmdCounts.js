var structCmdCounts =
[
    [ "nCmdTotal", "d7/d97/structCmdCounts.html#ace350f14c81c7d6f68832636cef36034", null ],
    [ "nRespAError", "d7/d97/structCmdCounts.html#a2936302c2e05b26053fe5d897f439f74", null ],
    [ "nRespDError", "d7/d97/structCmdCounts.html#ad5eb0945ae4eded8ea67aae9190f657e", null ],
    [ "nRespDone", "d7/d97/structCmdCounts.html#aa44ddc3c23511e57e0e877fdb568471a", null ],
    [ "nRespFailed", "d7/d97/structCmdCounts.html#a0964c8828f64fd42f06cdac43c96c275", null ],
    [ "nRespFault", "d7/d97/structCmdCounts.html#aaeef94312c473229e0f823307b08527d", null ],
    [ "nRespFlushed", "d7/d97/structCmdCounts.html#a8e97b8c709259a725a52fb5f30568de5", null ],
    [ "nRespInvalid", "d7/d97/structCmdCounts.html#affb697ec160d91da4a710280b64b0398", null ],
    [ "nRespNLock", "d7/d97/structCmdCounts.html#afaaaa5acc9d7696779d147df2cdfc4e2", null ],
    [ "nRespNRes", "d7/d97/structCmdCounts.html#a77e755303483e97c5ed990902bd122c9", null ],
    [ "nRespPaged", "d7/d97/structCmdCounts.html#aef80d84c9132a278d2e13dba3d3ac865", null ]
];