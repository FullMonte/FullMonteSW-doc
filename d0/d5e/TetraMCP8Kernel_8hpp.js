var TetraMCP8Kernel_8hpp =
[
    [ "TetraP8Kernel", "d4/d02/classTetraP8Kernel.html", "d4/d02/classTetraP8Kernel" ],
    [ "TetraMCP8Kernel", "d8/d42/classTetraMCP8Kernel.html", "d8/d42/classTetraMCP8Kernel" ],
    [ "MemcopyWED", "d2/d37/structTetraMCP8Kernel_1_1MemcopyWED.html", "d2/d37/structTetraMCP8Kernel_1_1MemcopyWED" ],
    [ "DEVICE_STRING", "d0/d5e/TetraMCP8Kernel_8hpp.html#a0cc1f594a089044000af260be46f2309", null ],
    [ "STATUS_DONE", "d0/d5e/TetraMCP8Kernel_8hpp.html#a3c7966dcc2aec8e20459e036f5823fc2", null ],
    [ "STATUS_READY", "d0/d5e/TetraMCP8Kernel_8hpp.html#a64f314e17a1be8e6c8e09bb90b9ecb68", null ],
    [ "STATUS_RUNNING", "d0/d5e/TetraMCP8Kernel_8hpp.html#a799b04d03f49612e3e636b56c3972b80", null ],
    [ "STATUS_WAITING", "d0/d5e/TetraMCP8Kernel_8hpp.html#a4eb8ee21eeb7699dbe5fcee4c30d7b72", null ]
];