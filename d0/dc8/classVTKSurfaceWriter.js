var classVTKSurfaceWriter =
[
    [ "VTKSurfaceWriter", "d0/dc8/classVTKSurfaceWriter.html#acb05ef7fe2e47ea1a7d10c1cc8bbd674", null ],
    [ "~VTKSurfaceWriter", "d0/dc8/classVTKSurfaceWriter.html#a7478714d379980e2e8202718ed79cd27", null ],
    [ "addData", "d0/dc8/classVTKSurfaceWriter.html#af653e6ebbcda5720edefdeda4ad5206a", null ],
    [ "addHeaderComment", "d0/dc8/classVTKSurfaceWriter.html#adfbbf16921daf122fa02486e4828583f", null ],
    [ "filename", "d0/dc8/classVTKSurfaceWriter.html#a67d5650d0cba5b838511334a8f2d82fe", null ],
    [ "filename", "d0/dc8/classVTKSurfaceWriter.html#a69471cad74eb7a18a9949844ed732be2", null ],
    [ "mesh", "d0/dc8/classVTKSurfaceWriter.html#a6094a72c3fbbd82dcb3e4639b6be7b2e", null ],
    [ "mesh", "d0/dc8/classVTKSurfaceWriter.html#a9d7c3dbf6002bf9445d78fb3e30be94f", null ],
    [ "removeData", "d0/dc8/classVTKSurfaceWriter.html#a9e909b44983bb275724d3edea378c588", null ],
    [ "removeData", "d0/dc8/classVTKSurfaceWriter.html#afe7de396c70e52f7674b6345059d0700", null ],
    [ "write", "d0/dc8/classVTKSurfaceWriter.html#a1196ed0e3915a7c925cb8807d2b8863c", null ],
    [ "m_arrays", "d0/dc8/classVTKSurfaceWriter.html#aa7a2a05517baa1c7f24b16958a29dd28", null ],
    [ "m_filename", "d0/dc8/classVTKSurfaceWriter.html#a7ab6723c25a6a1f1dfe0cce97b1aaafa", null ],
    [ "m_manualHeader", "d0/dc8/classVTKSurfaceWriter.html#aaf2d7a2cad7ce811b2abe7b10e4bb60f", null ],
    [ "m_mesh", "d0/dc8/classVTKSurfaceWriter.html#a58f8833ee8dc8a9d986b7d5c1bb7325d", null ],
    [ "surfWriter", "d0/dc8/classVTKSurfaceWriter.html#aad7eb4e04a13f7388f9129324850f6e6", null ]
];