var classAbstractDirectedSurfaceScorer =
[
    [ "AbstractDirectedSurfaceScorer", "d0/da7/classAbstractDirectedSurfaceScorer.html#a766e76a90597bc8cb51c4731f5761bfd", null ],
    [ "~AbstractDirectedSurfaceScorer", "d0/da7/classAbstractDirectedSurfaceScorer.html#a454842df9e1dd28f7e40f6f947c9442f", null ],
    [ "addGeometryPredicate", "d0/da7/classAbstractDirectedSurfaceScorer.html#a4f77ff250d0525c97cdbaedbce27a0c9", null ],
    [ "addScoringRegionBoundary", "d0/da7/classAbstractDirectedSurfaceScorer.html#a43e714d086fa2d20b59850852cf4303e", null ],
    [ "addScoringSurface", "d0/da7/classAbstractDirectedSurfaceScorer.html#a821f25c9e8168e5ef32345a39e4ef597", null ],
    [ "clearSurfaces", "d0/da7/classAbstractDirectedSurfaceScorer.html#a9049e87c7abb790baa4c1f1dc23ff45a", null ],
    [ "createOutputSurfaces", "d0/da7/classAbstractDirectedSurfaceScorer.html#ac59a61fe97427d84dbb2aff0dd668402", null ],
    [ "getNumberOfOutputSurfaces", "d0/da7/classAbstractDirectedSurfaceScorer.html#a8b3f2d5027ad2a04978067c4b19591fc", null ],
    [ "getOutputSurface", "d0/da7/classAbstractDirectedSurfaceScorer.html#ab7929539d5644738ca2702b2644aa2c6", null ],
    [ "numericalAperture", "d0/da7/classAbstractDirectedSurfaceScorer.html#ac5aa8417f869fa39fb3a734218f503c2", null ],
    [ "numericalAperture", "d0/da7/classAbstractDirectedSurfaceScorer.html#ad02b258902b6eebac852bc577dcf228e", null ],
    [ "removeGeometryPredicate", "d0/da7/classAbstractDirectedSurfaceScorer.html#aae33fd0ab9c154b826893b6ff376efff", null ],
    [ "removeScoringRegionBoundary", "d0/da7/classAbstractDirectedSurfaceScorer.html#ad5189d7ab3ca2547c88b2d2a5cb053e7", null ],
    [ "removeScoringSurface", "d0/da7/classAbstractDirectedSurfaceScorer.html#a741bb8db7eb686f4fe118566f8b64e82", null ],
    [ "m_NA", "d0/da7/classAbstractDirectedSurfaceScorer.html#abdb7905c5ee363d86ea293f2ba913593", null ],
    [ "m_outputCellPredicates", "d0/da7/classAbstractDirectedSurfaceScorer.html#abcd31fac768ae7d4e82447c23accacaa", null ],
    [ "m_outputSurfaces", "d0/da7/classAbstractDirectedSurfaceScorer.html#a54e2852c53dbdc44d4b204fcd404a520", null ]
];