var classAbsorberTestFixture =
[
    [ "StimGen", "d0/dc1/classAbsorberTestFixture.html#ae3bbe66ef5d4b1d183862f187b5e38a7", null ],
    [ "AbsorberTestFixture", "d0/dc1/classAbsorberTestFixture.html#a48959e91b3dc2e8cc10f7cf9973a9201", null ],
    [ "getAbsorbFrac", "d0/dc1/classAbsorberTestFixture.html#a021a48cf613dab1b4d143a9e4870be64", null ],
    [ "setAbsorbFracs", "d0/dc1/classAbsorberTestFixture.html#ada29816ceee9b9925b24551021e67f59", null ],
    [ "checker", "d0/dc1/classAbsorberTestFixture.html#af0055d368bc2d02dfba0d9a02ec33f65", null ],
    [ "m_rng", "d0/dc1/classAbsorberTestFixture.html#aa2f2d9c48dd0e625455c14fee48e6a89", null ],
    [ "stimulus", "d0/dc1/classAbsorberTestFixture.html#a168b62a55bb3b472ddd2e59bab7cadde", null ]
];