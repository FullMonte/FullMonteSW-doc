var classEmitter_1_1HemiSphere =
[
    [ "HemiSphere", "d0/df4/classEmitter_1_1HemiSphere.html#a2245e5b1726b0f09ae1ba90c35420f74", null ],
    [ "direction", "d0/df4/classEmitter_1_1HemiSphere.html#ae0dc0d08755dc064dc32dd5d4a47212c", null ],
    [ "setThetaDistribution", "d0/df4/classEmitter_1_1HemiSphere.html#aed2c911cda71e8442c2d92066c8170af", null ],
    [ "setThetaDistribution", "d0/df4/classEmitter_1_1HemiSphere.html#a610788bebf5d3b4d7bca4a9d8bdd551f", null ],
    [ "m_materialset", "d0/df4/classEmitter_1_1HemiSphere.html#aa939d9a3b99175f13a27c9e7ead586eb", null ],
    [ "m_mesh", "d0/df4/classEmitter_1_1HemiSphere.html#a0eda6079b8cc516b91ae15cb9fe032dc", null ],
    [ "m_NA", "d0/df4/classEmitter_1_1HemiSphere.html#aac7ca9f99459dae0978f3d6fa4d46258", null ],
    [ "m_norm", "d0/df4/classEmitter_1_1HemiSphere.html#af23d18e549934da783d3f1e5d90e66f2", null ],
    [ "m_refractiveIndex", "d0/df4/classEmitter_1_1HemiSphere.html#a926b4e9fea609d73e5308777ee184c78", null ],
    [ "m_thetaDistribution", "d0/df4/classEmitter_1_1HemiSphere.html#aa040b1b9332a4201cc12d55691e5731c", null ],
    [ "Q", "d0/df4/classEmitter_1_1HemiSphere.html#af7aac90bd72d283653f76b2c87adfa89", null ]
];