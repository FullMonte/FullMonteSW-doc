var classOutputData =
[
    [ "Visitor", "dc/dad/classOutputData_1_1Visitor.html", "dc/dad/classOutputData_1_1Visitor" ],
    [ "OutputType", "d0/d86/classOutputData.html#a4825fb70f99fdb498b34915a4b71fee7", [
      [ "UnknownOutputType", "d0/d86/classOutputData.html#a4825fb70f99fdb498b34915a4b71fee7a450df4d6db3f7d6f0774a3d53708733b", null ],
      [ "PhotonWeight", "d0/d86/classOutputData.html#a4825fb70f99fdb498b34915a4b71fee7a2c41129e8b0d8e46895d730db6c06870", null ],
      [ "Energy", "d0/d86/classOutputData.html#a4825fb70f99fdb498b34915a4b71fee7ab5bd56f3a90700cd565e880b6420ff58", null ],
      [ "Fluence", "d0/d86/classOutputData.html#a4825fb70f99fdb498b34915a4b71fee7a646bd12be481b1f744de9f00309a87bf", null ],
      [ "EnergyPerVolume", "d0/d86/classOutputData.html#a4825fb70f99fdb498b34915a4b71fee7a1d307d9b28d7e58abfc0b2e591aab34e", null ]
    ] ],
    [ "UnitType", "d0/d86/classOutputData.html#af72dca41b98b54fa2b98cfc76b9fec78", [
      [ "UnknownUnitType", "d0/d86/classOutputData.html#af72dca41b98b54fa2b98cfc76b9fec78a9cac73f81ed21aec3fda81790ce4d977", null ],
      [ "J_m", "d0/d86/classOutputData.html#af72dca41b98b54fa2b98cfc76b9fec78aaa02edefccc94e3f9011f188fc5a0529", null ],
      [ "J_cm", "d0/d86/classOutputData.html#af72dca41b98b54fa2b98cfc76b9fec78a78f22576ba3853a68eef9bc0794ee984", null ],
      [ "J_mm", "d0/d86/classOutputData.html#af72dca41b98b54fa2b98cfc76b9fec78a091faa9a0ad7875dfbd15063d5218ea5", null ],
      [ "W_m", "d0/d86/classOutputData.html#af72dca41b98b54fa2b98cfc76b9fec78ae432bb2f984a970e1bcc4844bf86dbd7", null ],
      [ "W_cm", "d0/d86/classOutputData.html#af72dca41b98b54fa2b98cfc76b9fec78a5c7d8591180bee552c0d8fb6e2cee44c", null ],
      [ "W_mm", "d0/d86/classOutputData.html#af72dca41b98b54fa2b98cfc76b9fec78a5f6506d02818f69999b8a8b39933e26c", null ]
    ] ],
    [ "~OutputData", "d0/d86/classOutputData.html#a525bfc788d9e24559c70d27bfadc6035", null ],
    [ "acceptVisitor", "d0/d86/classOutputData.html#aad8358931404c9e14cefee9ee65e3a28", null ],
    [ "clone", "d0/d86/classOutputData.html#ae216e84e250eaa9258ec03f82d5d0067", null ],
    [ "deepCopy", "d0/d86/classOutputData.html#a6c10eb6da9bd39ce184a90930804d82b", null ],
    [ "name", "d0/d86/classOutputData.html#a2368f59ebaadf975c37dcf4034eebc02", null ],
    [ "name", "d0/d86/classOutputData.html#a9b3c480f45f3f41ba4b201be586c96ee", null ],
    [ "outputType", "d0/d86/classOutputData.html#a63c785e16e185ac38d652bb814bfa696", null ],
    [ "outputType", "d0/d86/classOutputData.html#aa226bcad30e3f01549813c4371361b7a", null ],
    [ "staticType", "d0/d86/classOutputData.html#a91a173a5d4fe0172c11d85d40111ad35", null ],
    [ "type", "d0/d86/classOutputData.html#a61322138a4a1d00e957ff7824d71acd5", null ],
    [ "unitType", "d0/d86/classOutputData.html#ab4c59f7f20effd9375209b2af467b4e0", null ],
    [ "unitType", "d0/d86/classOutputData.html#a0f66f0abb94586a424badbdd0218fa5d", null ],
    [ "m_name", "d0/d86/classOutputData.html#a8846bf4e30eb62c770b9b28335e09c4c", null ],
    [ "m_outputType", "d0/d86/classOutputData.html#a2fc3147e2cc660617af8410a53f043d8", null ],
    [ "m_unitType", "d0/d86/classOutputData.html#ad6a3bb07754a5bdf5b6fb640e63265ba", null ],
    [ "s_type", "d0/d86/classOutputData.html#aac37d594926ab8a36ed1a754af062674", null ]
];