var classpsl__m20kecc =
[
    [ "psl_m20kecc.SYN", "d5/d02/classpsl__m20kecc_1_1SYN.html", "d5/d02/classpsl__m20kecc_1_1SYN" ],
    [ "all", "d8/df0/classpsl__m20kecc.html#ad77eebe99f28721d83890e401b8f328a", null ],
    [ "altera_mf", "d8/df0/classpsl__m20kecc.html#ad244a3ba2f7f89a89a263149d2baa159", null ],
    [ "clock", "d8/df0/classpsl__m20kecc.html#a8ae98be2d0e5dca7ee35793a00f67f9a", null ],
    [ "data", "d8/df0/classpsl__m20kecc.html#a07e06f7444446b3d34f397083fd31532", null ],
    [ "eccstatus", "d8/df0/classpsl__m20kecc.html#add632cb7bd4f27ac6792343d6e1f0d24", null ],
    [ "ieee", "d8/df0/classpsl__m20kecc.html#abd0b845e448b93969bdae4df6614f8ad", null ],
    [ "q", "d8/df0/classpsl__m20kecc.html#a022f7b6cee9a60c520f6237de722b780", null ],
    [ "rdaddress", "d8/df0/classpsl__m20kecc.html#a80a03b5560046e31fc330fd108b41723", null ],
    [ "rden", "d8/df0/classpsl__m20kecc.html#ae8df1d8f7494d484782197c34967cbc3", null ],
    [ "std_logic_1164", "d8/df0/classpsl__m20kecc.html#aa7e4bae5a290736b7759706b10260bc7", null ],
    [ "wraddress", "d8/df0/classpsl__m20kecc.html#a5c2d53a2b4fa0c31812773f09e50fdd9", null ],
    [ "wren", "d8/df0/classpsl__m20kecc.html#ad750eedc09348e31cd14672b661de51a", null ]
];