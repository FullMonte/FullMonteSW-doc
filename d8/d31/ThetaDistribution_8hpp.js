var ThetaDistribution_8hpp =
[
    [ "ThetaDistributionFunc", "d5/df8/classThetaDistributionFunc.html", "d5/df8/classThetaDistributionFunc" ],
    [ "ThetaDistribution", "d8/d31/ThetaDistribution_8hpp.html#a6b01d02cb7fe9011b597a648aefb3aae", [
      [ "UNIFORM", "d8/d31/ThetaDistribution_8hpp.html#a6b01d02cb7fe9011b597a648aefb3aaea8f44784d154005a214e0fe94119d28ef", null ],
      [ "LAMBERT", "d8/d31/ThetaDistribution_8hpp.html#a6b01d02cb7fe9011b597a648aefb3aaea143bd921a55b7a7d5e59b87b5be78075", null ],
      [ "CUSTOM", "d8/d31/ThetaDistribution_8hpp.html#a6b01d02cb7fe9011b597a648aefb3aaea945d6010d321d9fe75cbba7b6f37f3b5", null ]
    ] ]
];