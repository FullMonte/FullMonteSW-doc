var scatterCheck_8m =
[
    [ "checkPacketDirection", "d8/d94/scatterCheck_8m.html#a06496b6c83ddac6e685f017ce1a184e8", null ],
    [ "dot", "d8/d94/scatterCheck_8m.html#aaca229d4839ca018c357b1ea9d308f76", null ],
    [ "hist", "d8/d94/scatterCheck_8m.html#a977751d11647f8632883a43865d5cbe6", null ],
    [ "plot", "d8/d94/scatterCheck_8m.html#aa3d50600bef03e28003f9fe05ecd2bf7", null ],
    [ "title", "d8/d94/scatterCheck_8m.html#a94787a6270713613644abff26e220b61", null ],
    [ "title", "d8/d94/scatterCheck_8m.html#ace2235ddd44d1115cc651aa1b2e879b0", null ],
    [ "xlabel", "d8/d94/scatterCheck_8m.html#a41997c1fc7125f0a218fde48ce159a46", null ],
    [ "ylabel", "d8/d94/scatterCheck_8m.html#a45fda5c5fd6adcaeeffac04ea4405827", null ],
    [ "a", "d8/d94/scatterCheck_8m.html#a4124bc0a9335c27f086f24ba207a4912", null ],
    [ "ap", "d8/d94/scatterCheck_8m.html#af7bda698a6161487a48a7dacb498a738", null ],
    [ "b", "d8/d94/scatterCheck_8m.html#a21ad0bd836b90d08f4cf640b4c298e7c", null ],
    [ "bp", "d8/d94/scatterCheck_8m.html#ab03632e190331f379668603ee46d4772", null ],
    [ "cs_phi", "d8/d94/scatterCheck_8m.html#a7f636d905bd48023fd99156e0839cfd5", null ],
    [ "cs_theta", "d8/d94/scatterCheck_8m.html#aaf3e580f0b041ba7bab5f7089c7d9760", null ],
    [ "d", "d8/d94/scatterCheck_8m.html#a1aabac6d068eef6a7bad3fdf50a05cc8", null ],
    [ "dp", "d8/d94/scatterCheck_8m.html#a72303c49017eab1a5a5b720793a9c002", null ],
    [ "figure", "d8/d94/scatterCheck_8m.html#a391e34f2de441d79152a7b3d6e4c9c86", null ],
    [ "T", "d8/d94/scatterCheck_8m.html#adf1f3edb9115acb0a1e04209b7a9937b", null ],
    [ "U", "d8/d94/scatterCheck_8m.html#a81cf6107131a3583e2b0b762cb9c2862", null ]
];