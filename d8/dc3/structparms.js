var structparms =
[
    [ "base_image", "d8/dc3/structparms.html#a6b1ed126c6d09ad3b34f9850b7ed5161", null ],
    [ "buffer_percent", "d8/dc3/structparms.html#a8fae0ff16b38f495a3a1eadc4c2dacb5", null ],
    [ "caia_version", "d8/dc3/structparms.html#a370b4089b73fa2756946bd890e309c88", null ],
    [ "credits", "d8/dc3/structparms.html#acd0b92b77872cb7f09a41152df909f58", null ],
    [ "image_loaded", "d8/dc3/structparms.html#a46ac067b9652f3ed6aafd73a5b8e0e03", null ],
    [ "paged_percent", "d8/dc3/structparms.html#a36c138c499cbca8edd2682ffed974594", null ],
    [ "psl_rev_level", "d8/dc3/structparms.html#a345502e30cfaeb0e4a0996704de49e36", null ],
    [ "reorder_percent", "d8/dc3/structparms.html#a3368188c3f381876e2a7a5b3334db2bb", null ],
    [ "resp_percent", "d8/dc3/structparms.html#a22d4f8d0f0f2c86bbea2a25329150ffe", null ],
    [ "seed", "d8/dc3/structparms.html#ae25e337a3e66c63ccda0ac7bf7b744b2", null ],
    [ "timeout", "d8/dc3/structparms.html#adcaa726bee2ca9efbd8cdbbbcc863c42", null ]
];