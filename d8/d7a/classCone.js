var classCone =
[
    [ "Cone", "d8/d7a/classCone.html#aec709e915b3271a750d420b14b215bfb", null ],
    [ "~Cone", "d8/d7a/classCone.html#a36a6a946043f7b24a34e42cb88b5a4e8", null ],
    [ "halfAngle", "d8/d7a/classCone.html#a632ce7be6f77ce3a65d82140d99dc166", null ],
    [ "halfAngle", "d8/d7a/classCone.html#a2265a523c22f82b0c317a8005b412d06", null ],
    [ "numericalAperture", "d8/d7a/classCone.html#ab351aa3814033ab5d705cfd607cde134", null ],
    [ "numericalAperture", "d8/d7a/classCone.html#aeaa1e099960a41c8d5d1630442fa26b6", null ],
    [ "refractiveIndex", "d8/d7a/classCone.html#a5d7f69a50daccbd50bb00ab663bf52ed", null ],
    [ "refractiveIndex", "d8/d7a/classCone.html#af46d3eef4cebcef29255e9f130cd6db6", null ],
    [ "m_halfAngle", "d8/d7a/classCone.html#a01297b7e613f4563adaa84c09dec7c43", null ]
];