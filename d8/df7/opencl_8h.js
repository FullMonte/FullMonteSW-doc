var opencl_8h =
[
    [ "checkError", "d8/df7/opencl_8h.html#aa3eea8e2765e8164d53cba428bfbd248", null ],
    [ "_checkError", "d8/df7/opencl_8h.html#a2ce13cd7272eaf6125b49278edd228f4", null ],
    [ "alignedFree", "d8/df7/opencl_8h.html#ac6192b30f98727a43139f9f1b8b211fb", null ],
    [ "alignedMalloc", "d8/df7/opencl_8h.html#adad76a5b63ec2259c4d8751886bc7753", null ],
    [ "cleanup", "d8/df7/opencl_8h.html#a4b66d5e31b5dc18b314c8a68163263bd", null ],
    [ "createProgramFromBinary", "d8/df7/opencl_8h.html#abdc8b1048e537da84d2e6892bced185e", null ],
    [ "fileExists", "d8/df7/opencl_8h.html#aa4e1b53becdb354c70e1d083fcfd3f90", null ],
    [ "findPlatform", "d8/df7/opencl_8h.html#a21f2d6fe5e3c2b3644779413e328b125", null ],
    [ "getBoardBinaryFile", "d8/df7/opencl_8h.html#a3b769719b83c93344deeb5fd55bd6674", null ],
    [ "getCurrentTimestamp", "d8/df7/opencl_8h.html#a6a503356aff347ca684c9e96b54eff47", null ],
    [ "getDeviceName", "d8/df7/opencl_8h.html#afec35b4109bad93114c2e4c0099082c3", null ],
    [ "getDevices", "d8/df7/opencl_8h.html#a903382a4bacbc5d633875e6d29bdb6f4", null ],
    [ "getPlatformName", "d8/df7/opencl_8h.html#a9f1bfa559f86c9ab7025c148bfb44890", null ],
    [ "getStartEndTime", "d8/df7/opencl_8h.html#a14c078e95e04a6daae9ec168d0fea8ee", null ],
    [ "getStartEndTime", "d8/df7/opencl_8h.html#af18eb694855657be00d3f80931797b7a", null ],
    [ "loadBinaryFile", "d8/df7/opencl_8h.html#ad600cef36637401a7e18a85fa3ef4276", null ],
    [ "oclContextCallback", "d8/df7/opencl_8h.html#a7e4486a95239e8a4d869d09da49407f5", null ],
    [ "printError", "d8/df7/opencl_8h.html#a8a81775e8c6f7197aeec0697ad7d20f3", null ],
    [ "setCwdToExeDir", "d8/df7/opencl_8h.html#af21781dcb319fba099f4a2d92d5d79ec", null ],
    [ "waitMilliseconds", "d8/df7/opencl_8h.html#abe55a802d3195f4c0a0cc2cb72955758", null ]
];