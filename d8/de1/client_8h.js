var client_8h =
[
    [ "client", "d3/db7/structclient.html", "d3/db7/structclient" ],
    [ "client_state", "d8/de1/client_8h.html#a6fc25b0a629fd1ef2a29c1543adc4a24", [
      [ "CLIENT_NONE", "d8/de1/client_8h.html#a6fc25b0a629fd1ef2a29c1543adc4a24a6eaf6e7309cf4916f98d2de6f99056c1", null ],
      [ "CLIENT_INIT", "d8/de1/client_8h.html#a6fc25b0a629fd1ef2a29c1543adc4a24a97d49ec1987c02db8f23ec464ea4e1a2", null ],
      [ "CLIENT_VALID", "d8/de1/client_8h.html#a6fc25b0a629fd1ef2a29c1543adc4a24aa91b4863d3a25e4a6d1493f60fcf6ee8", null ]
    ] ],
    [ "flush_state", "d8/de1/client_8h.html#ac594a36b52d4eea6e42806e31f87c773", [
      [ "FLUSH_NONE", "d8/de1/client_8h.html#ac594a36b52d4eea6e42806e31f87c773a2f5c4053a047fd0ceba0f9116c14147b", null ],
      [ "FLUSH_PAGED", "d8/de1/client_8h.html#ac594a36b52d4eea6e42806e31f87c773a776b352380baf64eb2fd14c8508c49ef", null ],
      [ "FLUSH_FLUSHING", "d8/de1/client_8h.html#ac594a36b52d4eea6e42806e31f87c773afab05fbfa3839de92ecaa7e62e0b082f", null ]
    ] ],
    [ "client_drop", "d8/de1/client_8h.html#a32d045b094c53ae4a478d380f241a953", null ]
];