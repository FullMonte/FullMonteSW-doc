var classvtkDoseHistogramWrapper =
[
    [ "MeasureType", "d8/dba/classvtkDoseHistogramWrapper.html#ab464f897d2d7ebc624e50377cd864fab", [
      [ "Unknown", "d8/dba/classvtkDoseHistogramWrapper.html#ab464f897d2d7ebc624e50377cd864faba634e8e1750f546d110d30c755bbe87b1", null ],
      [ "Surface", "d8/dba/classvtkDoseHistogramWrapper.html#ab464f897d2d7ebc624e50377cd864faba11df2408a7501897ceaa5adc74545528", null ],
      [ "Volume", "d8/dba/classvtkDoseHistogramWrapper.html#ab464f897d2d7ebc624e50377cd864fabaa0a75532ba70574aae3c617707d4e77a", null ]
    ] ],
    [ "vtkDoseHistogramWrapper", "d8/dba/classvtkDoseHistogramWrapper.html#a6d81c4821846d83c7ac095a46896586f", null ],
    [ "~vtkDoseHistogramWrapper", "d8/dba/classvtkDoseHistogramWrapper.html#a724293265845a908a6d019c6c8e88323", null ],
    [ "measureMode", "d8/dba/classvtkDoseHistogramWrapper.html#a6f571d5b69314449a4e44ffe65aa64da", null ],
    [ "measureMode", "d8/dba/classvtkDoseHistogramWrapper.html#aafc7e6be3fa5bdce4b9206d8eef015a2", null ],
    [ "measureType", "d8/dba/classvtkDoseHistogramWrapper.html#a5dcb77bbc0b16f72836a5e227f03dbb2", null ],
    [ "measureType", "d8/dba/classvtkDoseHistogramWrapper.html#a4d99aab15b745a67f72cacbde5fabb5d", null ],
    [ "New", "d8/dba/classvtkDoseHistogramWrapper.html#af1acf59638886991c076a517a78b686d", null ],
    [ "source", "d8/dba/classvtkDoseHistogramWrapper.html#afd91ff7a304ac54825feb316a486fbc8", null ],
    [ "source", "d8/dba/classvtkDoseHistogramWrapper.html#a9a6c07ca46d224082693e44c7bc871af", null ],
    [ "table", "d8/dba/classvtkDoseHistogramWrapper.html#ace649eb7405b763d8fd472785dd4f8e9", null ],
    [ "update", "d8/dba/classvtkDoseHistogramWrapper.html#a8dbea0456a518a4d91268eb949669ff5", null ],
    [ "vtkTypeMacro", "d8/dba/classvtkDoseHistogramWrapper.html#a7f782553c34c4e94dc8f6354c88f3a33", null ],
    [ "m_histogram", "d8/dba/classvtkDoseHistogramWrapper.html#a8b6dbb978e850ba4a7bbf350f2a32401", null ],
    [ "m_measureMode", "d8/dba/classvtkDoseHistogramWrapper.html#afa806c7456955cddeaeea59221678a4e", null ],
    [ "m_measureType", "d8/dba/classvtkDoseHistogramWrapper.html#ade32a24bbef8e1e30e8079abbabeae7b", null ],
    [ "m_vtkTable", "d8/dba/classvtkDoseHistogramWrapper.html#ad00faf4ab5a3c6674c0a2da7e478d6a5", null ]
];