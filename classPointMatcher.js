var classPointMatcher =
[
    [ "PointMatcher", "classPointMatcher.html#aefdc62c8c5edf3618de943305688e974", null ],
    [ "~PointMatcher", "classPointMatcher.html#ac21de0cb26f9aafd91744a45bc021086", null ],
    [ "query", "classPointMatcher.html#adf28b73c1f4b765cf951a0dd67f76359", null ],
    [ "reference", "classPointMatcher.html#a3ae967518dafe4ee4db882753abc4d1b", null ],
    [ "result", "classPointMatcher.html#a48c593f8181b3a8ddc0cc43897f2cd58", null ],
    [ "tolerance", "classPointMatcher.html#a13dd98321839fb171f1fa9c37e980ea8", null ],
    [ "unmatchedCount", "classPointMatcher.html#a6323d5cc7ac77dd55837ae8f29746455", null ],
    [ "update", "classPointMatcher.html#a7a8b7895fa7b0b220b15f0325808f2bb", null ]
];