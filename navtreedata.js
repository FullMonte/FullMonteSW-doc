var NAVTREE =
[
  [ "FullMonteSW", "index.html", [
    [ "Reporting Bugs and Requesting Features with FullMonte", "d7/d55/md_BUGREPORTS.html", null ],
    [ "BUILDING", "d6/dd0/md_BUILDING.html", null ],
    [ "Documentation", "d2/dc1/md_CONTRIBUTING.html", null ],
    [ "HowToCiteAndIncludedSW", "df/d8e/md_HowToCiteAndIncludedSW.html", null ],
    [ "CUDA software Kernels", "d6/d6c/md_Kernels_CUDA_README.html", null ],
    [ "Intel FPGA OpenCL software Kernels", "d4/d93/md_Kernels_FPGACL_README.html", null ],
    [ "LICENSE", "da/d19/md_LICENSE.html", null ],
    [ "README", "d0/d30/md_README.html", null ],
    [ "VTK interfaces", "db/d78/md_VTK_README.html", null ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"d0/dc8/classVTKSurfaceWriter.html#afe7de396c70e52f7674b6345059d0700",
"d1/da8/structDoseHistogramGenerator_1_1InputElement_1_1DoseLess.html",
"d2/d85/classMCMLOutputReader.html#acfd5625c373875b8c937805d352a8831",
"d3/d34/classEventCountComparison.html#a2cbef2e33e61b929b929be269bfbfdbe",
"d3/de0/classEventLogger.html#a4ffc620093a59d2063d70c78c129f4f8",
"d4/dd9/StandardArrayKernel_8hpp.html#af2c30e64c19eb271f9e9489410852216",
"d5/d86/classAccumulationEventScorer.html#aa0855ea0dc0e27eff2890da060cbb2cc",
"d6/d64/TetraMesh_8hpp.html",
"d6/dfa/classRNG__SFMT__AVX.html#ac9bde2bdc801815e365f2e2fee2cd39c",
"d7/dc2/classSource_1_1Abstract_1_1Visitor.html#a1361312df004acd3f975caade6a13383",
"d8/d91/MCMLKernel_8cpp.html#a046c8558aa81e7533079eb75ebc68458",
"d9/d45/EventCountComparison_8hpp_source.html",
"d9/dce/MMCJSONReader_8cpp.html#a8b97e0b2a0df6320490300fb05591b35",
"da/d65/classTetraMCP8DebugKernel.html#accc647098d4057fb6c170a92cc8759ea",
"da/dec/classSource_1_1Abstract.html#a12fe2df050fc11e0e2598637754df4b5",
"db/d89/classSource_1_1Fiber.html#a2964f1e87789355314553085929e0616",
"dc/da1/classTextFileMatrixWriter.html#a939e3e82a9bcca06909a00ca4cc4e082",
"dd/dd7/classLineQuery.html#a415271ff3c57007e06d2bf482d63e0e2",
"de/d90/classSurfaceSourceBuilder.html#a6655deb8e7fdc10aa4b4fc1b38bcb465",
"df/d1a/classMMCTextMeshReader.html#a745131d5620f7fc35a5f435ce1ee7c1e",
"df/d96/classSpatialMapOperator.html#ae6913e36b3c4181595dfc2eb682f45f7",
"hierarchy.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';