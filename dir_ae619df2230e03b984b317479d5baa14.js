var dir_ae619df2230e03b984b317479d5baa14 =
[
    [ "BDPIHGTestFixture.cpp", "d4/d1c/BDPIHGTestFixture_8cpp.html", "d4/d1c/BDPIHGTestFixture_8cpp" ],
    [ "BDPIHGTestFixture.hpp", "d9/d7c/BDPIHGTestFixture_8hpp.html", "d9/d7c/BDPIHGTestFixture_8hpp" ],
    [ "checkHG.m", "dc/d3a/checkHG_8m.html", "dc/d3a/checkHG_8m" ],
    [ "hg.m", "d5/d0c/hg_8m.html", "d5/d0c/hg_8m" ],
    [ "hg_bounds.m", "d3/dbb/hg__bounds_8m.html", "d3/dbb/hg__bounds_8m" ],
    [ "hg_test.m", "da/d77/hg__test_8m.html", "da/d77/hg__test_8m" ],
    [ "HGChecker.cpp", "d4/d0c/HGChecker_8cpp.html", null ],
    [ "HGChecker.hpp", "d8/d83/HGChecker_8hpp.html", [
      [ "HGChecker", "df/d0b/classHGChecker.html", "df/d0b/classHGChecker" ]
    ] ],
    [ "HGRandomStim.hpp", "da/d03/HGRandomStim_8hpp.html", [
      [ "HGRandomStim", "d5/d0f/classHGRandomStim.html", "d5/d0f/classHGRandomStim" ]
    ] ],
    [ "HGTestFixture.hpp", "d2/d5e/HGTestFixture_8hpp.html", [
      [ "HGTestFixture", "d7/d6c/classHGTestFixture.html", "d7/d6c/classHGTestFixture" ]
    ] ],
    [ "new_HGCAPI.cpp", "d9/d16/new__HGCAPI_8cpp.html", "d9/d16/new__HGCAPI_8cpp" ]
];