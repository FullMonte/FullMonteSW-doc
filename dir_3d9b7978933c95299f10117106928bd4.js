var dir_3d9b7978933c95299f10117106928bd4 =
[
    [ "AbsorptionSum.cpp", "d1/d8a/AbsorptionSum_8cpp.html", null ],
    [ "AbsorptionSum.hpp", "d2/d87/AbsorptionSum_8hpp.html", [
      [ "SpatialMap", "df/d80/classSpatialMap.html", "df/d80/classSpatialMap" ],
      [ "AbsorptionSum", "df/d91/classAbsorptionSum.html", "df/d91/classAbsorptionSum" ]
    ] ],
    [ "AccumulationLineQuery.cpp", "d8/da9/AccumulationLineQuery_8cpp.html", null ],
    [ "AccumulationLineQuery.hpp", "d2/dab/AccumulationLineQuery_8hpp.html", [
      [ "AccumulationLineQuery", "d3/dab/classAccumulationLineQuery.html", "d3/dab/classAccumulationLineQuery" ]
    ] ],
    [ "BasicStats.cpp", "d3/d8c/BasicStats_8cpp.html", null ],
    [ "BasicStats.hpp", "d1/d9f/BasicStats_8hpp.html", [
      [ "BasicStats", "df/d70/classBasicStats.html", "df/d70/classBasicStats" ]
    ] ],
    [ "Chi2Homogeneity.cpp", "d1/de9/Chi2Homogeneity_8cpp.html", null ],
    [ "Chi2Homogeneity.hpp", "d0/dec/Chi2Homogeneity_8hpp.html", [
      [ "Chi2Homogeneity", "d9/d85/classChi2Homogeneity.html", "d9/d85/classChi2Homogeneity" ]
    ] ],
    [ "Chi2SetFixture.cpp", "dc/d8b/Chi2SetFixture_8cpp.html", null ],
    [ "Chi2SetFixture.hpp", "d0/d92/Chi2SetFixture_8hpp.html", "d0/d92/Chi2SetFixture_8hpp" ],
    [ "ConservationCheck.cpp", "d6/d97/ConservationCheck_8cpp.html", null ],
    [ "ConservationCheck.hpp", "d1/d73/ConservationCheck_8hpp.html", [
      [ "ConservationCheck", "df/dfc/classConservationCheck.html", "df/dfc/classConservationCheck" ]
    ] ],
    [ "DataDifference.cpp", "de/d87/DataDifference_8cpp.html", null ],
    [ "DataDifference.hpp", "d9/d1a/DataDifference_8hpp.html", [
      [ "AbstractDataDifference", "d8/db1/classAbstractDataDifference.html", "d8/db1/classAbstractDataDifference" ],
      [ "DataDifference", "d3/d5e/classDataDifference.html", "d3/d5e/classDataDifference" ]
    ] ],
    [ "DataSetStats.cpp", "dd/d33/DataSetStats_8cpp.html", null ],
    [ "DataSetStats.hpp", "dc/de3/DataSetStats_8hpp.html", [
      [ "SpatialMap", "df/d80/classSpatialMap.html", "df/d80/classSpatialMap" ],
      [ "DataSetStats", "d6/d2b/classDataSetStats.html", "d6/d2b/classDataSetStats" ]
    ] ],
    [ "DirectedSurfaceSum.cpp", "df/d14/DirectedSurfaceSum_8cpp.html", null ],
    [ "DirectedSurfaceSum.hpp", "d2/df5/DirectedSurfaceSum_8hpp.html", [
      [ "DirectedSurfaceSum", "d5/d48/classDirectedSurfaceSum.html", "d5/d48/classDirectedSurfaceSum" ]
    ] ],
    [ "DoseHistogram.cpp", "dd/d90/DoseHistogram_8cpp.html", "dd/d90/DoseHistogram_8cpp" ],
    [ "DoseHistogram.hpp", "d4/d42/DoseHistogram_8hpp.html", "d4/d42/DoseHistogram_8hpp" ],
    [ "DoseHistogramCollection.cpp", "d0/d30/DoseHistogramCollection_8cpp.html", "d0/d30/DoseHistogramCollection_8cpp" ],
    [ "DoseHistogramCollection.hpp", "d9/de2/DoseHistogramCollection_8hpp.html", [
      [ "DoseHistogramCollection", "d3/db9/classDoseHistogramCollection.html", "d3/db9/classDoseHistogramCollection" ]
    ] ],
    [ "DoseHistogramGenerator.cpp", "dd/d42/DoseHistogramGenerator_8cpp.html", null ],
    [ "DoseHistogramGenerator.hpp", "db/d8e/DoseHistogramGenerator_8hpp.html", [
      [ "SpatialMap", "df/d80/classSpatialMap.html", "df/d80/classSpatialMap" ],
      [ "DoseHistogramGenerator", "d9/d34/classDoseHistogramGenerator.html", "d9/d34/classDoseHistogramGenerator" ],
      [ "ElRegion", "d4/d08/structDoseHistogramGenerator_1_1ElRegion.html", "d4/d08/structDoseHistogramGenerator_1_1ElRegion" ],
      [ "RegionComp", "d3/d3f/structDoseHistogramGenerator_1_1ElRegion_1_1RegionComp.html", "d3/d3f/structDoseHistogramGenerator_1_1ElRegion_1_1RegionComp" ],
      [ "InputElement", "d2/d8d/structDoseHistogramGenerator_1_1InputElement.html", "d2/d8d/structDoseHistogramGenerator_1_1InputElement" ],
      [ "DoseLess", "d1/da8/structDoseHistogramGenerator_1_1InputElement_1_1DoseLess.html", "d1/da8/structDoseHistogramGenerator_1_1InputElement_1_1DoseLess" ],
      [ "OutputElement", "db/d26/structDoseHistogramGenerator_1_1OutputElement.html", "db/d26/structDoseHistogramGenerator_1_1OutputElement" ]
    ] ],
    [ "DoseSurfaceHistogramGenerator.cpp", "dc/def/DoseSurfaceHistogramGenerator_8cpp.html", null ],
    [ "DoseSurfaceHistogramGenerator.hpp", "d5/d9f/DoseSurfaceHistogramGenerator_8hpp.html", [
      [ "DoseSurfaceHistogramGenerator", "dd/d01/classDoseSurfaceHistogramGenerator.html", "dd/d01/classDoseSurfaceHistogramGenerator" ]
    ] ],
    [ "DoseVolumeHistogramGenerator.cpp", "d5/d49/DoseVolumeHistogramGenerator_8cpp.html", null ],
    [ "DoseVolumeHistogramGenerator.hpp", "da/d87/DoseVolumeHistogramGenerator_8hpp.html", [
      [ "DoseVolumeHistogramGenerator", "dd/d97/classDoseVolumeHistogramGenerator.html", "dd/d97/classDoseVolumeHistogramGenerator" ]
    ] ],
    [ "EmpiricalCDF.cpp", "d2/d86/EmpiricalCDF_8cpp.html", "d2/d86/EmpiricalCDF_8cpp" ],
    [ "EmpiricalCDF.hpp", "df/d03/EmpiricalCDF_8hpp.html", [
      [ "EmpiricalCDF", "d2/dfc/classEmpiricalCDF.html", "d2/dfc/classEmpiricalCDF" ],
      [ "Element", "d9/df3/structEmpiricalCDF_1_1Element.html", "d9/df3/structEmpiricalCDF_1_1Element" ]
    ] ],
    [ "EnergyToFluence.cpp", "d0/d31/EnergyToFluence_8cpp.html", null ],
    [ "EnergyToFluence.hpp", "d8/d33/EnergyToFluence_8hpp.html", [
      [ "SpatialMap", "df/d80/classSpatialMap.html", "df/d80/classSpatialMap" ],
      [ "EnergyToFluence", "de/dee/classEnergyToFluence.html", "de/dee/classEnergyToFluence" ],
      [ "Powers", "d7/d79/structEnergyToFluence_1_1Powers.html", "d7/d79/structEnergyToFluence_1_1Powers" ]
    ] ],
    [ "EventCountCheck.hpp", "d7/dfd/EventCountCheck_8hpp.html", null ],
    [ "EventCountComparison.cpp", "d9/da9/EventCountComparison_8cpp.html", null ],
    [ "EventCountComparison.hpp", "d9/d45/EventCountComparison_8hpp.html", [
      [ "StatisticalTest", "d1/de0/classStatisticalTest.html", "d1/de0/classStatisticalTest" ],
      [ "Chi2StatisticalTest", "d5/dee/classChi2StatisticalTest.html", "d5/dee/classChi2StatisticalTest" ],
      [ "EventCountComparison", "d3/d34/classEventCountComparison.html", "d3/d34/classEventCountComparison" ]
    ] ],
    [ "LayeredAccumulationProbe.cpp", "d0/d8d/LayeredAccumulationProbe_8cpp.html", null ],
    [ "LayeredAccumulationProbe.hpp", "d9/dfd/LayeredAccumulationProbe_8hpp.html", [
      [ "SpatialMap", "df/d80/classSpatialMap.html", "df/d80/classSpatialMap" ],
      [ "LayeredAccumulationProbe", "db/dd3/classLayeredAccumulationProbe.html", "db/dd3/classLayeredAccumulationProbe" ]
    ] ],
    [ "PercentDifference.cpp", "d7/dd2/PercentDifference_8cpp.html", null ],
    [ "PercentDifference.hpp", "d7/d71/PercentDifference_8hpp.html", [
      [ "PercentDifference", "d9/d50/classPercentDifference.html", "d9/d50/classPercentDifference" ]
    ] ],
    [ "Permute.cpp", "de/d67/Permute_8cpp.html", null ],
    [ "Permute.hpp", "d7/d4f/Permute_8hpp.html", [
      [ "Permute", "d3/dd4/classPermute.html", "d3/dd4/classPermute" ]
    ] ],
    [ "PointMatcher.cpp", "d5/d83/PointMatcher_8cpp.html", null ],
    [ "PointMatcher.hpp", "df/d43/PointMatcher_8hpp.html", [
      [ "PointMatcher", "da/d86/classPointMatcher.html", "da/d86/classPointMatcher" ]
    ] ],
    [ "Rescale.cpp", "d4/d92/Rescale_8cpp.html", null ],
    [ "Rescale.hpp", "d7/d8f/Rescale_8hpp.html", [
      [ "Rescale", "dc/d36/classRescale.html", "dc/d36/classRescale" ]
    ] ],
    [ "Sort.cpp", "d3/d06/Sort_8cpp.html", null ],
    [ "Sort.hpp", "de/d22/Sort_8hpp.html", [
      [ "Sort", "d3/da9/classSort.html", "d3/da9/classSort" ]
    ] ],
    [ "SpatialMapOperator.cpp", "dd/dd4/SpatialMapOperator_8cpp.html", null ],
    [ "SpatialMapOperator.hpp", "da/daa/SpatialMapOperator_8hpp.html", [
      [ "SpatialMapOperator", "df/d96/classSpatialMapOperator.html", "df/d96/classSpatialMapOperator" ]
    ] ],
    [ "SurfaceAccumulationProbe.cpp", "df/db2/SurfaceAccumulationProbe_8cpp.html", null ],
    [ "SurfaceAccumulationProbe.hpp", "d8/dc3/SurfaceAccumulationProbe_8hpp.html", [
      [ "SpatialMap", "df/d80/classSpatialMap.html", "df/d80/classSpatialMap" ],
      [ "SurfaceAccumulationProbe", "dc/da6/classSurfaceAccumulationProbe.html", "dc/da6/classSurfaceAccumulationProbe" ]
    ] ],
    [ "UnitConverter.hpp", "db/d07/UnitConverter_8hpp.html", "db/d07/UnitConverter_8hpp" ],
    [ "VolumeAccumulationProbe.cpp", "d8/d5d/VolumeAccumulationProbe_8cpp.html", null ],
    [ "VolumeAccumulationProbe.hpp", "d6/d93/VolumeAccumulationProbe_8hpp.html", [
      [ "SpatialMap", "df/d80/classSpatialMap.html", "df/d80/classSpatialMap" ],
      [ "VolumeAccumulationProbe", "d2/dee/classVolumeAccumulationProbe.html", "d2/dee/classVolumeAccumulationProbe" ]
    ] ],
    [ "ZScoresComparison.cpp", "d6/df6/ZScoresComparison_8cpp.html", null ],
    [ "ZScoresComparison.hpp", "d3/d5e/ZScoresComparison_8hpp.html", [
      [ "ZScoresComparison", "df/df4/classZScoresComparison.html", "df/df4/classZScoresComparison" ]
    ] ]
];