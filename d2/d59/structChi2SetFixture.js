var structChi2SetFixture =
[
    [ "leftSet", "d2/d59/structChi2SetFixture.html#ac15931ba7db890dbda26e396d4e8ea8a", null ],
    [ "run", "d2/d59/structChi2SetFixture.html#ad3c6264c669817dd910be752803917a4", null ],
    [ "m_leftSet", "d2/d59/structChi2SetFixture.html#aa51297ece201252270c045deb484a280", null ],
    [ "m_pCrit", "d2/d59/structChi2SetFixture.html#a7dc7dda91ebaf5f06aabd83b367eb6d1", null ],
    [ "m_rejects", "d2/d59/structChi2SetFixture.html#af8a54dac58c17f5dd5893645670a3e85", null ],
    [ "m_runs", "d2/d59/structChi2SetFixture.html#a054db2953697588dd8547613289f9c20", null ],
    [ "m_tester", "d2/d59/structChi2SetFixture.html#a4a6a3565f2e0d29f903787ba9f3fa750", null ]
];