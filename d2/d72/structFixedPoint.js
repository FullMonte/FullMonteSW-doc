var structFixedPoint =
[
    [ "Base", "d2/d72/structFixedPoint.html#a004b59740301e9bfc51d8d222746e554", null ],
    [ "value_type", "d2/d72/structFixedPoint.html#ab14f6812ae4fcc9d572b678fe6c9246c", null ],
    [ "FixedPoint", "d2/d72/structFixedPoint.html#ac31a0058075cae76c1a5fed96116acaf", null ],
    [ "FixedPoint", "d2/d72/structFixedPoint.html#a535f33289c4845ad2f87c9ed7f8b2594", null ],
    [ "FixedPoint", "d2/d72/structFixedPoint.html#a404ee827ce876261588970569e49dfc5", null ],
    [ "FixedPoint", "d2/d72/structFixedPoint.html#af378c84e98feef2c300adddff2e1388a", null ],
    [ "BOOST_STATIC_ASSERT_MSG", "d2/d72/structFixedPoint.html#a9231b241b88463d69b87245d4b1e95f8", null ],
    [ "epsilon", "d2/d72/structFixedPoint.html#a39a566e5bb6c66d4487d9bb591f9155b", null ],
    [ "equals", "d2/d72/structFixedPoint.html#a094fa02847939c6c7220742bfb020d4d", null ],
    [ "integer_value", "d2/d72/structFixedPoint.html#a6a0aa0d38a570c7facbd88391a1a16cf", null ],
    [ "operator double", "d2/d72/structFixedPoint.html#a341089e402c2bcb0b05e9d18b1f69ac7", null ],
    [ "operator float", "d2/d72/structFixedPoint.html#a49f9d4082cd34521819d2967a0610ce0", null ],
    [ "serialize", "d2/d72/structFixedPoint.html#a121f195bf60214e3d0f36789661a7c7b", null ],
    [ "value", "d2/d72/structFixedPoint.html#a8aa1c1e899177a0494d968fd3f1fc043", null ],
    [ "eps_", "d2/d72/structFixedPoint.html#aa470dedbbcd26f1fec8ef9bfa6b14b9f", null ],
    [ "maxValue_", "d2/d72/structFixedPoint.html#a131b573836ee905ca5e67a7cb35cde04", null ],
    [ "minValue_", "d2/d72/structFixedPoint.html#a8082548895cd6aa5a43c4d42900b853d", null ]
];