var Scratch_2taylor2_8m =
[
    [ "legend", "d2/de9/Scratch_2taylor2_8m.html#ab721672a8698aa5a39a000e7c9d54d1c", null ],
    [ "plot", "d2/de9/Scratch_2taylor2_8m.html#a51c5ffd6e885d967126647e3cfa6403c", null ],
    [ "plot", "d2/de9/Scratch_2taylor2_8m.html#ab216c855bc48e27bac1e219394aa1fed", null ],
    [ "plot", "d2/de9/Scratch_2taylor2_8m.html#aaf617bc946a77f6bdbf34dd29f8599a6", null ],
    [ "title", "d2/de9/Scratch_2taylor2_8m.html#a180bc972ab7c6e14436fa10cee378e38", null ],
    [ "xlabel", "d2/de9/Scratch_2taylor2_8m.html#a00fe093ccc1c704788889a1b49bdf388", null ],
    [ "ylabel", "d2/de9/Scratch_2taylor2_8m.html#a14525da491e7716f1699ce8b6bd305ff", null ],
    [ "dx", "d2/de9/Scratch_2taylor2_8m.html#aacddc911cdfe5cd5ec97b084754542d4", null ],
    [ "figure", "d2/de9/Scratch_2taylor2_8m.html#a391e34f2de441d79152a7b3d6e4c9c86", null ],
    [ "log2_ref", "d2/de9/Scratch_2taylor2_8m.html#a1ac598b7901491b6939ef529fabeba03", null ],
    [ "on", "d2/de9/Scratch_2taylor2_8m.html#a58ab1fd68e97078232808206b850161b", null ],
    [ "x", "d2/de9/Scratch_2taylor2_8m.html#a9336ebf25087d91c818ee6e9ec29f8c1", null ],
    [ "x0", "d2/de9/Scratch_2taylor2_8m.html#aa48da42c617fdb7cf84e9a3f80aa04e8", null ],
    [ "y", "d2/de9/Scratch_2taylor2_8m.html#a2fb1c5cf58867b5bbc9a1b145a86f3a0", null ]
];