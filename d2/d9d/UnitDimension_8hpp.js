var UnitDimension_8hpp =
[
    [ "DimensionUnit", "d2/d9d/UnitDimension_8hpp.html#ac291dc90fb2c10da0e7ca79fb76fafa9", [
      [ "NONE", "d2/d9d/UnitDimension_8hpp.html#ac291dc90fb2c10da0e7ca79fb76fafa9ac157bdf0b85a40d2619cbc8bc1ae5fe2", null ],
      [ "m", "d2/d9d/UnitDimension_8hpp.html#ac291dc90fb2c10da0e7ca79fb76fafa9aca3cf2ca9e0e7f4e96f2bbc362b9353a", null ],
      [ "cm", "d2/d9d/UnitDimension_8hpp.html#ac291dc90fb2c10da0e7ca79fb76fafa9a298e66f2fa17989b011c1d6d2a32856c", null ],
      [ "mm", "d2/d9d/UnitDimension_8hpp.html#ac291dc90fb2c10da0e7ca79fb76fafa9a9b2d7688fc6394a3f52163b317043936", null ]
    ] ]
];