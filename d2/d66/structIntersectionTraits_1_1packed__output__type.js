var structIntersectionTraits_1_1packed__output__type =
[
    [ "serialize", "d2/d66/structIntersectionTraits_1_1packed__output__type.html#a825c31764dec90a8bca4f4aea775c4c4", null ],
    [ "boost::serialization::access", "d2/d66/structIntersectionTraits_1_1packed__output__type.html#ac98d07dd8f7b70e16ccb9a01abf56b9c", null ],
    [ "bits", "d2/d66/structIntersectionTraits_1_1packed__output__type.html#a4704fa45ee721756205f3c1479cbca04", null ],
    [ "bypass", "d2/d66/structIntersectionTraits_1_1packed__output__type.html#a65f098dba76bccfb10113513dcf666ad", null ],
    [ "costheta", "d2/d66/structIntersectionTraits_1_1packed__output__type.html#ad69ddd4c12aff215fca46dec1907f4a6", null ],
    [ "height", "d2/d66/structIntersectionTraits_1_1packed__output__type.html#a04b1aa4296afebdebb8df93c9156fbca", null ],
    [ "IDi", "d2/d66/structIntersectionTraits_1_1packed__output__type.html#a454ce06a4fe71f36c73ddbfb3c94b0c9", null ],
    [ "IDt", "d2/d66/structIntersectionTraits_1_1packed__output__type.html#a422ea15b1bf58a87cf190babae669464", null ],
    [ "idx", "d2/d66/structIntersectionTraits_1_1packed__output__type.html#aad2fa0ceeb4f148e9a5ae8e3054ef166", null ],
    [ "resultCode", "d2/d66/structIntersectionTraits_1_1packed__output__type.html#a59f7b5eca59ebec94f3590f3bbed2799", null ]
];