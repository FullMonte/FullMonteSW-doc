var classMultiThreadWithIndividualCopy =
[
    [ "Logger", "classMultiThreadWithIndividualCopy_1_1Logger.html", "classMultiThreadWithIndividualCopy_1_1Logger" ],
    [ "SingleThreadLogger", "classMultiThreadWithIndividualCopy.html#a2483ca81622b92f6d1a1efb13315cc0d", null ],
    [ "State", "classMultiThreadWithIndividualCopy.html#a921039de7dfacdd94aa6898cf17691ba", null ],
    [ "MultiThreadWithIndividualCopy", "classMultiThreadWithIndividualCopy.html#af9debe4ec046cc32a05be7c7f820724a", null ],
    [ "clear", "classMultiThreadWithIndividualCopy.html#afae5d9e3ad79adb9c6e80d22af6f5a24", null ],
    [ "createLogger", "classMultiThreadWithIndividualCopy.html#a1446b76da0bbc822d1deaba549d03513", null ],
    [ "merge", "classMultiThreadWithIndividualCopy.html#a6cc5523228b2f3504e2ab2238f2f5525", null ],
    [ "postResults", "classMultiThreadWithIndividualCopy.html#a80f93ca8b0d937c97add0b6d7deccf1a", null ],
    [ "prepare", "classMultiThreadWithIndividualCopy.html#ac6adeb38d617d2aeeb13d43f030d3454", null ]
];