var classMaterial =
[
    [ "Material", "classMaterial.html#a137e987401b63eb7c6c27c3e38bc74b5", null ],
    [ "Material", "classMaterial.html#a824c9cf037bf0ab881b7d31ca1d16f9f", null ],
    [ "~Material", "classMaterial.html#a2c19452d71f54075df8f5405b03129f4", null ],
    [ "absorptionCoeff", "classMaterial.html#ac6c87b1b022957902e93ab44952606d6", null ],
    [ "absorptionCoeff", "classMaterial.html#aba80e73c56a2f1c90499bd4afe72d9d7", null ],
    [ "anisotropy", "classMaterial.html#aaa5ea324529a978f5f120d4f1a98ace7", null ],
    [ "anisotropy", "classMaterial.html#af2f12b37f63761d9728539b30366a4b4", null ],
    [ "reducedScatteringCoeff", "classMaterial.html#a785551288a3ff63d9dee79a21ae81f4f", null ],
    [ "reducedScatteringCoeff", "classMaterial.html#a6136576de7d9ca6b768a45b2f3fb2099", null ],
    [ "reducedScatteringCoeffWithG", "classMaterial.html#a114d8d5e2b3b7485a17dce1ae3329e1d", null ],
    [ "refractiveIndex", "classMaterial.html#a1d086a56433324f66d276a95cec87132", null ],
    [ "refractiveIndex", "classMaterial.html#a78bf1c20372210f93d80fe9c29a1eea3", null ],
    [ "scatteringCoeff", "classMaterial.html#a5d4f305dc3e0a1f25ada38c5649067a7", null ],
    [ "scatteringCoeff", "classMaterial.html#a63bd8bdb00ef0ce6edeb0bac33689edb", null ]
];