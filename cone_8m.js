var cone_8m =
[
    [ "plot3", "cone_8m.html#a36c363e7ee64e1d94e6b02c933eac606", null ],
    [ "set", "cone_8m.html#a2ef65c49d36e0bec2feffed4f6ab72e4", null ],
    [ "cosphi", "cone_8m.html#a77ac09e4c18f08dd13b5820fe322290e", null ],
    [ "costheta", "cone_8m.html#a7c71162fdc7b6f34c540fcbbcef2b45c", null ],
    [ "N", "cone_8m.html#a8cc2e7240164328fdc3f0e5e21032c56", null ],
    [ "phi", "cone_8m.html#aaf40c223796354fb6b79c9ed047c513a", null ],
    [ "rnd", "cone_8m.html#a82b55e95b097bf7947bbfab550696d80", null ],
    [ "sinphi", "cone_8m.html#a2de6d18dd55109c414e0b34da1d301b4", null ],
    [ "sintheta", "cone_8m.html#a8197006c192457438d6f3ed0898aea15", null ],
    [ "theta_max", "cone_8m.html#a3e52e604725c21acb00bbe506ae0a442", null ]
];