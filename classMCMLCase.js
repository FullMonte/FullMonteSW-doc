var classMCMLCase =
[
    [ "MCMLCase", "classMCMLCase.html#a04bccca25a750a2b6ba7e7ba55cfc8bc", null ],
    [ "~MCMLCase", "classMCMLCase.html#a072be4ccc234e85fc6927feb7b0ea575", null ],
    [ "geometry", "classMCMLCase.html#acf560d4989df48c9ba9e37d796dddcce", null ],
    [ "geometry", "classMCMLCase.html#a749b3445bd688533da254717f5dae6fc", null ],
    [ "materials", "classMCMLCase.html#ace7d05f2bbbcc07eba879a0227ffb7db", null ],
    [ "materials", "classMCMLCase.html#a35de840465efeba54045e9ae72e91b0e", null ],
    [ "outputFilename", "classMCMLCase.html#ab775d6d66f166edeaa8b0baf0f3d3cff", null ],
    [ "outputFilename", "classMCMLCase.html#a471f71f1904a6b2893f5f1e4aadb007a", null ],
    [ "outputFormat", "classMCMLCase.html#a2e606f708050e861d45ec9274192a45c", null ],
    [ "packets", "classMCMLCase.html#adb73a3a319667b441254be7c0d07dd51", null ],
    [ "packets", "classMCMLCase.html#a156b21085bca78a44b8213f64534d810", null ],
    [ "rouletteThreshold", "classMCMLCase.html#a8ffe2138dca59d641f5fea80424ec36a", null ],
    [ "rouletteThreshold", "classMCMLCase.html#acfa5730dde248766ca316058a14ead12", null ]
];