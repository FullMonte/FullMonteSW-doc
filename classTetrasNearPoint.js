var classTetrasNearPoint =
[
    [ "TetrasNearPoint", "classTetrasNearPoint.html#ab42308504dc5d450cfc13e287466d892", null ],
    [ "~TetrasNearPoint", "classTetrasNearPoint.html#ac0ad95c28a341e82e2c5d64424a53add", null ],
    [ "centre", "classTetrasNearPoint.html#af507db6d93b6bb77f4b79b08c396dd80", null ],
    [ "containingTetra", "classTetrasNearPoint.html#a7578e1c7dbf96b5dd0ad647ec51cba70", null ],
    [ "currentTetraID", "classTetrasNearPoint.html#a3c6fbc477ebefe076663c5a39bd2cea6", null ],
    [ "done", "classTetrasNearPoint.html#af41352f878f8eafb509151c7979ed12c", null ],
    [ "mesh", "classTetrasNearPoint.html#a835641c71a85b83dc413a4b5a85e59f6", null ],
    [ "next", "classTetrasNearPoint.html#aba23c1df5d4d3a01985ae393dee029f0", null ],
    [ "radius", "classTetrasNearPoint.html#a4c0d28f1f049849947d3a9a455b39f8b", null ],
    [ "start", "classTetrasNearPoint.html#a9bf269aa932d72d4ceb99f9d826679e1", null ]
];