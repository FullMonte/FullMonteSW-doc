var classTextFile =
[
    [ "Mode", "classTextFile.html#a3b44a3f2222fca697e8550cc29340c54", [
      [ "Surface", "classTextFile.html#a3b44a3f2222fca697e8550cc29340c54ab2d0574e8c71e14fbe4f847af4201105", null ],
      [ "Volume", "classTextFile.html#a3b44a3f2222fca697e8550cc29340c54a5afe06a3c575bb2cdeae0aeffa8c32fa", null ]
    ] ],
    [ "TextFile", "classTextFile.html#ad2d429d0e838d57d0cfc67f515827266", null ],
    [ "~TextFile", "classTextFile.html#abb8ce39394fd4750aeea1af871b8204f", null ],
    [ "fileName", "classTextFile.html#af5f0277735cf5049e8cf48a395c6a566", null ],
    [ "fileName", "classTextFile.html#a707e6075b72c028490691c49cc419474", null ],
    [ "mesh", "classTextFile.html#a3feec62518bbb4229bff45993ce757a6", null ],
    [ "mesh", "classTextFile.html#af9f438197668dc6393addab5c1e76eab", null ],
    [ "mode", "classTextFile.html#a9e561c9a3823a11df8fd7a988fdfedee", null ],
    [ "mode", "classTextFile.html#a0a82577479d8e9f38a389a5e006122b0", null ]
];