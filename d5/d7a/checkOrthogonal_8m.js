var checkOrthogonal_8m =
[
    [ "checkOrthogonal", "d5/d7a/checkOrthogonal_8m.html#aa838d5d7bcd5f515a5767eed6dff125f", null ],
    [ "crit", "d5/d7a/checkOrthogonal_8m.html#a782349f85c537574c559a024f893ef20", null ],
    [ "hist", "d5/d7a/checkOrthogonal_8m.html#a48e8e3960bee03665703e8bd780eabdb", null ],
    [ "if", "d5/d7a/checkOrthogonal_8m.html#ac409edade10ddb898ed7853585c74e9b", null ],
    [ "length", "d5/d7a/checkOrthogonal_8m.html#adf50a9a871d8c96fc0e59b323446198c", null ],
    [ "title", "d5/d7a/checkOrthogonal_8m.html#a5fd6708276debd0ff55f8e99a2eb335a", null ],
    [ "vector", "d5/d7a/checkOrthogonal_8m.html#a227b0f47d042529d0bd963226f8e6c65", null ],
    [ "while", "d5/d7a/checkOrthogonal_8m.html#a30b257781a21b8e9c1c5dc05dd4252b7", null ],
    [ "eps", "d5/d7a/checkOrthogonal_8m.html#adc2185da0dc4b67332d77d61d41d56c5", null ],
    [ "figure", "d5/d7a/checkOrthogonal_8m.html#ac55f65f70118ee46607ae7bbb88342b8", null ],
    [ "i", "d5/d7a/checkOrthogonal_8m.html#af7e2633f170bb0c25e20469ea6bc9d00", null ],
    [ "n_eps", "d5/d7a/checkOrthogonal_8m.html#ab43d174a089fe3705111c16c57760206", null ],
    [ "n_outside", "d5/d7a/checkOrthogonal_8m.html#af67f5d00052b81cea2a4899c2c184fc6", null ]
];