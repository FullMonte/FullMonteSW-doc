var classTagManager =
[
    [ "is_in_use", "d5/d53/classTagManager.html#ae1659e675d5d607e49c7b2abf831c078", null ],
    [ "release_tag", "d5/d53/classTagManager.html#af9bc9afad4bb62900449384c835f52a5", null ],
    [ "release_tag", "d5/d53/classTagManager.html#a8efd7884e4edd8aa93c6b3cf0dbd89df", null ],
    [ "request_tag", "d5/d53/classTagManager.html#a51f0251f3eda4fe420b18bd071b9ca25", null ],
    [ "reset", "d5/d53/classTagManager.html#a6bf53746bcd9b420aa942a6166e12a93", null ],
    [ "set_max_credits", "d5/d53/classTagManager.html#af377d5751e997ac07fba72d32bf51025", null ],
    [ "max_credits", "d5/d53/classTagManager.html#ac74bfd7a865dc6f401057cf059bc8527", null ],
    [ "num_credits", "d5/d53/classTagManager.html#a5b8a289a560af49f2ddee2367afdf69e", null ],
    [ "tags_in_use", "d5/d53/classTagManager.html#a5e8f3aade8d9708b2a7917941e3bf739", null ]
];