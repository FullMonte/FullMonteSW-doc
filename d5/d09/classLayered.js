var classLayered =
[
    [ "Layered", "d5/d09/classLayered.html#a6a9d25d4d62c7dc21f93b8384c4f91ce", null ],
    [ "~Layered", "d5/d09/classLayered.html#a1613e249edbdb4aaa79674ae8f61606b", null ],
    [ "addLayer", "d5/d09/classLayered.html#aa0a136330d93253f265502d04f46d5c7", null ],
    [ "angularBins", "d5/d09/classLayered.html#ab0b0629d3865ffc5721f5a4144cc994b", null ],
    [ "angularBins", "d5/d09/classLayered.html#ac860d23c5c9f8d565daa672ee8711d5d", null ],
    [ "angularResolution", "d5/d09/classLayered.html#a3785128377801a2e85940cb2230922a0", null ],
    [ "binFromIndex", "d5/d09/classLayered.html#a3f5164c73b5da3210592ac788d50fbc8", null ],
    [ "bins", "d5/d09/classLayered.html#a56b2b9b34243e103e7568ddb995c7678", null ],
    [ "binVolume", "d5/d09/classLayered.html#a2ec91642bd823cdc5a345de78fa5f259", null ],
    [ "binVolume", "d5/d09/classLayered.html#aa1aa4638bf2ee8a8bb8cba80b33a6ac7", null ],
    [ "buildRegions", "d5/d09/classLayered.html#a045f2d4d93e13a263bb767493b06eb70", null ],
    [ "dims", "d5/d09/classLayered.html#a19aa7b1f33feba1371f00e5610c83b1e", null ],
    [ "dims", "d5/d09/classLayered.html#a3c091c92d0e1a18375a14c6f85440562", null ],
    [ "directedSurfaceAreas", "d5/d09/classLayered.html#afd1911adba153b54e59c44d568115769", null ],
    [ "elementVolumes", "d5/d09/classLayered.html#a99a24ea6253290eb6ca1ea882bb53651", null ],
    [ "extent", "d5/d09/classLayered.html#a0d2758c8d7891723b717b22a84eca97a", null ],
    [ "extent", "d5/d09/classLayered.html#aafa76649a653b3f9c90355f050304e03", null ],
    [ "indexForBin", "d5/d09/classLayered.html#abcc0f0bb83a22ff3357b61c60b5ccb83", null ],
    [ "layer", "d5/d09/classLayered.html#a9b439193e64144ffe8443646726517d4", null ],
    [ "layerCount", "d5/d09/classLayered.html#abbbdbf03056812a3c3e845a240e77942", null ],
    [ "resolution", "d5/d09/classLayered.html#a87745fbd21f6ccf58a7a95b949490140", null ],
    [ "resolution", "d5/d09/classLayered.html#aa06517aad78cb33c6046f09d70092baf", null ],
    [ "surfaceAreas", "d5/d09/classLayered.html#a56d02371e557ba35cdf9dc59f84e9c09", null ],
    [ "updateMaterials", "d5/d09/classLayered.html#a23b6200a0a09acf96d33a14aca9aa8a7", null ],
    [ "m_dr", "d5/d09/classLayered.html#a94d2812aa79c5f4b90d3f45e4b49e4e8", null ],
    [ "m_dz", "d5/d09/classLayered.html#a8bc41d330e6eb3acca3f597431c076be", null ],
    [ "m_layers", "d5/d09/classLayered.html#ad675af97d13db76159505fb37cdc0848", null ],
    [ "m_materials", "d5/d09/classLayered.html#a09cef3e1f597d84a2e00568d93f5e794", null ],
    [ "m_Na", "d5/d09/classLayered.html#aeced29ad45b67d546c17cd398ec5e101", null ],
    [ "m_Nr", "d5/d09/classLayered.html#a602afb1e66d150c20889090ad106ebdb", null ],
    [ "m_Nz", "d5/d09/classLayered.html#abe07414f63f78f5ec3dd18576996dc7b", null ]
];