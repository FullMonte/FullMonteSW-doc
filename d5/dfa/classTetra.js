var classTetra =
[
    [ "FaceFlags", "d5/dfa/classTetra.html#a06e41e7cfaa3fa479cbcbf0930233011", [
      [ "FluenceScoring", "d5/dfa/classTetra.html#a06e41e7cfaa3fa479cbcbf0930233011a8fb4bf8db195e09c9df4e1f35ace46d5", null ],
      [ "SpecialInterface", "d5/dfa/classTetra.html#a06e41e7cfaa3fa479cbcbf0930233011ae41f1edfdbe20a6097bd18a8c642a8da", null ]
    ] ],
    [ "dots", "d5/dfa/classTetra.html#ae0fadb28953424e120b7699365afda37", null ],
    [ "face_constant", "d5/dfa/classTetra.html#a736b3bd0314b26f153260f521ad2f565", null ],
    [ "face_normal", "d5/dfa/classTetra.html#a64ffb46d2793ed18ce9b4e5218eb5490", null ],
    [ "getFaceFlag", "d5/dfa/classTetra.html#a0a64b318c8d891a98388c772676d0624", null ],
    [ "getIntersection", "d5/dfa/classTetra.html#aec0ee232382b0568ecbd16a78b3bb2b4", null ],
    [ "heights", "d5/dfa/classTetra.html#a0a72820fb4a19e9a31f41fb624aeb595", null ],
    [ "pointWithin", "d5/dfa/classTetra.html#a18dbda6a80b887c074951464bd45a64b", null ],
    [ "setFaceFlag", "d5/dfa/classTetra.html#aec871b726aa0407737a65abc667a0c19", null ],
    [ "printTetra", "d5/dfa/classTetra.html#aaceade84fc88703f755963b7f3786c52", null ],
    [ "__attribute__", "d5/dfa/classTetra.html#a3f10bb901c4adb71899b344bc368a2f1", null ],
    [ "adjTetras", "d5/dfa/classTetra.html#affac3fbbc6e7980e9a38233538263a6d", null ],
    [ "C", "d5/dfa/classTetra.html#ae3d0638eda9ba0e9e2278ed9fd3fb1ac", null ],
    [ "detectorEnclosedIDs", "d5/dfa/classTetra.html#a6a36d6e18185d885a530ce52782253c1", null ],
    [ "faceFlags", "d5/dfa/classTetra.html#acbdcdc753ada5f30070c1b4e91fbbe66", null ],
    [ "IDfds", "d5/dfa/classTetra.html#a06cc973044b02c7976aeb754f46473aa", null ],
    [ "matID", "d5/dfa/classTetra.html#a535991ce3c0c227c8ab0c1de912019e6", null ],
    [ "nx", "d5/dfa/classTetra.html#af0aa42eb9eefa9379d53c90a585c1632", null ],
    [ "ny", "d5/dfa/classTetra.html#a7fc3bc4e6848f07de21d21215a5d68e2", null ],
    [ "nz", "d5/dfa/classTetra.html#ac373d6ed2ad7bda28a2e8135f3a17eac", null ]
];