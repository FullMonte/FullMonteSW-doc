var structcmd =
[
    [ "afu_event", "d5/db7/structcmd.html#aefa08834796912b8c251964b72c3b39f", null ],
    [ "afu_name", "d5/db7/structcmd.html#ab321b7f15f1d822c429aa005f6e8989a", null ],
    [ "buffer_read", "d5/db7/structcmd.html#a2471fccb06ef5429e4b3a4b64abbed05", null ],
    [ "client", "d5/db7/structcmd.html#aeaaa5fa1ea415bc4db18706f43318077", null ],
    [ "credits", "d5/db7/structcmd.html#a51169b3a5616cab2b87d069606046ea7", null ],
    [ "dbg_fp", "d5/db7/structcmd.html#ae4296759ee8aedd1e92f5f3b05b1b09f", null ],
    [ "dbg_id", "d5/db7/structcmd.html#aeb70b71b4494cddecca9269f3438acae", null ],
    [ "irq", "d5/db7/structcmd.html#a4ad169dd885d4959fb354d5df2924945", null ],
    [ "list", "d5/db7/structcmd.html#adb5826eb8d2d87fd56ab1d2e57c9c8c0", null ],
    [ "lock_addr", "d5/db7/structcmd.html#a38498087bc4e8d6c1f08197c0d30f9fa", null ],
    [ "locked", "d5/db7/structcmd.html#aeaf8335a21ffe9794e73d917f4a6df18", null ],
    [ "max_clients", "d5/db7/structcmd.html#a47b79f75f887c4856165fbf6b57304dd", null ],
    [ "mmio", "d5/db7/structcmd.html#a393a1390975723c7c748f0eeafd7480d", null ],
    [ "page_entries", "d5/db7/structcmd.html#aa2cd01427c4a5f57a85962548880172e", null ],
    [ "parms", "d5/db7/structcmd.html#acf6d9912a0d83e5ceffb2fe70a082ee0", null ],
    [ "psl_state", "d5/db7/structcmd.html#a4786bb9e1e0b4fcacf2964079b9b6ea6", null ],
    [ "res_addr", "d5/db7/structcmd.html#a818c8a7f7ee8483174600a5cb0d90535", null ]
];