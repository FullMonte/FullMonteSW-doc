var namespaceSSE =
[
    [ "Scalar", "d1/d20/classSSE_1_1Scalar.html", "d1/d20/classSSE_1_1Scalar" ],
    [ "SSEBase", "df/d6f/classSSE_1_1SSEBase.html", "df/d6f/classSSE_1_1SSEBase" ],
    [ "SSEKernel", "d7/d42/classSSE_1_1SSEKernel.html", "d7/d42/classSSE_1_1SSEKernel" ],
    [ "UnitVector", "d3/d84/classSSE_1_1UnitVector.html", "d3/d84/classSSE_1_1UnitVector" ],
    [ "Vector", "d5/d0a/classSSE_1_1Vector.html", "d5/d0a/classSSE_1_1Vector" ]
];