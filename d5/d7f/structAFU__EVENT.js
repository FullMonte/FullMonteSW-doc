var structAFU__EVENT =
[
    [ "aux1_change", "d5/d7f/structAFU__EVENT.html#acf9eb920443f57880c664224bbf67704", null ],
    [ "aux2_change", "d5/d7f/structAFU__EVENT.html#a9987415b4481721039b2e04f4d20bb56", null ],
    [ "buffer_rdata", "d5/d7f/structAFU__EVENT.html#aff8e5452810ecfe1a14267065862a47d", null ],
    [ "buffer_rdata_valid", "d5/d7f/structAFU__EVENT.html#ac9a991f9aaf8196e4a72311e3906b0bf", null ],
    [ "buffer_read", "d5/d7f/structAFU__EVENT.html#a1d41fc72c10896459847b136aeaa98d7", null ],
    [ "buffer_read_address", "d5/d7f/structAFU__EVENT.html#ac58028fe12798754b64e1fea9c6a8175", null ],
    [ "buffer_read_latency", "d5/d7f/structAFU__EVENT.html#a0dae07434b5608705a54c564b994dbc3", null ],
    [ "buffer_read_length", "d5/d7f/structAFU__EVENT.html#a25f33d79c69cfe1b46b93808acc00f5f", null ],
    [ "buffer_read_tag", "d5/d7f/structAFU__EVENT.html#af074ceb597045d40862d2bae190b0b26", null ],
    [ "buffer_read_tag_parity", "d5/d7f/structAFU__EVENT.html#af184efc54df3a82e620718679c9721ec", null ],
    [ "buffer_rparity", "d5/d7f/structAFU__EVENT.html#ad27732fe3243e1fa77cad68486ba9efc", null ],
    [ "buffer_wdata", "d5/d7f/structAFU__EVENT.html#ac0e3551b13bfd85d7aaec7c1210d310c", null ],
    [ "buffer_wparity", "d5/d7f/structAFU__EVENT.html#aca3caa90d43837f029322880cc71a84e", null ],
    [ "buffer_write", "d5/d7f/structAFU__EVENT.html#ab4cb27a765f8ed4b1d3e1f9f00840249", null ],
    [ "buffer_write_address", "d5/d7f/structAFU__EVENT.html#a113a2e0b70fa63b85cab443a638f83f7", null ],
    [ "buffer_write_length", "d5/d7f/structAFU__EVENT.html#af46b9dd82af8a0dc420ebb4cb48cff11", null ],
    [ "buffer_write_tag", "d5/d7f/structAFU__EVENT.html#a90beffdce8d47b2eb8e0e1a81083cfa0", null ],
    [ "buffer_write_tag_parity", "d5/d7f/structAFU__EVENT.html#ae3697a9c5a2a7ee7fb022aa1cb5e028e", null ],
    [ "cache_position", "d5/d7f/structAFU__EVENT.html#afc055d5679426cc02314928d647580f9", null ],
    [ "cache_state", "d5/d7f/structAFU__EVENT.html#acb25a3f76ac47c564114b00cd7097e6c", null ],
    [ "clock", "d5/d7f/structAFU__EVENT.html#abd1be1b809e623bfae320152ead36964", null ],
    [ "command_abort", "d5/d7f/structAFU__EVENT.html#adb361e68b64611bcdf3ff1127d4f363b", null ],
    [ "command_address", "d5/d7f/structAFU__EVENT.html#a49eb142c2429fb1b71a10a774e9ecc6b", null ],
    [ "command_address_parity", "d5/d7f/structAFU__EVENT.html#a226372a20a3d54789a7698fdfcab682c", null ],
    [ "command_code", "d5/d7f/structAFU__EVENT.html#afa90e9139b9152ff96ec4b3a00358ffe", null ],
    [ "command_code_parity", "d5/d7f/structAFU__EVENT.html#a430b4602aaebde6e9e786bdea505c564", null ],
    [ "command_handle", "d5/d7f/structAFU__EVENT.html#ad7ec7a5c4c8289803851451659952785", null ],
    [ "command_size", "d5/d7f/structAFU__EVENT.html#a1ee9259f36f76c1d46eecb782353040d", null ],
    [ "command_tag", "d5/d7f/structAFU__EVENT.html#abc4ba7e1c9155c1b7bad93746a4627e0", null ],
    [ "command_tag_parity", "d5/d7f/structAFU__EVENT.html#a575a4c61b797023f2738aa6534cb1dad", null ],
    [ "command_valid", "d5/d7f/structAFU__EVENT.html#ad49e75bb146c8f0f9f6d461a75cb0edc", null ],
    [ "credits", "d5/d7f/structAFU__EVENT.html#a7ce103b34fc700ff388e899d1b3db1b0", null ],
    [ "job_address", "d5/d7f/structAFU__EVENT.html#a532654e34eef3189dbe9f224e292099b", null ],
    [ "job_address_parity", "d5/d7f/structAFU__EVENT.html#ac87bc139a4996e010d827d7806eafa46", null ],
    [ "job_cack_llcmd", "d5/d7f/structAFU__EVENT.html#ac3421038f7d7a6c8b066ae4e4dc323b9", null ],
    [ "job_code", "d5/d7f/structAFU__EVENT.html#a827967b880e52583d82760bd7d5cf9a0", null ],
    [ "job_code_parity", "d5/d7f/structAFU__EVENT.html#a500f0ad0f8a46b472d123c58bb75f4aa", null ],
    [ "job_done", "d5/d7f/structAFU__EVENT.html#affd61d85cbe233b5870ae39146d2759f", null ],
    [ "job_error", "d5/d7f/structAFU__EVENT.html#ae197afba19aeaf63fc7043b29fe2b190", null ],
    [ "job_running", "d5/d7f/structAFU__EVENT.html#a1ed50b8cbe134f0b5449fdf0e21cce6f", null ],
    [ "job_valid", "d5/d7f/structAFU__EVENT.html#a7d08b0ac589207f95e6b39bb46dcfb5d", null ],
    [ "job_yield", "d5/d7f/structAFU__EVENT.html#a4d9bbf101030f61ddcdf1cf6c9bb58e0", null ],
    [ "mmio_ack", "d5/d7f/structAFU__EVENT.html#afe6665e78d66009cd2a67b061134b363", null ],
    [ "mmio_address", "d5/d7f/structAFU__EVENT.html#a617c993b1650adb6f7e2ae18304de513", null ],
    [ "mmio_address_parity", "d5/d7f/structAFU__EVENT.html#a716bcdd41ad6789c2d104b97da5b6546", null ],
    [ "mmio_afudescaccess", "d5/d7f/structAFU__EVENT.html#aba9c63ac6d0ca797d3ae3090e70733f2", null ],
    [ "mmio_double", "d5/d7f/structAFU__EVENT.html#a56e1a73c935788fc7de29cd3c98f7602", null ],
    [ "mmio_rdata", "d5/d7f/structAFU__EVENT.html#a21649b6b27b4e0f3c902b9ca96b1cc9d", null ],
    [ "mmio_rdata_parity", "d5/d7f/structAFU__EVENT.html#a4ed93fc9723ae216c44df3afe0ab0d74", null ],
    [ "mmio_read", "d5/d7f/structAFU__EVENT.html#a8438d0f2f1e0703f3c8ddff58bf4a47c", null ],
    [ "mmio_valid", "d5/d7f/structAFU__EVENT.html#a4e8cf3d7eee10ebcc6150475e532c3b1", null ],
    [ "mmio_wdata", "d5/d7f/structAFU__EVENT.html#a93ab6aaa1b7dc93b15ddd04dae36d6cb", null ],
    [ "mmio_wdata_parity", "d5/d7f/structAFU__EVENT.html#a11eb5ab8264d8824a8550b650e5eed1c", null ],
    [ "parity_enable", "d5/d7f/structAFU__EVENT.html#a0217dd47f23eec76d87beaa75e28f1aa", null ],
    [ "proto_primary", "d5/d7f/structAFU__EVENT.html#ae1a94e1587bf4ada8488b72190826eca", null ],
    [ "proto_secondary", "d5/d7f/structAFU__EVENT.html#a4492fe89386acb3cbf2e86a6076879a0", null ],
    [ "proto_tertiary", "d5/d7f/structAFU__EVENT.html#aaa26caa4f4814a228e213a3e45ba36f4", null ],
    [ "rbp", "d5/d7f/structAFU__EVENT.html#adda2ee162c086c40f82a16f4f8743240", null ],
    [ "rbuf", "d5/d7f/structAFU__EVENT.html#a28e78a4676c36debba75ba072c44ab63", null ],
    [ "response_code", "d5/d7f/structAFU__EVENT.html#ab2d1916a8ebe7f9796acb08697086454", null ],
    [ "response_tag", "d5/d7f/structAFU__EVENT.html#a03761d67e202f30e7e57fc80d76f087d", null ],
    [ "response_tag_parity", "d5/d7f/structAFU__EVENT.html#a597effbaa4f16bb787860fa6affad727", null ],
    [ "response_valid", "d5/d7f/structAFU__EVENT.html#a6807d9e73cdea56c93f97e5fde3930e7", null ],
    [ "room", "d5/d7f/structAFU__EVENT.html#aa449b7b58f4fa3a7b43a309331c4d2c4", null ],
    [ "sockfd", "d5/d7f/structAFU__EVENT.html#a945c167a21454c376606cb466c419c97", null ],
    [ "tbuf", "d5/d7f/structAFU__EVENT.html#adf174f97c033f281213b201436d442a4", null ],
    [ "timebase_request", "d5/d7f/structAFU__EVENT.html#a99e948d838236fb3f2eaac8c7589dbd6", null ]
];