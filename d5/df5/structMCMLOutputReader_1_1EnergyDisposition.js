var structMCMLOutputReader_1_1EnergyDisposition =
[
    [ "absorption", "d5/df5/structMCMLOutputReader_1_1EnergyDisposition.html#a48d79cd36ab10acd5be5c98db67c1499", null ],
    [ "diffuseReflectance", "d5/df5/structMCMLOutputReader_1_1EnergyDisposition.html#a1cfed2dc94e578b901633dc8abed942c", null ],
    [ "specularReflectance", "d5/df5/structMCMLOutputReader_1_1EnergyDisposition.html#a3cc311186977a5cbfac518e0924c834f", null ],
    [ "transmission", "d5/df5/structMCMLOutputReader_1_1EnergyDisposition.html#ab42561e70a52be370d3f7d0a2862e427", null ]
];