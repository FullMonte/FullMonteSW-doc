var classDescriptor =
[
    [ "Descriptor", "d5/d44/classDescriptor.html#ab068c5cd657ad940f491982f2de2c577", null ],
    [ "get_AFU_CR_len", "d5/d44/classDescriptor.html#ac93ea8a3635986a4c219297c6d643dc4", null ],
    [ "get_AFU_CR_offset", "d5/d44/classDescriptor.html#af5fda0368d9b44e486499e0a52072c53", null ],
    [ "get_AFU_EB_len", "d5/d44/classDescriptor.html#a8ff02bdf5721e67d415b99818d8e1785", null ],
    [ "get_AFU_EB_offset", "d5/d44/classDescriptor.html#a729a2922770d8e29795ed8f44b25c5f7", null ],
    [ "get_num_ints_per_process", "d5/d44/classDescriptor.html#a20966ab63012b5fb370817f49d8de1ec", null ],
    [ "get_num_of_afu_CRs", "d5/d44/classDescriptor.html#a90fe2a8feeddf8abf2c42d579f0daf52", null ],
    [ "get_num_of_process", "d5/d44/classDescriptor.html#ad42f79c950ef7f32c4c2e037b0cc596a", null ],
    [ "get_PerProcessPSA_control", "d5/d44/classDescriptor.html#ad58bfe8f7d10867228874f7e10910890", null ],
    [ "get_PerProcessPSA_length", "d5/d44/classDescriptor.html#a7de019354e2b043ffd4e4fff1837cb36", null ],
    [ "get_PerProcessPSA_offset", "d5/d44/classDescriptor.html#a9a69e965a52dde95d638b752eb59a012", null ],
    [ "get_reg", "d5/d44/classDescriptor.html#a2fe27eeb88a6032e8ae0c09311751230", null ],
    [ "get_reg_prog_model", "d5/d44/classDescriptor.html#a2d88d1329d5bb66f39255871059fc07a", null ],
    [ "is_dedicated", "d5/d44/classDescriptor.html#a92ffb83a04e452ea80e79196d94867db", null ],
    [ "is_directed", "d5/d44/classDescriptor.html#a55b283e002631b000177bebc7009b4f5", null ],
    [ "parse_descriptor_file", "d5/d44/classDescriptor.html#a0a171d78a455b418a300dbbd4232c6d3", null ],
    [ "set_jerror", "d5/d44/classDescriptor.html#adc48f153bda6eea9d5e79dfd58a81181", null ],
    [ "to_vector_index", "d5/d44/classDescriptor.html#a88c45bf37ba6162efad39bb7a5461fda", null ],
    [ "regs", "d5/d44/classDescriptor.html#abc0d3560321612f891c90b7008d177dd", null ]
];