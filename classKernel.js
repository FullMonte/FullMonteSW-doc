var classKernel =
[
    [ "Status", "classKernel.html#a17ab0d69d138fd3c14071e2b11923dc8", [
      [ "Idle", "classKernel.html#a17ab0d69d138fd3c14071e2b11923dc8a3a8b1ddb30c1f18d03becb3da3e28ca8", null ],
      [ "Preparing", "classKernel.html#a17ab0d69d138fd3c14071e2b11923dc8a4eeb8e01b9592517df04e178be979859", null ],
      [ "Running", "classKernel.html#a17ab0d69d138fd3c14071e2b11923dc8a113a31ac080a4c9b3c65a73029aa56f8", null ],
      [ "Finished", "classKernel.html#a17ab0d69d138fd3c14071e2b11923dc8ab95d834e1e01c49a0a01ce3140cd888f", null ]
    ] ],
    [ "~Kernel", "classKernel.html#ab0aee98e27b5f821688faad456185b43", null ],
    [ "awaitStatus", "classKernel.html#adcbf3783b3b388ff5671a8d1c9b51d96", null ],
    [ "done", "classKernel.html#a113c6e00986ce567f8ed1bebc9ed0b88", null ],
    [ "finishAsync", "classKernel.html#aa40eef8512a76a60ec85708ddbc5be09", null ],
    [ "geometry", "classKernel.html#ae9aaa15557dfc9c0cd4ec1bc3aba8725", null ],
    [ "geometry", "classKernel.html#a34031ebe606a20a06834c718a7de4f5a", null ],
    [ "materials", "classKernel.html#a04dd95f7a685927de6e96a94fc97ad09", null ],
    [ "materials", "classKernel.html#ab089481837504096c2e92e12deb30686", null ],
    [ "progressFraction", "classKernel.html#a7800eb81cafbcb47a319f8d3536f3330", null ],
    [ "results", "classKernel.html#a8982b8868e135feea4d81e4f1894a35a", null ],
    [ "runSync", "classKernel.html#aa183334091015ea51c16ff382034c77c", null ],
    [ "source", "classKernel.html#a9b0a863406ee817f678f339f4fa9882d", null ],
    [ "source", "classKernel.html#a4da8090044b11096b036f4391baad74a", null ],
    [ "startAsync", "classKernel.html#a50531254d6f3b77117d08fb3f55f7956", null ],
    [ "status", "classKernel.html#ab9a8a259dda8d8118d95ce55c1a14a44", null ],
    [ "updateStatus", "classKernel.html#a8ec18a13112354ed60566c0f34f771c2", null ],
    [ "m_materialSet", "classKernel.html#aaf45bf600f1fdcdbcd90c52190c997de", null ],
    [ "m_results", "classKernel.html#a914715255d9bb7051f833f1e237faaea", null ],
    [ "m_src", "classKernel.html#a88024fabe25c4c0a81a1dc68d2f50817", null ]
];