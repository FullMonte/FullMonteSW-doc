var classBasis =
[
    [ "Basis", "classBasis.html#a5d5d39d14a48a6cbd232bdfabb6430a1", null ],
    [ "Basis", "classBasis.html#ad8346668a1d9fb4b238f3f6513836fd1", null ],
    [ "basis_x", "classBasis.html#a36bb6561c9ce33f456b2a3679e882e79", null ],
    [ "basis_y", "classBasis.html#abb87325af6869d796a97e85e22193c33", null ],
    [ "constant", "classBasis.html#a47ca182038ab231a1ad152a722cc37fb", null ],
    [ "invert", "classBasis.html#a6156f13fd909d7a3070f71e8d274bdcc", null ],
    [ "invert", "classBasis.html#ad1ce1130e5e34dffaf7404d89d12d74b", null ],
    [ "normal", "classBasis.html#ae6ceb48dd5ea533dc58da4b945517250", null ],
    [ "origin", "classBasis.html#a8312caf158a0510f8bf08b9efd8210aa", null ],
    [ "origin", "classBasis.html#a645a4a3ac01b0daed477d9a773408d42", null ],
    [ "project", "classBasis.html#a87698c141c7e5f69b7d19404d0bac085", null ],
    [ "vector", "classBasis.html#a7e820aa60678d1e43941d0de70c2d619", null ]
];