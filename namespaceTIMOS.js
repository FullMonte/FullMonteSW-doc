var namespaceTIMOS =
[
    [ "FaceSource", "structTIMOS_1_1FaceSource.html", "structTIMOS_1_1FaceSource" ],
    [ "GenericSource", "structTIMOS_1_1GenericSource.html", "structTIMOS_1_1GenericSource" ],
    [ "Material", "structTIMOS_1_1Material.html", "structTIMOS_1_1Material" ],
    [ "Optical", "structTIMOS_1_1Optical.html", "structTIMOS_1_1Optical" ],
    [ "PencilBeamSource", "structTIMOS_1_1PencilBeamSource.html", "structTIMOS_1_1PencilBeamSource" ],
    [ "PointSource", "structTIMOS_1_1PointSource.html", "structTIMOS_1_1PointSource" ],
    [ "TetraSource", "structTIMOS_1_1TetraSource.html", "structTIMOS_1_1TetraSource" ]
];