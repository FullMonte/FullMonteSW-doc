var dir_d76c4051082c1f7f0f5964f0c8d5a466 =
[
    [ "Colin27", "dir_9133ae4dd7f3f4b7fe714c33c4c9712c.html", "dir_9133ae4dd7f3f4b7fe714c33c4c9712c" ],
    [ "Digimouse", "dir_c27caaf91fea8dc0e5575493ac3f8c88.html", "dir_c27caaf91fea8dc0e5575493ac3f8c88" ],
    [ "MCML", "dir_5e1c8471e6e14178d82423776ab9363e.html", "dir_5e1c8471e6e14178d82423776ab9363e" ],
    [ "Chi2Check.cpp", "Chi2Check_8cpp.html", "Chi2Check_8cpp" ],
    [ "Chi2Similarity.cpp", "Chi2Similarity_8cpp.html", null ],
    [ "Chi2Similarity.hpp", "Chi2Similarity_8hpp.html", [
      [ "SpatialMap", "classSpatialMap.html", "classSpatialMap" ],
      [ "Chi2Similarity", "classChi2Similarity.html", "classChi2Similarity" ],
      [ "Chi2Entry", "structChi2Similarity_1_1Chi2Entry.html", "structChi2Similarity_1_1Chi2Entry" ]
    ] ],
    [ "Compare.cpp", "Compare_8cpp.html", "Compare_8cpp" ],
    [ "fmdigi.tcl", "fmdigi_8tcl.html", null ],
    [ "vis_layered_compare.cpp", "vis__layered__compare_8cpp.html", "vis__layered__compare_8cpp" ],
    [ "vis_mesh_compare.cpp", "vis__mesh__compare_8cpp.html", "vis__mesh__compare_8cpp" ]
];