var cacheSim_8cpp =
[
    [ "ReplacementStrategy", "dd/dad/cacheSim_8cpp.html#ae468be68a776ccf3f8717399bd518d80", [
      [ "LRU", "dd/dad/cacheSim_8cpp.html#ae468be68a776ccf3f8717399bd518d80a4ade1425886499333b42f8000cf81f61", null ],
      [ "FIFO", "dd/dad/cacheSim_8cpp.html#ae468be68a776ccf3f8717399bd518d80a7795ebef271efe70b28f37deb9a07a83", null ],
      [ "MRU", "dd/dad/cacheSim_8cpp.html#ae468be68a776ccf3f8717399bd518d80a4c48e17e1bf212adffe64446a4770136", null ]
    ] ],
    [ "FirstInFirstOut", "dd/dad/cacheSim_8cpp.html#a09e4c983546cb38c38e64387f25f537a", null ],
    [ "LeastRecentlyUsed", "dd/dad/cacheSim_8cpp.html#a6f674e5f70bad1511f63e312de67245d", null ],
    [ "main", "dd/dad/cacheSim_8cpp.html#a3c04138a5bfe5d72780bb7e82a18e627", null ],
    [ "MostRecentlyUsed", "dd/dad/cacheSim_8cpp.html#a19529548fc503399d745c6790fb9d02a", null ]
];