var classAFU =
[
    [ "AttrGetFail", "d9/d87/classAFU_1_1AttrGetFail.html", "d9/d87/classAFU_1_1AttrGetFail" ],
    [ "InvalidDevice", "d7/de2/classAFU_1_1InvalidDevice.html", "d7/de2/classAFU_1_1InvalidDevice" ],
    [ "MMIOMapFail", "d2/d6e/classAFU_1_1MMIOMapFail.html", "d2/d6e/classAFU_1_1MMIOMapFail" ],
    [ "MMIOUnmapFail", "dd/d4e/classAFU_1_1MMIOUnmapFail.html", "dd/d4e/classAFU_1_1MMIOUnmapFail" ],
    [ "AFU_State", "dd/d9a/classAFU.html#a7f34d7a89cbeff1877cb01c8be5f057b", [
      [ "IDLE", "dd/d9a/classAFU.html#a7f34d7a89cbeff1877cb01c8be5f057bade384ee4910d27d5508e1223f18ee6e9", null ],
      [ "RESET", "dd/d9a/classAFU.html#a7f34d7a89cbeff1877cb01c8be5f057bac925af11fccbf544847420fa96c2854a", null ],
      [ "READY", "dd/d9a/classAFU.html#a7f34d7a89cbeff1877cb01c8be5f057ba0af6d4712aace4e14de28d223bf9f787", null ],
      [ "RUNNING", "dd/d9a/classAFU.html#a7f34d7a89cbeff1877cb01c8be5f057baab82b493ed3bdd7c69c4759b73fa6442", null ],
      [ "WAITING_FOR_LAST_RESPONSES", "dd/d9a/classAFU.html#a7f34d7a89cbeff1877cb01c8be5f057ba9f2645cabfb12c8031a6b796e779d596", null ]
    ] ],
    [ "AFU", "dd/d9a/classAFU.html#aeefffd98708354ca97634e2e75e99754", null ],
    [ "AFU", "dd/d9a/classAFU.html#a14cab668505aa3af8979965573d60d2c", null ],
    [ "AFU", "dd/d9a/classAFU.html#a73e4273e0c4d96d41c6f714534605167", null ],
    [ "AFU", "dd/d9a/classAFU.html#a2807b0d40dd48d86e59ec16330c63d5d", null ],
    [ "AFU", "dd/d9a/classAFU.html#a54ac3c81c0a726981cc57d907597aef3", null ],
    [ "~AFU", "dd/d9a/classAFU.html#a8504ce22897371e20e124cba93d27e87", null ],
    [ "AFU", "dd/d9a/classAFU.html#a8c30ec777e0992fbbebe057d74abd86f", null ],
    [ "~AFU", "dd/d9a/classAFU.html#a8504ce22897371e20e124cba93d27e87", null ],
    [ "await_event", "dd/d9a/classAFU.html#a92745abccff04c574aa0e34f41beac38", null ],
    [ "close", "dd/d9a/classAFU.html#ad49e57c418c1c06447fe7c5b548f0235", null ],
    [ "get_mmio_read_parity", "dd/d9a/classAFU.html#a13257b0a62e36af29b9bb5e51e5ac761", null ],
    [ "mmio_map", "dd/d9a/classAFU.html#aef641960f43d2ad313a323e12b9ef585", null ],
    [ "mmio_read32", "dd/d9a/classAFU.html#a6accc62f02d3e71e2a2a6503eeb05851", null ],
    [ "mmio_read64", "dd/d9a/classAFU.html#a3f8d462fc07a5f45a845141f89516231", null ],
    [ "mmio_unmap", "dd/d9a/classAFU.html#ae82a410fa54f638e133af05c7f375a45", null ],
    [ "mmio_write32", "dd/d9a/classAFU.html#a15311aaf928cddca1e98f95791c8e570", null ],
    [ "mmio_write64", "dd/d9a/classAFU.html#aab4c598627b8a487861d9d57c1e4a89f", null ],
    [ "open", "dd/d9a/classAFU.html#a7463b1df0538b6e6c83e7b4897c48b54", null ],
    [ "open", "dd/d9a/classAFU.html#ad612342185eee46ec2bfacddbf7a4b9c", null ],
    [ "print_details", "dd/d9a/classAFU.html#a0b9d80193f37869ddb5c00649b0bcf5f", null ],
    [ "process_event_blocking", "dd/d9a/classAFU.html#a1e8acd7e1174bdc504166544c71ca464", null ],
    [ "reset", "dd/d9a/classAFU.html#a286be4c69e1892c914fefd5a2f085529", null ],
    [ "reset_machine_controllers", "dd/d9a/classAFU.html#ac27b6873b3130b31f8746024910d86c2", null ],
    [ "resolve_aux1_event", "dd/d9a/classAFU.html#a11b7cf17b7706292c0fdade7af5b73ff", null ],
    [ "resolve_buffer_read_event", "dd/d9a/classAFU.html#a338f1868b2417a39c681e0077ba7f0b2", null ],
    [ "resolve_buffer_write_event", "dd/d9a/classAFU.html#abbe4ac33e4f2c37f942c1dd9cd66852c", null ],
    [ "resolve_control_event", "dd/d9a/classAFU.html#af3daad7fe3f088e5199be5d93ff5a427", null ],
    [ "resolve_mmio_descriptor_event", "dd/d9a/classAFU.html#a7b0589f211003c22faa644898d292b80", null ],
    [ "resolve_mmio_event", "dd/d9a/classAFU.html#ad91a63cb9af095daaa20dc4439f49847", null ],
    [ "resolve_response_event", "dd/d9a/classAFU.html#a0a2c2d2a44ea92d35efb83e61f828dce", null ],
    [ "set_seed", "dd/d9a/classAFU.html#a0ec8e5855a01d82e3251c65c1ac4bf92", null ],
    [ "set_seed", "dd/d9a/classAFU.html#aefc9bdb3f2f3e7cae492f326ac193692", null ],
    [ "start", "dd/d9a/classAFU.html#a10c580df7dd6f3d3cac990d7b6c9a5e3", null ],
    [ "start", "dd/d9a/classAFU.html#a555e50760dfa338e7bc2b1b620a37df3", null ],
    [ "start", "dd/d9a/classAFU.html#a9a2885bbe8e2f13610c4b98a41902da2", null ],
    [ "start", "dd/d9a/classAFU.html#a0e905b93b762c8c3ce2ce9d64e2ff570", null ],
    [ "start", "dd/d9a/classAFU.html#a6ffaca44d0134ebab806c7f817f736ad", null ],
    [ "afu_event", "dd/d9a/classAFU.html#ac0a9799e48ec7cc8de0760261185d246", null ],
    [ "CACHELINE_BYTES", "dd/d9a/classAFU.html#abed3fcbe018ac619ffa60631bca40dc8", null ],
    [ "context_to_mc", "dd/d9a/classAFU.html#a7b181c473d13521b60373229ed44733f", null ],
    [ "descriptor", "dd/d9a/classAFU.html#a4b00356c2f58bf39d669b4b6ee8f978f", null ],
    [ "global_configs", "dd/d9a/classAFU.html#a404a81ed465b4405be88e44cabeda323", null ],
    [ "highest_priority_mc", "dd/d9a/classAFU.html#a58e04a76aeb1b1fb62f07d73cbf29471", null ],
    [ "m_afu_h", "dd/d9a/classAFU.html#aef9c946d5009f075a8e8dcf7c227e426", null ],
    [ "m_devstr", "dd/d9a/classAFU.html#adbe999f84119f600b701e1bb600bb392", null ],
    [ "machine_controller", "dd/d9a/classAFU.html#a3a38b6db4ed2f7d69048c09ba014f443", null ],
    [ "mode_names", "dd/d9a/classAFU.html#a055cda5bc5db31824581c4125fbdb877", null ],
    [ "reset_delay", "dd/d9a/classAFU.html#a1796ba789a09dc7f89038daded24adbe", null ],
    [ "set_jerror_not_run", "dd/d9a/classAFU.html#a91507dd252b02371bc1c67dec9d4835b", null ],
    [ "state", "dd/d9a/classAFU.html#a63e7f2a6bea74fcbc82604d043624af2", null ]
];