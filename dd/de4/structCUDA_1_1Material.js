var structCUDA_1_1Material =
[
    [ "absfrac", "dd/de4/structCUDA_1_1Material.html#a294af554e196ca14db762281693062af", null ],
    [ "g", "dd/de4/structCUDA_1_1Material.html#a9a7c47dff6eb9d56935beacd8caee0f0", null ],
    [ "gg", "dd/de4/structCUDA_1_1Material.html#a56836fdb1671685c607b5b00b490e5c2", null ],
    [ "isotropic", "dd/de4/structCUDA_1_1Material.html#ac730ba2b1d69d6ad327f0aa983681f12", null ],
    [ "m_init", "dd/de4/structCUDA_1_1Material.html#aebfac7c87b523ad9ac62ab2062d56647", null ],
    [ "m_prop", "dd/de4/structCUDA_1_1Material.html#a7e3c0acb48329d7d0e70ad0dbfc41654", null ],
    [ "mu_a", "dd/de4/structCUDA_1_1Material.html#a4e3ded0d885329eb34924f04c6ac9b7f", null ],
    [ "mu_s", "dd/de4/structCUDA_1_1Material.html#a10201b8f8c1dd5678563fac02f0d8e0e", null ],
    [ "mu_t", "dd/de4/structCUDA_1_1Material.html#a07c4df3de2371013686bbc03d084d884", null ],
    [ "n", "dd/de4/structCUDA_1_1Material.html#a6ac98aba9f2c34ed0ef4ed8654eae9cb", null ],
    [ "one_minus_gg", "dd/de4/structCUDA_1_1Material.html#aace87fcfff43442007b7641f397bfede", null ],
    [ "one_plus_gg", "dd/de4/structCUDA_1_1Material.html#a18b411b6ba1f4d4fce5ba1fcb13263ac", null ],
    [ "recip_2g", "dd/de4/structCUDA_1_1Material.html#a777f6edab837e6abe87de0b9f3b4d855", null ],
    [ "recip_mu_t", "dd/de4/structCUDA_1_1Material.html#a530586e8af0687a9bfc3a5e52695c2f6", null ],
    [ "scatters", "dd/de4/structCUDA_1_1Material.html#a99647f8127d2f5769301d501aa097650", null ]
];