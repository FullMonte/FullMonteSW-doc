var host__memcopy__float_8cpp =
[
    [ "MemcopyWED", "d7/d69/structMemcopyWED.html", "d7/d69/structMemcopyWED" ],
    [ "DEFAULT", "dd/d08/host__memcopy__float_8cpp.html#a3da44afeba217135a680a7477b5e3ce3", null ],
    [ "DEVICE_STRING", "dd/d08/host__memcopy__float_8cpp.html#a0cc1f594a089044000af260be46f2309", null ],
    [ "HARDWARE", "dd/d08/host__memcopy__float_8cpp.html#ae63cc10269ee1bb0c967a74ac5a0d0d5", null ],
    [ "PI", "dd/d08/host__memcopy__float_8cpp.html#a598a3330b3c21701223ee0ca14316eca", null ],
    [ "STATUS_DONE", "dd/d08/host__memcopy__float_8cpp.html#a3c7966dcc2aec8e20459e036f5823fc2", null ],
    [ "STATUS_READY", "dd/d08/host__memcopy__float_8cpp.html#a64f314e17a1be8e6c8e09bb90b9ecb68", null ],
    [ "STATUS_RUNNING", "dd/d08/host__memcopy__float_8cpp.html#a799b04d03f49612e3e636b56c3972b80", null ],
    [ "STATUS_WAITING", "dd/d08/host__memcopy__float_8cpp.html#a4eb8ee21eeb7699dbe5fcee4c30d7b72", null ],
    [ "base_generator_type", "dd/d08/host__memcopy__float_8cpp.html#a91e9fc4e1fe0e380daf0d5b48e3fc4a9", null ],
    [ "main", "dd/d08/host__memcopy__float_8cpp.html#a0ddf1224851353fc92bfbff6f499fa97", null ]
];