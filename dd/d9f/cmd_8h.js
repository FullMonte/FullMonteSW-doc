var cmd_8h =
[
    [ "pages", "dc/d21/structpages.html", "dc/d21/structpages" ],
    [ "cmd_event", "d6/dd1/structcmd__event.html", "d6/dd1/structcmd__event" ],
    [ "cmd", "d5/db7/structcmd.html", "d5/db7/structcmd" ],
    [ "LOG2_ENTRIES", "dd/d9f/cmd_8h.html#a1bdbfc9b26f13d02790cc364c497da1f", null ],
    [ "LOG2_WAYS", "dd/d9f/cmd_8h.html#a7d2ee67bb1b3b5447ef88f36c73f2cd5", null ],
    [ "PAGE_ADDR_BITS", "dd/d9f/cmd_8h.html#af8f9bab70326d767568f7dbc8452ef97", null ],
    [ "PAGE_ENTRIES", "dd/d9f/cmd_8h.html#a04d95f827e85da8915a4eebff582a7be", null ],
    [ "PAGE_MASK", "dd/d9f/cmd_8h.html#ae4aa620ce57c7c3171b916de2c5f09f2", null ],
    [ "PAGE_WAYS", "dd/d9f/cmd_8h.html#a114db3e16dc51292a3da4ab3d41f4bf0", null ],
    [ "TOTAL_PAGES_CACHED", "dd/d9f/cmd_8h.html#ac512cb3ae79d60b0e8440d3ac8a14412", null ],
    [ "cmd_type", "dd/d9f/cmd_8h.html#a41a2fe54b18ab7ba405a7e7da6d041f1", [
      [ "CMD_READ", "dd/d9f/cmd_8h.html#a41a2fe54b18ab7ba405a7e7da6d041f1a43d7f3a35093d4198124c87b43afdefb", null ],
      [ "CMD_WRITE", "dd/d9f/cmd_8h.html#a41a2fe54b18ab7ba405a7e7da6d041f1a612986a7ae236cab656844222f00ba82", null ],
      [ "CMD_TOUCH", "dd/d9f/cmd_8h.html#a41a2fe54b18ab7ba405a7e7da6d041f1a80d2f18be98b7e5bc7b44e3699eee477", null ],
      [ "CMD_INTERRUPT", "dd/d9f/cmd_8h.html#a41a2fe54b18ab7ba405a7e7da6d041f1a2b90de780d43a3df768b23e1195b48dc", null ],
      [ "CMD_READ_PE", "dd/d9f/cmd_8h.html#a41a2fe54b18ab7ba405a7e7da6d041f1ab0189b4238b2e1031e9c43d9663296cd", null ],
      [ "CMD_OTHER", "dd/d9f/cmd_8h.html#a41a2fe54b18ab7ba405a7e7da6d041f1a969c839c3f592f7fb054ee6cb5b33115", null ]
    ] ],
    [ "mem_state", "dd/d9f/cmd_8h.html#a010dec02307467dbb53e944b4258f137", [
      [ "MEM_IDLE", "dd/d9f/cmd_8h.html#a010dec02307467dbb53e944b4258f137a0e32cb4b435834a5d4d58f80638c1a7a", null ],
      [ "MEM_TOUCH", "dd/d9f/cmd_8h.html#a010dec02307467dbb53e944b4258f137a691ac4a95f4169f87cd47be72533a1ea", null ],
      [ "MEM_TOUCHED", "dd/d9f/cmd_8h.html#a010dec02307467dbb53e944b4258f137a4fed740b51701480e596570770e0c875", null ],
      [ "MEM_BUFFER", "dd/d9f/cmd_8h.html#a010dec02307467dbb53e944b4258f137a33d6abe4dda3adc3bbc5eea07814aa23", null ],
      [ "MEM_REQUEST", "dd/d9f/cmd_8h.html#a010dec02307467dbb53e944b4258f137af3f2ad3363a33d2935534187b4c3a20e", null ],
      [ "MEM_RECEIVED", "dd/d9f/cmd_8h.html#a010dec02307467dbb53e944b4258f137a345c72ac3cbf22ae1480702a1566d2de", null ],
      [ "MEM_DONE", "dd/d9f/cmd_8h.html#a010dec02307467dbb53e944b4258f137a9871efbcaa056ca86e882c0a4b882695", null ]
    ] ],
    [ "client_cmd", "dd/d9f/cmd_8h.html#ae0d41cef2ba17368c1dec333dfa71292", null ],
    [ "cmd_init", "dd/d9f/cmd_8h.html#a90c86bd5975cce28269de2f67e6b81e0", null ],
    [ "handle_aerror", "dd/d9f/cmd_8h.html#a68d0745d1ce4102ce0ce81e0446a67fd", null ],
    [ "handle_buffer_data", "dd/d9f/cmd_8h.html#af3b808eec44061bb4bafb4d7224e7c73", null ],
    [ "handle_buffer_read", "dd/d9f/cmd_8h.html#a81810467a0ba3bbef18c464aadb8769d", null ],
    [ "handle_buffer_write", "dd/d9f/cmd_8h.html#a6639b247abdd80132f733d6f8cfe86fc", null ],
    [ "handle_cmd", "dd/d9f/cmd_8h.html#a90ce114d36f6ef253d5281c2c2b46dde", null ],
    [ "handle_interrupt", "dd/d9f/cmd_8h.html#abcc5d32b810f1945b1aa9f577e718e39", null ],
    [ "handle_mem_return", "dd/d9f/cmd_8h.html#af0a21b6bcffa621c53f0e82a08e45931", null ],
    [ "handle_mem_write", "dd/d9f/cmd_8h.html#a8a7788970e708ef6f17248efa5dca5b5", null ],
    [ "handle_response", "dd/d9f/cmd_8h.html#a19b191d898938ce3d899ceb453b22df6", null ],
    [ "handle_touch", "dd/d9f/cmd_8h.html#a90c6b02ede0bf4619586b4c22054200e", null ]
];