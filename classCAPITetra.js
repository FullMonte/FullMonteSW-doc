var classCAPITetra =
[
    [ "getIntersection", "classCAPITetra.html#a041db9a1810021772a389d37d1eec59c", null ],
    [ "adjTetras", "classCAPITetra.html#abff2b9e476ca551ceff716a70b6f83c6", null ],
    [ "C", "classCAPITetra.html#a69ab368383881de2d03f29eb7d560d42", null ],
    [ "faceFlags", "classCAPITetra.html#ac7f08c52523b46b7adda19652166d04f", null ],
    [ "IDfds", "classCAPITetra.html#ac7dd3c0e09a72addb67c6fdc27d78f8b", null ],
    [ "matID", "classCAPITetra.html#a7582397052f93688a78b857207c0d745", null ],
    [ "nx", "classCAPITetra.html#ab29d4a4a307eab30c2b2411c04ce1b76", null ],
    [ "ny", "classCAPITetra.html#a25d7772c8d625e68e6940efc1e093543", null ],
    [ "nz", "classCAPITetra.html#afe60ca576bfa64282c5afdc771edfee1", null ]
];