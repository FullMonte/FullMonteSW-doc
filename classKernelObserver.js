var classKernelObserver =
[
    [ "~KernelObserver", "classKernelObserver.html#a74678e9f9e50cd26bc36a57c8fed6a0f", null ],
    [ "notify_create", "classKernelObserver.html#aa9bdc8099fdd9a158275af196a9dea66", null ],
    [ "notify_finish", "classKernelObserver.html#a60efaa78df71e1b432f2530575477953", null ],
    [ "notify_prepare", "classKernelObserver.html#aba60c5183cb4e0c08a8aabefe10b2a85", null ],
    [ "notify_result", "classKernelObserver.html#ab147816cd1ef89af41ef763abf28133d", null ],
    [ "notify_start", "classKernelObserver.html#a2c65d4948b1e654300f2dbd935a6709d", null ]
];