var classx86Kernel_1_1Material =
[
    [ "Material", "classx86Kernel_1_1Material.html#a137e987401b63eb7c6c27c3e38bc74b5", null ],
    [ "Material", "classx86Kernel_1_1Material.html#aa8262c7da4cba0ad0bb9c1b7db0f45a4", null ],
    [ "Material", "classx86Kernel_1_1Material.html#a891c322f71da50c6f11b747d0b14eabb", null ],
    [ "absfrac", "classx86Kernel_1_1Material.html#a62e2bcac355fbca549557eddce4d0c98", null ],
    [ "m_init", "classx86Kernel_1_1Material.html#a93264807e2ea4fbcc6671f393868591f", null ],
    [ "m_prop", "classx86Kernel_1_1Material.html#a7b6f172a0f4fd69c6db75f18457e55b0", null ],
    [ "muT", "classx86Kernel_1_1Material.html#a4ba3644cf35d63c708a630a96dd88988", null ],
    [ "n", "classx86Kernel_1_1Material.html#a4abaa9af15eb52c93131af79f939d4ac", null ],
    [ "scatters", "classx86Kernel_1_1Material.html#a7c3c52a1aacfc8196d9aecf34ef6e48a", null ]
];