var dir_bece14133438fd55826e84188656a9cb =
[
    [ "BDPIScatterTestFixture.cpp", "d4/ddc/BDPIScatterTestFixture_8cpp.html", "d4/ddc/BDPIScatterTestFixture_8cpp" ],
    [ "CAPIScatter.cpp", "dc/d1c/CAPIScatter_8cpp.html", "dc/d1c/CAPIScatter_8cpp" ],
    [ "RandomScatterStim.hpp", "dd/d72/RandomScatterStim_8hpp.html", [
      [ "RandomScatterStim", "d8/ddf/classRandomScatterStim.html", "d8/ddf/classRandomScatterStim" ],
      [ "param_type", "d6/d2b/structRandomScatterStim_1_1param__type.html", null ]
    ] ],
    [ "randSphere.m", "d0/dc6/randSphere_8m.html", "d0/dc6/randSphere_8m" ],
    [ "randSphere3.m", "d1/dca/randSphere3_8m.html", "d1/dca/randSphere3_8m" ],
    [ "scatterCheck.m", "d8/d94/scatterCheck_8m.html", "d8/d94/scatterCheck_8m" ],
    [ "ScatterChecker.cpp", "d0/d7e/ScatterChecker_8cpp.html", null ],
    [ "ScatterChecker.hpp", "d8/dca/ScatterChecker_8hpp.html", [
      [ "ScatterChecker", "de/dc6/classScatterChecker.html", "de/dc6/classScatterChecker" ]
    ] ],
    [ "scatterTest.m", "d2/d39/scatterTest_8m.html", "d2/d39/scatterTest_8m" ],
    [ "ScatterTestFixture.cpp", "d0/d8b/ScatterTestFixture_8cpp.html", null ],
    [ "ScatterTestFixture.hpp", "d9/dda/ScatterTestFixture_8hpp.html", [
      [ "ScatterTestFixture", "db/d54/classScatterTestFixture.html", "db/d54/classScatterTestFixture" ]
    ] ],
    [ "ScatterTraits.cpp", "d4/d6c/ScatterTraits_8cpp.html", "d4/d6c/ScatterTraits_8cpp" ],
    [ "ScatterTraits.hpp", "da/d5a/ScatterTraits_8hpp.html", [
      [ "ScatterTraits", "d2/df2/classScatterTraits.html", "d2/df2/classScatterTraits" ],
      [ "Input", "d8/d87/structScatterTraits_1_1Input.html", "d8/d87/structScatterTraits_1_1Input" ]
    ] ]
];