var dir_5999d19fb248973c0f100b82abd02bd9 =
[
    [ "check.m", "d4/de5/SinCosCordic_2check_8m.html", "d4/de5/SinCosCordic_2check_8m" ],
    [ "coeffs.m", "da/d65/coeffs_8m.html", "da/d65/coeffs_8m" ],
    [ "cordicstage.m", "d9/d6e/cordicstage_8m.html", "d9/d6e/cordicstage_8m" ],
    [ "SinCosChecker.cpp", "d9/dd4/SinCosChecker_8cpp.html", null ],
    [ "SinCosChecker.hpp", "d4/d5b/SinCosChecker_8hpp.html", [
      [ "SinCosChecker", "dc/d18/classSinCosChecker.html", "dc/d18/classSinCosChecker" ]
    ] ],
    [ "SinCosCordicTester.cpp", "dc/d4d/SinCosCordicTester_8cpp.html", null ],
    [ "SinCosCordicTestFixture.hpp", "de/db7/SinCosCordicTestFixture_8hpp.html", "de/db7/SinCosCordicTestFixture_8hpp" ],
    [ "SinCosCordicTraits.hpp", "d2/dc4/SinCosCordicTraits_8hpp.html", [
      [ "SinCosCordicTraits", "da/d62/classSinCosCordicTraits.html", "da/d62/classSinCosCordicTraits" ]
    ] ]
];