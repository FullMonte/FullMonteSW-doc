var files_dup =
[
    [ "External", "dir_443fc37f241ba4018364a998c18d007b.html", "dir_443fc37f241ba4018364a998c18d007b" ],
    [ "Geometry", "dir_eccc925508204c58f9de69585b548932.html", "dir_eccc925508204c58f9de69585b548932" ],
    [ "Kernels", "dir_5a4b884a9b00b9d0f7e0867a3d5d7279.html", "dir_5a4b884a9b00b9d0f7e0867a3d5d7279" ],
    [ "Logging", "dir_d9949a2e036493f8647151a4ab6ee0ce.html", "dir_d9949a2e036493f8647151a4ab6ee0ce" ],
    [ "OutputTypes", "dir_cc703c7f06742ef5f16a57ba64c7834c.html", "dir_cc703c7f06742ef5f16a57ba64c7834c" ],
    [ "Queries", "dir_3d9b7978933c95299f10117106928bd4.html", "dir_3d9b7978933c95299f10117106928bd4" ],
    [ "SharedMem", "dir_b7ecacf8268486a3c072594a360a709a.html", "dir_b7ecacf8268486a3c072594a360a709a" ],
    [ "Storage", "dir_d29c5f5a2915d6c5388c9daae4f109c7.html", "dir_d29c5f5a2915d6c5388c9daae4f109c7" ],
    [ "Utilities", "dir_b05a5fa85f84bfc9013183c9d87f9a36.html", "dir_b05a5fa85f84bfc9013183c9d87f9a36" ],
    [ "VTK", "dir_04355626533a17e58c63cd539085a462.html", "dir_04355626533a17e58c63cd539085a462" ],
    [ "Warnings", "dir_e8f2fa10fa09990d02fb5094abca2f68.html", "dir_e8f2fa10fa09990d02fb5094abca2f68" ]
];