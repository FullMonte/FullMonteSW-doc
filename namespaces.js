var namespaces =
[
    [ "detail", "dd/d39/namespacedetail.html", null ],
    [ "Emitter", "de/d53/namespaceEmitter.html", null ],
    [ "Events", "d7/d69/namespaceEvents.html", null ],
    [ "KernelEvent", "d6/d27/namespaceKernelEvent.html", null ],
    [ "RandomDistribution", "d9/d25/namespaceRandomDistribution.html", null ],
    [ "Source", "d1/d39/namespaceSource.html", "d1/d39/namespaceSource" ],
    [ "SSE", "d5/de2/namespaceSSE.html", null ],
    [ "TIMOS", "d6/db9/namespaceTIMOS.html", null ],
    [ "URNG", "de/d06/namespaceURNG.html", null ],
    [ "x86Kernel", "d9/d3f/namespacex86Kernel.html", null ]
];