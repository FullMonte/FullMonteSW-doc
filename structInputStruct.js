var structInputStruct =
[
    [ "da", "structInputStruct.html#a08a5e551575af9d3f9f11c026140ee24", null ],
    [ "dr", "structInputStruct.html#a679d46163bc34ff943903e2494d51e1e", null ],
    [ "dz", "structInputStruct.html#a412fbb939d6d987e41a79836ac8b6e74", null ],
    [ "layerspecs", "structInputStruct.html#ac9848aba6591c011a2bb9bb9cda52fb2", null ],
    [ "na", "structInputStruct.html#a50d65aa67a50d771ebd44f1b05e33756", null ],
    [ "nr", "structInputStruct.html#a70d69d85e4ffc2b19ee5b20d33c54614", null ],
    [ "num_layers", "structInputStruct.html#ae5edf8703354355e7a14b145be238fce", null ],
    [ "num_photons", "structInputStruct.html#a1e2db87272a6e6aad2df373138e57b3e", null ],
    [ "nz", "structInputStruct.html#aa63c4a1398fb53434f46d9ca2600c309", null ],
    [ "out_fformat", "structInputStruct.html#a8f1833828489d91fbf091b7a1b3ebb81", null ],
    [ "out_fname", "structInputStruct.html#a2ac8c50c57fc98db1bf307f956004b87", null ],
    [ "Wth", "structInputStruct.html#aff99c66de3b98da33a4db41c824796ee", null ]
];