var namespaces_dup =
[
    [ "aocl_utils", "d9/da0/namespaceaocl__utils.html", null ],
    [ "boost", "d4/da9/namespaceboost.html", "d4/da9/namespaceboost" ],
    [ "CUDA", "d6/dee/namespaceCUDA.html", null ],
    [ "detail", "dd/d39/namespacedetail.html", null ],
    [ "Emitter", "de/d53/namespaceEmitter.html", null ],
    [ "Events", "d7/d69/namespaceEvents.html", null ],
    [ "FPGACL", "d3/d30/namespaceFPGACL.html", null ],
    [ "FullMonteHW", "df/df3/namespaceFullMonteHW.html", "df/df3/namespaceFullMonteHW" ],
    [ "KernelEvent", "d6/d27/namespaceKernelEvent.html", null ],
    [ "RandomDistribution", "d9/d25/namespaceRandomDistribution.html", null ],
    [ "regress", "d6/d59/namespaceregress.html", null ],
    [ "sfmt", "df/dd8/namespacesfmt.html", null ],
    [ "Source", "d1/d39/namespaceSource.html", "d1/d39/namespaceSource" ],
    [ "SSE", "d5/de2/namespaceSSE.html", null ],
    [ "std", "d8/dcc/namespacestd.html", null ],
    [ "TIMOS", "d6/db9/namespaceTIMOS.html", null ],
    [ "unity_test_summary", "d1/dd6/namespaceunity__test__summary.html", null ],
    [ "unity_to_junit", "de/d65/namespaceunity__to__junit.html", null ],
    [ "URNG", "de/d06/namespaceURNG.html", null ],
    [ "x86Kernel", "d9/d3f/namespacex86Kernel.html", null ]
];