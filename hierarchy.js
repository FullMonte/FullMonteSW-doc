var hierarchy =
[
    [ "AbsorptionSum", "df/d91/classAbsorptionSum.html", null ],
    [ "Source::Abstract", "da/dec/classSource_1_1Abstract.html", [
      [ "Source::Composite", "d7/d70/classSource_1_1Composite.html", null ],
      [ "Source::CylDetector", "d0/d0f/classSource_1_1CylDetector.html", null ],
      [ "Source::Cylinder", "df/d6a/classSource_1_1Cylinder.html", null ],
      [ "Source::Fiber", "db/d89/classSource_1_1Fiber.html", null ],
      [ "Source::Line", "d6/dc4/classSource_1_1Line.html", null ],
      [ "Source::Point", "d3/d8c/classSource_1_1Point.html", [
        [ "Source::Ball", "d5/da1/classSource_1_1Ball.html", null ],
        [ "Source::PencilBeam", "db/d0a/classSource_1_1PencilBeam.html", null ]
      ] ],
      [ "Source::Surface", "df/d08/classSource_1_1Surface.html", [
        [ "Source::SurfaceTri", "d3/da5/classSource_1_1SurfaceTri.html", null ]
      ] ],
      [ "Source::TetraFace", "db/d84/classSource_1_1TetraFace.html", null ],
      [ "Source::Volume", "d3/da0/classSource_1_1Volume.html", null ]
    ] ],
    [ "AbstractDataDifference", "d8/db1/classAbstractDataDifference.html", [
      [ "DataDifference< T >", "d3/d5e/classDataDifference.html", null ],
      [ "DataDifference< SpatialMap< float > >", "d3/d5e/classDataDifference.html", [
        [ "Chi2Homogeneity", "d9/d85/classChi2Homogeneity.html", null ],
        [ "PercentDifference", "d9/d50/classPercentDifference.html", null ]
      ] ]
    ] ],
    [ "AbstractScorer", "d1/d56/classAbstractScorer.html", [
      [ "AbstractDirectedSurfaceScorer", "d0/da7/classAbstractDirectedSurfaceScorer.html", [
        [ "DirectedSurfaceScorer< AccumulatorT >", "d9/d01/classDirectedSurfaceScorer.html", null ]
      ] ],
      [ "AccumulationEventScorer", "d5/d86/classAccumulationEventScorer.html", [
        [ "TetraAccumulationEventScorer", "d2/d5c/classTetraAccumulationEventScorer.html", null ]
      ] ],
      [ "CylAbsorptionScorer< AccumulatorT >", "de/d52/classCylAbsorptionScorer.html", null ],
      [ "MemTraceScorer", "d3/d2d/classMemTraceScorer.html", [
        [ "TetraMemTraceScorer", "df/d6a/classTetraMemTraceScorer.html", null ]
      ] ],
      [ "MultiThreadWithIndividualCopy< SingleThreadLoggerT >", "da/dd4/classMultiThreadWithIndividualCopy.html", null ],
      [ "PacketPostmortem", "dd/d1c/classPacketPostmortem.html", null ],
      [ "PathScorer", "d1/d9d/classPathScorer.html", null ],
      [ "SurfaceExitImageScorer", "d9/d92/classSurfaceExitImageScorer.html", null ],
      [ "SurfaceExitScorer", "da/d0e/classSurfaceExitScorer.html", null ],
      [ "VolumeAbsorptionScorer", "d5/d0a/classVolumeAbsorptionScorer.html", null ]
    ] ],
    [ "AccumulationEvent::AccumulationEventEntry", "d2/de4/structAccumulationEvent_1_1AccumulationEventEntry.html", null ],
    [ "AccumulationLineQuery", "d3/dab/classAccumulationLineQuery.html", null ],
    [ "AffineTransform< FT, D >", "d8/d8c/classAffineTransform.html", null ],
    [ "array", null, [
      [ "Point< D, T >", "dc/d4f/classPoint.html", null ],
      [ "Point< 3, double >", "dc/d4f/classPoint.html", null ],
      [ "Point< D, double >", "dc/d4f/classPoint.html", [
        [ "Vector< 3, double >", "d6/da1/classVector.html", null ]
      ] ]
    ] ],
    [ "ArrayPredicate", "d5/df1/classArrayPredicate.html", [
      [ "GeometryPredicate", "de/da4/classGeometryPredicate.html", [
        [ "SurfaceCellPredicate", "db/d06/classSurfaceCellPredicate.html", [
          [ "SurfaceOfRegionPredicate", "de/df8/classSurfaceOfRegionPredicate.html", null ]
        ] ],
        [ "VolumeCellPredicate", "d8/d6f/classVolumeCellPredicate.html", [
          [ "VolumeCellInRegionPredicate", "d4/d85/classVolumeCellInRegionPredicate.html", null ]
        ] ]
      ] ]
    ] ],
    [ "ArrayPredicateEvaluator", "d0/d2b/classArrayPredicateEvaluator.html", [
      [ "SurfaceCellPredicateEvaluator< Pred, Geom, Func >", "da/d75/classSurfaceCellPredicateEvaluator.html", null ],
      [ "SurfaceOfRegionPredicateEvaluator", "d6/d9b/classSurfaceOfRegionPredicateEvaluator.html", null ],
      [ "VolumeCellPredicateEvaluator< Pred, Geom, Func >", "d8/d25/classVolumeCellPredicateEvaluator.html", null ]
    ] ],
    [ "AtomicMultiThreadAccumulator< Acc, Delta >", "da/d24/classAtomicMultiThreadAccumulator.html", null ],
    [ "AtomicMultiThreadAccumulator< double, float >", "da/d24/classAtomicMultiThreadAccumulator.html", null ],
    [ "AutoStreamBuffer", "d6/de2/classAutoStreamBuffer.html", null ],
    [ "BaseLogger", "d9/dd8/classBaseLogger.html", [
      [ "AccumulationEventScorer::LoggerBase", "da/d1f/classAccumulationEventScorer_1_1LoggerBase.html", [
        [ "TetraAccumulationEventScorer::Logger", "d7/da3/classTetraAccumulationEventScorer_1_1Logger.html", null ]
      ] ],
      [ "CylAbsorptionScorer< AccumulatorT >::Logger", "d1/d4a/classCylAbsorptionScorer_1_1Logger.html", null ],
      [ "DirectedSurfaceScorer< AccumulatorT >::Logger", "dc/de0/classDirectedSurfaceScorer_1_1Logger.html", null ],
      [ "LoggerWithState< StateT >", "d5/d3d/classLoggerWithState.html", null ],
      [ "LoggerWithState< MCConservationCounts >", "d5/d3d/classLoggerWithState.html", [
        [ "ConservationLogger", "d3/def/classConservationLogger.html", null ]
      ] ],
      [ "LoggerWithState< MCEventCounts >", "d5/d3d/classLoggerWithState.html", [
        [ "EventLogger", "d3/de0/classEventLogger.html", null ]
      ] ],
      [ "MemTraceScorer::LoggerBase", "dd/d6b/classMemTraceScorer_1_1LoggerBase.html", [
        [ "TetraMemTraceScorer::Logger", "d2/dd8/classTetraMemTraceScorer_1_1Logger.html", null ]
      ] ],
      [ "PacketPostmortem::Logger", "d8/df0/classPacketPostmortem_1_1Logger.html", null ],
      [ "PathScorer::Logger", "d9/d89/classPathScorer_1_1Logger.html", null ],
      [ "SurfaceExitImageScorer::Logger", "d1/db5/classSurfaceExitImageScorer_1_1Logger.html", null ],
      [ "SurfaceExitScorer::Logger", "d5/d4d/classSurfaceExitScorer_1_1Logger.html", null ],
      [ "VolumeAbsorptionScorer::Logger", "da/dc3/classVolumeAbsorptionScorer_1_1Logger.html", null ]
    ] ],
    [ "Basis", "da/ded/classBasis.html", null ],
    [ "BlockRandomDistribution< Distribution, OutputBlocks, Align >", "db/d6e/classBlockRandomDistribution.html", null ],
    [ "BlockRandomDistribution< FloatPM1Distribution, 1, 32 >", "db/d6e/classBlockRandomDistribution.html", null ],
    [ "BlockRandomDistribution< FloatU01Distribution, 1, 32 >", "db/d6e/classBlockRandomDistribution.html", null ],
    [ "BlockRandomDistribution< FloatUnitExpDistribution, 2, 32 >", "db/d6e/classBlockRandomDistribution.html", null ],
    [ "BlockRandomDistribution< FloatUVect2Distribution, 1, 32 >", "db/d6e/classBlockRandomDistribution.html", null ],
    [ "BlockRandomDistribution< UniformUI32Distribution, 1, 32 >", "db/d6e/classBlockRandomDistribution.html", null ],
    [ "BlockRNGAdaptor< RNG, T, GenBlockSize, ReturnBlockSize, Align >", "d3/df6/classBlockRNGAdaptor.html", null ],
    [ "BlockRNGAdaptor< sfmt_t, uint32_t, 1024, 8, 32 >", "d3/df6/classBlockRNGAdaptor.html", null ],
    [ "Chi2SetFixture", "d2/d59/structChi2SetFixture.html", null ],
    [ "clonable< Base, Derived >", "d0/df8/classclonable.html", null ],
    [ "clonable_base< Base >", "db/db8/classclonable__base.html", null ],
    [ "COMSOLDataReader", "de/d01/classCOMSOLDataReader.html", null ],
    [ "COMSOLReader", "d9/d83/classCOMSOLReader.html", null ],
    [ "COMSOLWriter", "d6/d38/classCOMSOLWriter.html", null ],
    [ "ConservationCheck", "df/dfc/classConservationCheck.html", null ],
    [ "CSVPhotonDataWriter", "dd/ded/classCSVPhotonDataWriter.html", null ],
    [ "CUDADirectedSurfaceScorer", "d3/dfb/classCUDADirectedSurfaceScorer.html", [
      [ "TetraMCCUDAKernel< RNGT >", "d2/dd5/classTetraMCCUDAKernel.html", null ],
      [ "TetraMCCUDAKernel< RNG_SFMT_AVX >", "d2/dd5/classTetraMCCUDAKernel.html", [
        [ "TetraCUDAInternalKernel", "de/df4/classTetraCUDAInternalKernel.html", null ],
        [ "TetraCUDASurfaceKernel", "d0/d70/classTetraCUDASurfaceKernel.html", null ],
        [ "TetraCUDASVKernel", "d7/d36/classTetraCUDASVKernel.html", null ],
        [ "TetraCUDAVolumeKernel", "dd/de1/classTetraCUDAVolumeKernel.html", null ]
      ] ]
    ] ],
    [ "CUDATetrasFromLayered", "d1/d28/classCUDATetrasFromLayered.html", null ],
    [ "CUDATetrasFromTetraMesh", "da/dd2/classCUDATetrasFromTetraMesh.html", null ],
    [ "CylCoord", "de/d75/structCylCoord.html", null ],
    [ "Emitter::CylDetector< RNG >", "db/d46/classEmitter_1_1CylDetector.html", null ],
    [ "Emitter::Cylinder< RNG >", "dd/d2f/classEmitter_1_1Cylinder.html", null ],
    [ "CylIndex", "d5/dd5/structCylIndex.html", null ],
    [ "DataSetStats", "d6/d2b/classDataSetStats.html", [
      [ "BasicStats", "df/d70/classBasicStats.html", null ]
    ] ],
    [ "delim", "d3/dab/structdelim.html", null ],
    [ "delim_stream", "de/d65/structdelim__stream.html", null ],
    [ "Emitter::DetectorBase< RNG >", "d3/df3/classEmitter_1_1DetectorBase.html", [
      [ "Emitter::Detector< RNG, DetectorType >", "d2/ded/classEmitter_1_1Detector.html", null ]
    ] ],
    [ "Source::detail::Directed", "d6/d44/classSource_1_1detail_1_1Directed.html", [
      [ "Source::PencilBeam", "db/d0a/classSource_1_1PencilBeam.html", null ]
    ] ],
    [ "Emitter::Directed", "dd/d6f/classEmitter_1_1Directed.html", null ],
    [ "DirectedSurfaceSum", "d5/d48/classDirectedSurfaceSum.html", null ],
    [ "Disk", "d7/dea/classDisk.html", null ],
    [ "Emitter::Disk< RNG >", "da/da8/classEmitter_1_1Disk.html", null ],
    [ "DoseHistogramGenerator", "d9/d34/classDoseHistogramGenerator.html", [
      [ "DoseSurfaceHistogramGenerator", "dd/d01/classDoseSurfaceHistogramGenerator.html", null ],
      [ "DoseVolumeHistogramGenerator", "dd/d97/classDoseVolumeHistogramGenerator.html", null ]
    ] ],
    [ "DoseHistogramGenerator::InputElement::DoseLess", "d1/da8/structDoseHistogramGenerator_1_1InputElement_1_1DoseLess.html", null ],
    [ "drop", "d5/df5/structdrop.html", null ],
    [ "DynamicIndexRelabel", "d5/dee/classDynamicIndexRelabel.html", null ],
    [ "DoseHistogram::Element", "da/d73/structDoseHistogram_1_1Element.html", null ],
    [ "EmpiricalCDF< Value, Weight, Comp >::Element", "d9/df3/structEmpiricalCDF_1_1Element.html", null ],
    [ "DoseHistogramGenerator::ElRegion", "d4/d08/structDoseHistogramGenerator_1_1ElRegion.html", null ],
    [ "Emitter::EmitterBase< RNG >", "d1/d8d/classEmitter_1_1EmitterBase.html", [
      [ "Emitter::Composite< RNG >", "d1/d78/classEmitter_1_1Composite.html", null ],
      [ "Emitter::PositionDirectionEmitter< RNG, Position, Direction >", "d2/d5c/classEmitter_1_1PositionDirectionEmitter.html", null ]
    ] ],
    [ "EmpiricalCDF< Value, Weight, Comp >", "d2/dfc/classEmpiricalCDF.html", null ],
    [ "MCMLOutputReader::EnergyDisposition", "d5/df5/structMCMLOutputReader_1_1EnergyDisposition.html", null ],
    [ "EnergyToFluence", "de/dee/classEnergyToFluence.html", null ],
    [ "Error", "d9/dd6/classError.html", null ],
    [ "ErrorChecker", "da/d70/classErrorChecker.html", [
      [ "MeshErrorChecker", "d2/d8b/classMeshErrorChecker.html", [
        [ "TetraOrientationChecker", "d6/dc7/classTetraOrientationChecker.html", null ]
      ] ]
    ] ],
    [ "EventCountComparison", "d3/d34/classEventCountComparison.html", null ],
    [ "AccumulationEventScorer::EventRecord", "da/db1/structAccumulationEventScorer_1_1EventRecord.html", null ],
    [ "face_opposite", "d1/dae/structface__opposite.html", null ],
    [ "face_prop< FaceProp >", "d0/dd7/structface__prop.html", null ],
    [ "TIMOSOutputReader::FaceInfo", "dc/d1c/structTIMOSOutputReader_1_1FaceInfo.html", null ],
    [ "TetraMeshBuilder::FaceInfo", "d8/d7e/structTetraMeshBuilder_1_1FaceInfo.html", null ],
    [ "TIMOS::FaceSource", "d3/d70/structTIMOS_1_1FaceSource.html", null ],
    [ "TetraMeshBuilder::FaceInfo::FaceTetraInfo", "df/d68/structTetraMeshBuilder_1_1FaceInfo_1_1FaceTetraInfo.html", null ],
    [ "FaceTetraLink", "dc/d04/structFaceTetraLink.html", null ],
    [ "Emitter::FiberCone< RNG >", "d3/de8/classEmitter_1_1FiberCone.html", null ],
    [ "FilterBase< T >", "d0/d26/classFilterBase.html", null ],
    [ "FilterBase< int >", "d0/d26/classFilterBase.html", null ],
    [ "FilterBase< unsigned >", "d0/d26/classFilterBase.html", null ],
    [ "flatten_whitespace", "d4/d47/structflatten__whitespace.html", null ],
    [ "FloatVectorBase", "d7/d98/structFloatVectorBase.html", null ],
    [ "FloatVectorBase_NonSSE", "d5/dd8/structFloatVectorBase__NonSSE.html", [
      [ "FloatPM1Distribution", "d8/dba/classFloatPM1Distribution.html", null ],
      [ "FloatU01Distribution", "d9/d40/classFloatU01Distribution.html", null ],
      [ "FloatUnitExpDistribution", "d4/d41/classFloatUnitExpDistribution.html", null ],
      [ "FloatUVect2Distribution", "df/d96/classFloatUVect2Distribution.html", null ],
      [ "HenyeyGreenstein8f", "de/dde/classHenyeyGreenstein8f.html", null ]
    ] ],
    [ "FPGACLTetrasFromLayered", "d7/d37/classFPGACLTetrasFromLayered.html", null ],
    [ "FPGACLTetrasFromTetraMesh", "dd/d9e/classFPGACLTetrasFromTetraMesh.html", null ],
    [ "FullMonteLogger", "d0/dae/classFullMonteLogger.html", null ],
    [ "FullMonteTimer", "df/d4c/classFullMonteTimer.html", null ],
    [ "GenericGeometryReader", "d6/d82/classGenericGeometryReader.html", null ],
    [ "TIMOS::GenericSource", "d8/dcf/structTIMOS_1_1GenericSource.html", null ],
    [ "Geometry", "d9/df2/classGeometry.html", [
      [ "Layered", "d5/d09/classLayered.html", null ],
      [ "TetraMesh", "dd/df2/classTetraMesh.html", null ]
    ] ],
    [ "Emitter::HemiSphere< RNG >", "d0/df4/classEmitter_1_1HemiSphere.html", null ],
    [ "ImplicitPlane< Kernel >", "d7/de7/classImplicitPlane.html", null ],
    [ "init_tag", "dc/dcc/structinit__tag.html", null ],
    [ "DoseHistogramGenerator::InputElement", "d2/d8d/structDoseHistogramGenerator_1_1InputElement.html", [
      [ "DoseHistogramGenerator::OutputElement", "db/d26/structDoseHistogramGenerator_1_1OutputElement.html", null ]
    ] ],
    [ "Emitter::Isotropic< RNG >", "d3/d40/classEmitter_1_1Isotropic.html", null ],
    [ "iterator_facade", null, [
      [ "RayWalkIterator", "d4/d0e/classRayWalkIterator.html", null ]
    ] ],
    [ "Kernel", "d1/db8/classKernel.html", [
      [ "MCKernelBase", "d0/ddf/classMCKernelBase.html", [
        [ "CUDAMCKernelBase", "da/d8c/classCUDAMCKernelBase.html", [
          [ "TetraMCCUDAKernel< RNGT >", "d2/dd5/classTetraMCCUDAKernel.html", null ],
          [ "TetraMCCUDAKernel< RNG_SFMT_AVX >", "d2/dd5/classTetraMCCUDAKernel.html", null ]
        ] ],
        [ "FPGACLMCKernelBase", "da/de9/classFPGACLMCKernelBase.html", [
          [ "TetraMCFPGACLKernel< RNGT >", "d4/df6/classTetraMCFPGACLKernel.html", null ],
          [ "TetraMCFPGACLKernel< RNG_SFMT_AVX >", "d4/df6/classTetraMCFPGACLKernel.html", [
            [ "TetraFPGACLVolumeKernel", "d4/dd5/classTetraFPGACLVolumeKernel.html", null ]
          ] ]
        ] ],
        [ "P8MCKernelBase", "d8/dc8/classP8MCKernelBase.html", [
          [ "TetraMCP8DebugKernel< RNGT >", "da/d65/classTetraMCP8DebugKernel.html", null ],
          [ "TetraMCP8DebugKernel< RNG_SFMT_AVX >", "da/d65/classTetraMCP8DebugKernel.html", [
            [ "TetraP8DebugInternalKernel", "d5/d39/classTetraP8DebugInternalKernel.html", null ]
          ] ],
          [ "TetraMCP8Kernel< RNGT >", "d8/d42/classTetraMCP8Kernel.html", null ],
          [ "TetraMCP8Kernel< RNG_SFMT_AVX >", "d8/d42/classTetraMCP8Kernel.html", [
            [ "TetraP8InternalKernel", "d8/d67/classTetraP8InternalKernel.html", null ]
          ] ]
        ] ],
        [ "ThreadedMCKernelBase", "d8/d4c/classThreadedMCKernelBase.html", [
          [ "TetraMCKernel< RNGT, ScorerT >", "d6/d5e/classTetraMCKernel.html", null ],
          [ "TetraMCKernel< RNG_SFMT_AVX, InternalScorer >", "d6/d5e/classTetraMCKernel.html", [
            [ "TetraInternalKernel", "d7/dea/classTetraInternalKernel.html", null ]
          ] ],
          [ "TetraMCKernel< RNG_SFMT_AVX, MCMLScorerPack >", "d6/d5e/classTetraMCKernel.html", [
            [ "MCMLKernel", "db/df7/classMCMLKernel.html", null ]
          ] ],
          [ "TetraMCKernel< RNG_SFMT_AVX, MCMLScorerPackQ >", "d6/d5e/classTetraMCKernel.html", [
            [ "MCMLKernelQ", "d4/d32/classMCMLKernelQ.html", null ]
          ] ],
          [ "TetraMCKernel< RNG_SFMT_AVX, MCMLScorerPackWithTraces >", "d6/d5e/classTetraMCKernel.html", [
            [ "MCMLKernelWithTraces", "dd/d79/classMCMLKernelWithTraces.html", null ]
          ] ],
          [ "TetraMCKernel< RNG_SFMT_AVX, SVScorer >", "d6/d5e/classTetraMCKernel.html", [
            [ "TetraSVKernel", "d5/db5/classTetraSVKernel.html", null ]
          ] ],
          [ "TetraMCKernel< RNG_SFMT_AVX, TetraSurfaceScorer >", "d6/d5e/classTetraMCKernel.html", [
            [ "TetraSurfaceKernel", "d5/d19/classTetraSurfaceKernel.html", null ]
          ] ],
          [ "TetraMCKernel< RNG_SFMT_AVX, TetraVolumeScorer >", "d6/d5e/classTetraMCKernel.html", [
            [ "TetraVolumeKernel", "db/dde/classTetraVolumeKernel.html", null ]
          ] ],
          [ "TetraMCKernel< RNG_SFMT_AVX, TraceScorer >", "d6/d5e/classTetraMCKernel.html", [
            [ "TetraTraceKernel", "d1/dc4/classTetraTraceKernel.html", null ]
          ] ]
        ] ]
      ] ]
    ] ],
    [ "KernelObserver", "d8/dd5/classKernelObserver.html", [
      [ "OStreamObserver", "dc/dcd/classOStreamObserver.html", null ]
    ] ],
    [ "LaunchPacket", "d8/ddd/structLaunchPacket.html", null ],
    [ "Layer", "d5/d8f/classLayer.html", null ],
    [ "LayeredAccumulationProbe", "db/dd3/classLayeredAccumulationProbe.html", null ],
    [ "Emitter::Line< RNG >", "d8/d01/classEmitter_1_1Line.html", null ],
    [ "LineQuery", "dd/dd7/classLineQuery.html", null ],
    [ "long2int", "dd/db6/unionlong2int.html", null ],
    [ "Material", "d3/d89/classMaterial.html", null ],
    [ "TIMOS::Material", "da/d18/structTIMOS_1_1Material.html", null ],
    [ "x86Kernel::Material", "d6/dda/classx86Kernel_1_1Material.html", null ],
    [ "MCConservationCounts", "d9/dec/structMCConservationCounts.html", [
      [ "MCConservationCountsOutput", "d7/d11/classMCConservationCountsOutput.html", null ]
    ] ],
    [ "MCEventCounts", "db/d33/structMCEventCounts.html", [
      [ "MCEventCountsOutput", "da/d64/classMCEventCountsOutput.html", null ]
    ] ],
    [ "MCMLCase", "de/d08/classMCMLCase.html", null ],
    [ "MCMLInputReader", "dd/d02/classMCMLInputReader.html", null ],
    [ "MCMLOutputReader", "d2/d85/classMCMLOutputReader.html", null ],
    [ "MCMLOutputWriter", "db/d21/classMCMLOutputWriter.html", null ],
    [ "TetraMCP8Kernel< RNGT >::MemcopyWED", "d2/d37/structTetraMCP8Kernel_1_1MemcopyWED.html", null ],
    [ "TetraMCP8DebugKernel< RNGT >::MemcopyWED", "d8/d41/structTetraMCP8DebugKernel_1_1MemcopyWED.html", null ],
    [ "MemTrace::MemTraceEntry", "d3/dbf/structMemTrace_1_1MemTraceEntry.html", null ],
    [ "MMCJSON", "d7/ddb/classMMCJSON.html", null ],
    [ "MMCJSONCase", "db/d11/classMMCJSONCase.html", null ],
    [ "MMCJSONReader", "d6/d2a/classMMCJSONReader.html", null ],
    [ "MMCJSONWriter", "d6/de1/classMMCJSONWriter.html", null ],
    [ "MMCMeshWriter", "dd/db4/classMMCMeshWriter.html", null ],
    [ "MMCOpticalReader", "dd/de6/classMMCOpticalReader.html", null ],
    [ "MMCOpticalWriter", "d8/d1e/classMMCOpticalWriter.html", null ],
    [ "MMCResultReader", "de/deb/classMMCResultReader.html", null ],
    [ "MMCTextMeshReader", "df/d1a/classMMCTextMeshReader.html", null ],
    [ "TIMOS::Optical", "da/d04/structTIMOS_1_1Optical.html", null ],
    [ "ordered_t", "d8/d07/structordered__t.html", null ],
    [ "OrthoBoundingBox< T, D >", "d4/dea/classOrthoBoundingBox.html", null ],
    [ "TetraMCP8Kernel< RNGT >::Output", "d4/d7f/structTetraMCP8Kernel_1_1Output.html", null ],
    [ "OutputData", "d0/d86/classOutputData.html", [
      [ "AbstractSpatialMap", "d6/d12/classAbstractSpatialMap.html", [
        [ "SpatialMap< Value >", "df/d80/classSpatialMap.html", [
          [ "SpatialMap2D< Value >", "da/dcb/classSpatialMap2D.html", null ]
        ] ],
        [ "SpatialMap< float >", "df/d80/classSpatialMap.html", null ]
      ] ],
      [ "AccumulationEvent", "d2/daa/classAccumulationEvent.html", null ],
      [ "AccumulationEventSet", "d7/d37/classAccumulationEventSet.html", null ],
      [ "DirectedSurface", "dd/de5/classDirectedSurface.html", null ],
      [ "DoseHistogram", "d9/d69/classDoseHistogram.html", null ],
      [ "DoseHistogramCollection", "d3/db9/classDoseHistogramCollection.html", null ],
      [ "MCConservationCountsOutput", "d7/d11/classMCConservationCountsOutput.html", null ],
      [ "MCEventCountsOutput", "da/d64/classMCEventCountsOutput.html", null ],
      [ "MeanVarianceSet", "d2/d87/classMeanVarianceSet.html", null ],
      [ "MemTrace", "d9/d54/classMemTrace.html", null ],
      [ "MemTraceSet", "d4/dba/classMemTraceSet.html", null ],
      [ "PacketPositionTraceSet", "df/d27/classPacketPositionTraceSet.html", null ],
      [ "Permutation", "d0/d38/classPermutation.html", null ],
      [ "PhotonData", "dc/d64/classPhotonData.html", null ]
    ] ],
    [ "OutputDataCollection", "d0/d38/classOutputDataCollection.html", null ],
    [ "OutputDataType", "d0/d5d/classOutputDataType.html", null ],
    [ "P8TetrasFromLayered", "db/da6/classP8TetrasFromLayered.html", null ],
    [ "P8TetrasFromTetraMesh", "d9/d6f/classP8TetrasFromTetraMesh.html", null ],
    [ "Packet", "d7/d97/classPacket.html", null ],
    [ "PacketDirection", "d0/d2b/structPacketDirection.html", null ],
    [ "PacketPositionTrace", "d5/d84/classPacketPositionTrace.html", null ],
    [ "PartitionRelabel", "db/d6e/classPartitionRelabel.html", null ],
    [ "TIMOS::PencilBeamSource", "d5/dbd/structTIMOS_1_1PencilBeamSource.html", null ],
    [ "Permute", "d3/dd4/classPermute.html", null ],
    [ "PlacementBase", "dd/de2/classPlacementBase.html", [
      [ "PlanePlacement", "dc/d08/classPlanePlacement.html", null ]
    ] ],
    [ "PlacementMediatorBase", "dc/d3c/classPlacementMediatorBase.html", [
      [ "PlacementMediatorInstance< PlacementT, SourceT >", "d8/dcd/classPlacementMediatorInstance.html", null ],
      [ "PlacementMediatorInstance< PlanePlacement, Source::Line >", "d8/dcd/classPlacementMediatorInstance.html", [
        [ "PlanePlacementLineSource", "d7/dbb/classPlanePlacementLineSource.html", null ]
      ] ],
      [ "PlacementMediatorInstance< PlanePlacement, Source::PencilBeam >", "d8/dcd/classPlacementMediatorInstance.html", [
        [ "PlanePlacementPencilBeam", "d9/d97/classPlanePlacementPencilBeam.html", null ]
      ] ]
    ] ],
    [ "PlanePlacementSurfaceDetector", "d9/d66/classPlanePlacementSurfaceDetector.html", null ],
    [ "Emitter::Point", "d3/dee/classEmitter_1_1Point.html", null ],
    [ "SSE::SSEKernel::Point< D >", "d2/d98/classSSE_1_1SSEKernel_1_1Point.html", null ],
    [ "Emitter::Point< D, T >", "d3/dee/classEmitter_1_1Point.html", [
      [ "Vector< D, T >", "d6/da1/classVector.html", [
        [ "UnitVector< D, T >", "d8/d8e/classUnitVector.html", null ],
        [ "UnitVector< 3 >", "d8/d8e/classUnitVector.html", null ],
        [ "UnitVector< 3, double >", "d8/d8e/classUnitVector.html", null ]
      ] ],
      [ "Vector< 3 >", "d6/da1/classVector.html", null ]
    ] ],
    [ "point_prop< PointProp >", "d4/dcd/structpoint__prop.html", null ],
    [ "PointIntersectionResult", "d3/dab/structPointIntersectionResult.html", null ],
    [ "PointMatcher", "da/d86/classPointMatcher.html", null ],
    [ "Points", "d4/dd1/classPoints.html", null ],
    [ "TIMOS::PointSource", "d8/dd9/structTIMOS_1_1PointSource.html", null ],
    [ "EnergyToFluence::Powers", "d7/d79/structEnergyToFluence_1_1Powers.html", null ],
    [ "QueuedMultiThreadAccumulator< Acc, Delta >", "da/d15/classQueuedMultiThreadAccumulator.html", null ],
    [ "QueuedMultiThreadAccumulator< double, float >", "da/d15/classQueuedMultiThreadAccumulator.html", null ],
    [ "Emitter::RandomInPlane< RNG >", "dd/df6/classEmitter_1_1RandomInPlane.html", null ],
    [ "Ray< D, T >", "d9/dce/classRay.html", null ],
    [ "RayThroughMesh", "df/d89/classRayThroughMesh.html", null ],
    [ "Reader", "d6/d87/classReader.html", null ],
    [ "TetraKernel::ReflectiveFaceDef", "da/d81/structTetraKernel_1_1ReflectiveFaceDef.html", null ],
    [ "DoseHistogramGenerator::ElRegion::RegionComp", "d3/d3f/structDoseHistogramGenerator_1_1ElRegion_1_1RegionComp.html", null ],
    [ "RemapRegions", "df/dea/classRemapRegions.html", null ],
    [ "RemapTable", "d4/d38/classRemapTable.html", null ],
    [ "Rescale", "dc/d36/classRescale.html", null ],
    [ "MemTraceScorer::RLERecord", "d3/dd6/structMemTraceScorer_1_1RLERecord.html", null ],
    [ "RNG_SFMT_AVX", "d6/dfa/classRNG__SFMT__AVX.html", null ],
    [ "RTIntersection", "d9/de5/structRTIntersection.html", null ],
    [ "SeedSweep", "d3/df1/classSeedSweep.html", null ],
    [ "single_delim_comment_filter", "d5/d4e/structsingle__delim__comment__filter.html", null ],
    [ "SingleThreadLoggerT", null, [
      [ "MultiThreadWithIndividualCopy< SingleThreadLoggerT >::Logger", "dd/dea/classMultiThreadWithIndividualCopy_1_1Logger.html", null ]
    ] ],
    [ "Sort", "d3/da9/classSort.html", null ],
    [ "SpatialMapOperator", "df/d96/classSpatialMapOperator.html", null ],
    [ "SpinMatrix", "d2/dc5/classSpinMatrix.html", null ],
    [ "SSE::SSEBase", "df/d6f/classSSE_1_1SSEBase.html", [
      [ "SSE::Scalar", "d1/d20/classSSE_1_1Scalar.html", null ],
      [ "SSE::Vector< D >", "d5/d0a/classSSE_1_1Vector.html", [
        [ "SSE::UnitVector< D >", "d3/d84/classSSE_1_1UnitVector.html", null ]
      ] ]
    ] ],
    [ "SSE::SSEKernel", "d7/d42/classSSE_1_1SSEKernel.html", null ],
    [ "StandardArrayKernel< T, D >", "d3/dae/structStandardArrayKernel.html", null ],
    [ "StatisticalTest", "d1/de0/classStatisticalTest.html", [
      [ "Chi2StatisticalTest", "d5/dee/classChi2StatisticalTest.html", null ]
    ] ],
    [ "PacketPositionTrace::Step", "d4/d88/structPacketPositionTrace_1_1Step.html", null ],
    [ "StepResult", "db/d20/structStepResult.html", null ],
    [ "SurfaceAccumulationProbe", "dc/da6/classSurfaceAccumulationProbe.html", null ],
    [ "SurfaceSourceBuilder", "de/d90/classSurfaceSourceBuilder.html", [
      [ "AxialSurfaceSourceBuilder", "d6/ddc/classAxialSurfaceSourceBuilder.html", null ],
      [ "CylinderSurfaceSourceBuilder", "d6/dea/classCylinderSurfaceSourceBuilder.html", null ]
    ] ],
    [ "swig_traits< T >", "dc/d95/classswig__traits.html", null ],
    [ "take", "da/db6/structtake.html", null ],
    [ "Emitter::Tetra< RNG >", "d6/da9/classEmitter_1_1Tetra.html", null ],
    [ "Tetra", "d5/dfa/classTetra.html", null ],
    [ "TetraCUDAKernel", "d3/dc9/classTetraCUDAKernel.html", [
      [ "TetraMCCUDAKernel< RNGT >", "d2/dd5/classTetraMCCUDAKernel.html", null ],
      [ "TetraMCCUDAKernel< RNG_SFMT_AVX >", "d2/dd5/classTetraMCCUDAKernel.html", null ]
    ] ],
    [ "TetraEnclosingPointByLinearSearch", "de/da8/classTetraEnclosingPointByLinearSearch.html", null ],
    [ "TetraFaceLink", "d2/dbb/structTetraFaceLink.html", null ],
    [ "TetraFPGACLKernel", "d2/d0d/classTetraFPGACLKernel.html", [
      [ "TetraMCFPGACLKernel< RNGT >", "d4/df6/classTetraMCFPGACLKernel.html", null ],
      [ "TetraMCFPGACLKernel< RNG_SFMT_AVX >", "d4/df6/classTetraMCFPGACLKernel.html", null ]
    ] ],
    [ "TIMOSOutputReader::TetraInfo", "df/d9b/structTIMOSOutputReader_1_1TetraInfo.html", null ],
    [ "TetraKernel", "d9/d26/classTetraKernel.html", [
      [ "TetraMCKernel< RNGT, ScorerT >", "d6/d5e/classTetraMCKernel.html", null ],
      [ "TetraMCKernel< RNG_SFMT_AVX, InternalScorer >", "d6/d5e/classTetraMCKernel.html", null ],
      [ "TetraMCKernel< RNG_SFMT_AVX, MCMLScorerPack >", "d6/d5e/classTetraMCKernel.html", null ],
      [ "TetraMCKernel< RNG_SFMT_AVX, MCMLScorerPackQ >", "d6/d5e/classTetraMCKernel.html", null ],
      [ "TetraMCKernel< RNG_SFMT_AVX, MCMLScorerPackWithTraces >", "d6/d5e/classTetraMCKernel.html", null ],
      [ "TetraMCKernel< RNG_SFMT_AVX, SVScorer >", "d6/d5e/classTetraMCKernel.html", null ],
      [ "TetraMCKernel< RNG_SFMT_AVX, TetraSurfaceScorer >", "d6/d5e/classTetraMCKernel.html", null ],
      [ "TetraMCKernel< RNG_SFMT_AVX, TetraVolumeScorer >", "d6/d5e/classTetraMCKernel.html", null ],
      [ "TetraMCKernel< RNG_SFMT_AVX, TraceScorer >", "d6/d5e/classTetraMCKernel.html", null ]
    ] ],
    [ "FaceTetraLink::TetraLink", "d2/d72/structFaceTetraLink_1_1TetraLink.html", null ],
    [ "TetraLookupCache", "da/de0/classTetraLookupCache.html", null ],
    [ "TetraMeshBuilder", "dc/d7f/classTetraMeshBuilder.html", [
      [ "PFTetraMeshBuilder", "dd/deb/classPFTetraMeshBuilder.html", null ],
      [ "PTTetraMeshBuilder", "dd/db5/classPTTetraMeshBuilder.html", null ]
    ] ],
    [ "TetraMesh::TetraMeshRTree", "d8/d43/classTetraMesh_1_1TetraMeshRTree.html", null ],
    [ "TetraP8DebugKernel", "d1/d9c/classTetraP8DebugKernel.html", [
      [ "TetraMCP8DebugKernel< RNGT >", "da/d65/classTetraMCP8DebugKernel.html", null ],
      [ "TetraMCP8DebugKernel< RNG_SFMT_AVX >", "da/d65/classTetraMCP8DebugKernel.html", null ]
    ] ],
    [ "TetraP8Kernel", "d4/d02/classTetraP8Kernel.html", [
      [ "TetraMCP8Kernel< RNGT >", "d8/d42/classTetraMCP8Kernel.html", null ],
      [ "TetraMCP8Kernel< RNG_SFMT_AVX >", "d8/d42/classTetraMCP8Kernel.html", null ]
    ] ],
    [ "TetrasFromLayered", "dc/d02/classTetrasFromLayered.html", null ],
    [ "TetrasFromTetraMesh", "de/db6/classTetrasFromTetraMesh.html", null ],
    [ "TetrasNearPoint", "dd/da7/classTetrasNearPoint.html", null ],
    [ "TIMOS::TetraSource", "d5/dd7/structTIMOS_1_1TetraSource.html", null ],
    [ "TextFile", "d5/d67/classTextFile.html", [
      [ "TextFileMeshWriter", "de/d71/classTextFileMeshWriter.html", null ]
    ] ],
    [ "TextFileDoseHistogramWriter", "de/d96/classTextFileDoseHistogramWriter.html", null ],
    [ "TextFileMatrixReader", "db/dd2/classTextFileMatrixReader.html", null ],
    [ "TextFileMatrixWriter", "dc/da1/classTextFileMatrixWriter.html", null ],
    [ "TextFileMeanVarianceReader", "d6/d0a/classTextFileMeanVarianceReader.html", null ],
    [ "TextFileMeanVarianceWriter", "d2/d6e/classTextFileMeanVarianceWriter.html", null ],
    [ "TextFileReader", "dd/dcd/classTextFileReader.html", [
      [ "TIMOSMaterialReader", "d2/db6/classTIMOSMaterialReader.html", null ],
      [ "TIMOSSourceReader", "d5/ddb/classTIMOSSourceReader.html", null ]
    ] ],
    [ "ThetaDistributionFunc", "d5/df8/classThetaDistributionFunc.html", null ],
    [ "ThreadedMCKernelBase::Thread", "da/d38/classThreadedMCKernelBase_1_1Thread.html", [
      [ "TetraMCKernel< RNGT, ScorerT >::Thread", "d1/d3d/classTetraMCKernel_1_1Thread.html", null ]
    ] ],
    [ "AtomicMultiThreadAccumulator< Acc, Delta >::ThreadHandle", "df/dd6/classAtomicMultiThreadAccumulator_1_1ThreadHandle.html", null ],
    [ "QueuedMultiThreadAccumulator< Acc, Delta >::ThreadHandle", "d8/d2f/classQueuedMultiThreadAccumulator_1_1ThreadHandle.html", null ],
    [ "TIMOSMaterialWriter", "df/d87/classTIMOSMaterialWriter.html", null ],
    [ "TIMOSMeshReader", "d7/da6/classTIMOSMeshReader.html", null ],
    [ "TIMOSMeshWriter", "d0/d57/classTIMOSMeshWriter.html", null ],
    [ "TIMOSOutputReader", "d5/d0d/classTIMOSOutputReader.html", null ],
    [ "TIMOSSourceWriter", "df/d22/classTIMOSSourceWriter.html", null ],
    [ "Emitter::Triangle< RNG >", "d3/d49/classEmitter_1_1Triangle.html", null ],
    [ "UniformUI32Distribution", "df/d80/classUniformUI32Distribution.html", null ],
    [ "UnitConverter", "d4/d4a/classUnitConverter.html", null ],
    [ "SSE::SSEKernel::UnitVector< D >", "db/d94/classSSE_1_1SSEKernel_1_1UnitVector.html", null ],
    [ "SSE::SSEKernel::Vector< D >", "de/d91/classSSE_1_1SSEKernel_1_1Vector.html", null ],
    [ "visitable_base< VisitorT >", "dc/d0c/classvisitable__base.html", null ],
    [ "visitable_base< Visitor >", "dc/d0c/classvisitable__base.html", [
      [ "visitable< Base, Derived, Visitor >", "d8/d61/classvisitable.html", null ]
    ] ],
    [ "Source::Abstract::Visitor", "d7/dc2/classSource_1_1Abstract_1_1Visitor.html", [
      [ "Emitter::TetraEmitterFactory< RNG >", "d3/d23/classEmitter_1_1TetraEmitterFactory.html", null ],
      [ "ExportVisitor", "d2/df1/classExportVisitor.html", null ],
      [ "Source::Printer", "d1/d33/classSource_1_1Printer.html", null ],
      [ "TIMOSSourceWriter::CountVisitor", "d3/dc9/classTIMOSSourceWriter_1_1CountVisitor.html", null ],
      [ "TIMOSSourceWriter::SourceVisitor", "d3/d73/classTIMOSSourceWriter_1_1SourceVisitor.html", null ]
    ] ],
    [ "visitor< Base, Visitor >", "d9/d50/classvisitor.html", null ],
    [ "OutputData::Visitor", "dc/dad/classOutputData_1_1Visitor.html", [
      [ "OutputDataSummarize", "d1/d63/classOutputDataSummarize.html", null ],
      [ "TextFileWriter", "de/d36/classTextFileWriter.html", null ]
    ] ],
    [ "VolumeAccumulationProbe", "d2/dee/classVolumeAccumulationProbe.html", null ],
    [ "vtkFullMontePlanePlacement", "df/d4f/classvtkFullMontePlanePlacement.html", null ],
    [ "VTKMeshReader", "d8/df8/classVTKMeshReader.html", null ],
    [ "VTKMeshWriter", "d9/d60/classVTKMeshWriter.html", null ],
    [ "vtkObject", null, [
      [ "vtkDoseHistogramWrapper", "d8/dba/classvtkDoseHistogramWrapper.html", null ],
      [ "vtkFullMonteArrayAdaptor", "d3/d84/classvtkFullMonteArrayAdaptor.html", null ],
      [ "vtkFullMonteFluenceLineQueryWrapper", "da/d14/classvtkFullMonteFluenceLineQueryWrapper.html", null ],
      [ "vtkFullMonteMeshFacesToPolyData", "d7/dd9/classvtkFullMonteMeshFacesToPolyData.html", null ],
      [ "vtkFullMonteMeshFromUnstructuredGrid", "d7/d91/classvtkFullMonteMeshFromUnstructuredGrid.html", null ],
      [ "vtkFullMontePacketPositionTraceSetToPolyData", "d9/d6d/classvtkFullMontePacketPositionTraceSetToPolyData.html", null ],
      [ "vtkFullMonteTetraMeshWrapper", "d7/d0c/classvtkFullMonteTetraMeshWrapper.html", null ],
      [ "vtkSourceExporter", "de/d93/classvtkSourceExporter.html", null ]
    ] ],
    [ "VTKPhotonPathWriter", "d2/dec/classVTKPhotonPathWriter.html", null ],
    [ "VTKSurfaceWriter", "d0/dc8/classVTKSurfaceWriter.html", null ],
    [ "WalkFaceInfo", "d0/d1c/structWalkFaceInfo.html", null ],
    [ "WalkSegment", "d8/d9a/structWalkSegment.html", null ],
    [ "WrappedInteger< I >", "d7/d2d/classWrappedInteger.html", null ],
    [ "WrappedInteger< unsigned >", "d7/d2d/classWrappedInteger.html", [
      [ "TetraMesh::DirectedFaceDescriptor", "df/dfa/classTetraMesh_1_1DirectedFaceDescriptor.html", null ],
      [ "TetraMesh::FaceDescriptor", "d0/de0/classTetraMesh_1_1FaceDescriptor.html", null ],
      [ "TetraMesh::PointDescriptor", "d4/d30/classTetraMesh_1_1PointDescriptor.html", null ],
      [ "TetraMesh::TetraDescriptor", "d7/d6f/classTetraMesh_1_1TetraDescriptor.html", null ]
    ] ],
    [ "WrappedVector< T, Index >", "dc/def/classWrappedVector.html", null ],
    [ "WrappedVector< Material *, unsigned >", "dc/def/classWrappedVector.html", [
      [ "MaterialSet", "d1/d43/classMaterialSet.html", null ]
    ] ],
    [ "WrappedVector< std::array< unsigned, N >, unsigned >", "dc/def/classWrappedVector.html", [
      [ "Cells< N >", "d4/dcd/classCells.html", null ]
    ] ],
    [ "WrappedVector< unsigned, unsigned >", "dc/def/classWrappedVector.html", [
      [ "Partition", "d5/d03/classPartition.html", null ]
    ] ],
    [ "xmm_mm_union", "db/dd2/unionxmm__mm__union.html", null ],
    [ "ZScoresComparison", "df/df4/classZScoresComparison.html", null ]
];