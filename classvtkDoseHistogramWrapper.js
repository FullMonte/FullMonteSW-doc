var classvtkDoseHistogramWrapper =
[
    [ "MeasureType", "classvtkDoseHistogramWrapper.html#ab464f897d2d7ebc624e50377cd864fab", [
      [ "Unknown", "classvtkDoseHistogramWrapper.html#ab464f897d2d7ebc624e50377cd864faba634e8e1750f546d110d30c755bbe87b1", null ],
      [ "Surface", "classvtkDoseHistogramWrapper.html#ab464f897d2d7ebc624e50377cd864faba11df2408a7501897ceaa5adc74545528", null ],
      [ "Volume", "classvtkDoseHistogramWrapper.html#ab464f897d2d7ebc624e50377cd864fabaa0a75532ba70574aae3c617707d4e77a", null ]
    ] ],
    [ "vtkDoseHistogramWrapper", "classvtkDoseHistogramWrapper.html#a6d81c4821846d83c7ac095a46896586f", null ],
    [ "~vtkDoseHistogramWrapper", "classvtkDoseHistogramWrapper.html#a724293265845a908a6d019c6c8e88323", null ],
    [ "measureMode", "classvtkDoseHistogramWrapper.html#a6f571d5b69314449a4e44ffe65aa64da", null ],
    [ "measureMode", "classvtkDoseHistogramWrapper.html#aafc7e6be3fa5bdce4b9206d8eef015a2", null ],
    [ "measureType", "classvtkDoseHistogramWrapper.html#a5dcb77bbc0b16f72836a5e227f03dbb2", null ],
    [ "measureType", "classvtkDoseHistogramWrapper.html#a4d99aab15b745a67f72cacbde5fabb5d", null ],
    [ "source", "classvtkDoseHistogramWrapper.html#afd91ff7a304ac54825feb316a486fbc8", null ],
    [ "table", "classvtkDoseHistogramWrapper.html#ace649eb7405b763d8fd472785dd4f8e9", null ],
    [ "update", "classvtkDoseHistogramWrapper.html#a8dbea0456a518a4d91268eb949669ff5", null ],
    [ "vtkTypeMacro", "classvtkDoseHistogramWrapper.html#a7f782553c34c4e94dc8f6354c88f3a33", null ]
];