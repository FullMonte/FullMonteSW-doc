var classAbsorptionSum =
[
    [ "AbsorptionSum", "classAbsorptionSum.html#ac616fe8514ac2b8c6b41c3323883963c", null ],
    [ "~AbsorptionSum", "classAbsorptionSum.html#a70d481457b155b3ba077eeb9bbd9b0a6", null ],
    [ "data", "classAbsorptionSum.html#a0ece8c345a91c562660a3ddba30a2796", null ],
    [ "data", "classAbsorptionSum.html#aacccb8ed6c15e4b9cf8e9a2f9a470095", null ],
    [ "partition", "classAbsorptionSum.html#add65d644d19d4c3d307e7f60dbd5ff87", null ],
    [ "partition", "classAbsorptionSum.html#ac9846b8ae296e3711ceb059ebdc3fbe0", null ],
    [ "totalForAllElements", "classAbsorptionSum.html#a8dc1f1853f6db88bddf7ca8e3c6270b6", null ],
    [ "totalForPartition", "classAbsorptionSum.html#acfb165926b4b96181cd302cc2ad9de32", null ],
    [ "update", "classAbsorptionSum.html#a2734058f40bb55b8baa046d9ef2c6de1", null ]
];