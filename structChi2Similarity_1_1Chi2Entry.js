var structChi2Similarity_1_1Chi2Entry =
[
    [ "criticalValue", "structChi2Similarity_1_1Chi2Entry.html#a7e35d7c5f159defe8302dec5bb5e1086", null ],
    [ "pValue", "structChi2Similarity_1_1Chi2Entry.html#ac4e9f9e52297dbbdd512cbb2854ec6cb", null ],
    [ "chi2", "structChi2Similarity_1_1Chi2Entry.html#ac4cdcfd892bb5552312999f648211b87", null ],
    [ "cv", "structChi2Similarity_1_1Chi2Entry.html#a923d4487389e3ec39d05899caf4e3ce4", null ],
    [ "df", "structChi2Similarity_1_1Chi2Entry.html#aa6a16ca868fdd01ec9ae58863cca1443", null ],
    [ "Esum", "structChi2Similarity_1_1Chi2Entry.html#a2966f638b2baea49ab88c435a8c6efd0", null ],
    [ "ID", "structChi2Similarity_1_1Chi2Entry.html#a4d65c67c66bdd691fca7c5c00a45cc00", null ],
    [ "index", "structChi2Similarity_1_1Chi2Entry.html#a29aeffdf3e98d2868ab86339ceaabb3d", null ],
    [ "mu", "structChi2Similarity_1_1Chi2Entry.html#aa692350bf5b923bc2b241f0f3856ea1d", null ],
    [ "p", "structChi2Similarity_1_1Chi2Entry.html#a23455079d31b8ff98720b5c5ce64c3ef", null ],
    [ "region", "structChi2Similarity_1_1Chi2Entry.html#a70e94a9147a2626b1ecbb26b7c5b8d96", null ],
    [ "sigma", "structChi2Similarity_1_1Chi2Entry.html#abd21dd68b9d670c9c0e65ca18e58c752", null ],
    [ "test", "structChi2Similarity_1_1Chi2Entry.html#a2bf9d4fa69307a1025a829d1eb76a40b", null ]
];