var classOrthoBoundingBox =
[
    [ "OrthoBoundingBox", "d4/dea/classOrthoBoundingBox.html#a0899cc687db44460164e5cb1e4b68008", null ],
    [ "OrthoBoundingBox", "d4/dea/classOrthoBoundingBox.html#a707098c0a4caa75e8c708666cb0bcf45", null ],
    [ "OrthoBoundingBox", "d4/dea/classOrthoBoundingBox.html#aa9082279549cef0e209dc3e109c97044", null ],
    [ "OrthoBoundingBox", "d4/dea/classOrthoBoundingBox.html#a40bc1036dff02011120c42df5eae83b2", null ],
    [ "OrthoBoundingBox", "d4/dea/classOrthoBoundingBox.html#aae85a7b937493de4feafb5ba3561c2c8", null ],
    [ "OrthoBoundingBox", "d4/dea/classOrthoBoundingBox.html#ae6a510721a91b60db0e7d0f58e1a03b8", null ],
    [ "contains", "d4/dea/classOrthoBoundingBox.html#a8810e93669afaed9f33af4aaedce2e3f", null ],
    [ "corners", "d4/dea/classOrthoBoundingBox.html#a766baa3070c742b66a447ea6b7f5fd44", null ],
    [ "corners", "d4/dea/classOrthoBoundingBox.html#abb0de3d5977a0670f9c3492c13484cbd", null ],
    [ "dims", "d4/dea/classOrthoBoundingBox.html#af4650d866552a4c3a5bc8d6707bc5d8d", null ],
    [ "dims", "d4/dea/classOrthoBoundingBox.html#a8a9673670f623fd1422086ed8d922fc4", null ],
    [ "insert", "d4/dea/classOrthoBoundingBox.html#af5883b81feb2628d1108d7b4216845b7", null ],
    [ "insert", "d4/dea/classOrthoBoundingBox.html#a26b92f07006d20317eaa52db71e11f58", null ],
    [ "inside_tester", "d4/dea/classOrthoBoundingBox.html#a56d8fbf859ee860e7541a99cd41aeed9", null ],
    [ "point_inside", "d4/dea/classOrthoBoundingBox.html#a2479c28927970d24801af5cf29dc0afa", null ],
    [ "scale_offset", "d4/dea/classOrthoBoundingBox.html#a30938ec1b2fed76b8ec965fd7d6a4d39", null ],
    [ "scale_offset", "d4/dea/classOrthoBoundingBox.html#a643db897a1d3ef558fc2b92e2d3fa3e6", null ],
    [ "m_corners", "d4/dea/classOrthoBoundingBox.html#a3776128ee52e0f663b6636ce9967caf3", null ],
    [ "m_max", "d4/dea/classOrthoBoundingBox.html#a8d565893e4eb92ef5e8f4097c4cfa8ac", null ],
    [ "m_min", "d4/dea/classOrthoBoundingBox.html#a3c2f09b2ce2f9d3d15ac7d801e50b7f2", null ]
];