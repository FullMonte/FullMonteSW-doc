var structFullMonteHW_1_1ReflectRefractRequest =
[
    [ "serialize", "d4/d38/structFullMonteHW_1_1ReflectRefractRequest.html#acbb4e7fca8c9b52859fc2469231230ba", null ],
    [ "bernoulli_rnd", "d4/d38/structFullMonteHW_1_1ReflectRefractRequest.html#a166fe12b1c69ecc9f8dc62e5d6ea0f01", null ],
    [ "bits", "d4/d38/structFullMonteHW_1_1ReflectRefractRequest.html#a2d93c92879c27cb3dd8384a6e7650fba", null ],
    [ "costheta", "d4/d38/structFullMonteHW_1_1ReflectRefractRequest.html#a11dcf837dbf94e5c2688a934941a602d", null ],
    [ "d", "d4/d38/structFullMonteHW_1_1ReflectRefractRequest.html#a630bf0c049be9c05791a5a3d6d4098de", null ],
    [ "ifcID", "d4/d38/structFullMonteHW_1_1ReflectRefractRequest.html#a1a5ae0e0029a84a127280561eda0fd8c", null ],
    [ "normal", "d4/d38/structFullMonteHW_1_1ReflectRefractRequest.html#a757e819c25a2c5d1a5e3e1d66ef55296", null ]
];