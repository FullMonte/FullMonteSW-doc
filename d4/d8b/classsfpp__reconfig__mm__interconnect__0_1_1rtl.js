var classsfpp__reconfig__mm__interconnect__0_1_1rtl =
[
    [ "altera_merlin_master_translator", "d4/d8b/classsfpp__reconfig__mm__interconnect__0_1_1rtl.html#a16197cdc6c6c3f6e7dc53e7968045d7b", null ],
    [ "altera_merlin_slave_translator", "d4/d8b/classsfpp__reconfig__mm__interconnect__0_1_1rtl.html#a5f8a6b9a7146a5423e225d6fece83b97", null ],
    [ "master_0_master_translator_avalon_universal_master_0_address", "d4/d8b/classsfpp__reconfig__mm__interconnect__0_1_1rtl.html#a175f724a4a5778a24319b63aa69af408", null ],
    [ "master_0_master_translator_avalon_universal_master_0_burstcount", "d4/d8b/classsfpp__reconfig__mm__interconnect__0_1_1rtl.html#a18f509c83d72273c412f10b9972179b7", null ],
    [ "master_0_master_translator_avalon_universal_master_0_byteenable", "d4/d8b/classsfpp__reconfig__mm__interconnect__0_1_1rtl.html#a6752bd009a0f2cb254f206c3ab4b895d", null ],
    [ "master_0_master_translator_avalon_universal_master_0_debugaccess", "d4/d8b/classsfpp__reconfig__mm__interconnect__0_1_1rtl.html#ae9799439d56e6d2704233689cb3d929e", null ],
    [ "master_0_master_translator_avalon_universal_master_0_lock", "d4/d8b/classsfpp__reconfig__mm__interconnect__0_1_1rtl.html#adf02b5bfa6fc6d856aa3f5dc64f41694", null ],
    [ "master_0_master_translator_avalon_universal_master_0_read", "d4/d8b/classsfpp__reconfig__mm__interconnect__0_1_1rtl.html#a244fcde200c9af3b47df685d794fc157", null ],
    [ "master_0_master_translator_avalon_universal_master_0_readdata", "d4/d8b/classsfpp__reconfig__mm__interconnect__0_1_1rtl.html#ab0fd9bdd68657bb1564018811310fe30", null ],
    [ "master_0_master_translator_avalon_universal_master_0_readdatavalid", "d4/d8b/classsfpp__reconfig__mm__interconnect__0_1_1rtl.html#af4e56cc4a8d77bcbfe892a7e21c0e3ed", null ],
    [ "master_0_master_translator_avalon_universal_master_0_waitrequest", "d4/d8b/classsfpp__reconfig__mm__interconnect__0_1_1rtl.html#af6ae29357f36bc1f5dbdee9b33b39c28", null ],
    [ "master_0_master_translator_avalon_universal_master_0_write", "d4/d8b/classsfpp__reconfig__mm__interconnect__0_1_1rtl.html#adf66342d572239632f7ae3227d32a327", null ],
    [ "master_0_master_translator_avalon_universal_master_0_writedata", "d4/d8b/classsfpp__reconfig__mm__interconnect__0_1_1rtl.html#ad8998a9814894ffe56a880ab37e177c4", null ]
];