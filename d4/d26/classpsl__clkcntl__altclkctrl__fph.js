var classpsl__clkcntl__altclkctrl__fph =
[
    [ "psl_clkcntl_altclkctrl_fph.RTL", "d8/d4e/classpsl__clkcntl__altclkctrl__fph_1_1RTL.html", "d8/d4e/classpsl__clkcntl__altclkctrl__fph_1_1RTL" ],
    [ "all", "d4/d26/classpsl__clkcntl__altclkctrl__fph.html#a496946683e27a472ffe8ce4a4b90c36b", null ],
    [ "ena", "d4/d26/classpsl__clkcntl__altclkctrl__fph.html#a965377076554f998b11fa00f8620da26", null ],
    [ "ieee", "d4/d26/classpsl__clkcntl__altclkctrl__fph.html#a943c78218d2ad4bbbf3aab39bb9269be", null ],
    [ "inclk", "d4/d26/classpsl__clkcntl__altclkctrl__fph.html#a9e70df95b746d3b129f5990f25d8097c", null ],
    [ "outclk", "d4/d26/classpsl__clkcntl__altclkctrl__fph.html#a2042c5cdda31e05d634015522acc6db0", null ],
    [ "std_logic_1164", "d4/d26/classpsl__clkcntl__altclkctrl__fph.html#a32b18b3a8e2ae7a9552631df6dae9855", null ],
    [ "stratixv", "d4/d26/classpsl__clkcntl__altclkctrl__fph.html#a9ff75ed347ed8fed835f217eff22d097", null ]
];