var SinCosCordic_2check_8m =
[
    [ "check", "d4/de5/SinCosCordic_2check_8m.html#affd055b59c71c3b478cc9cade5c05933", null ],
    [ "fclose", "d4/de5/SinCosCordic_2check_8m.html#a5e769bbbabcaddc548203741c7100228", null ],
    [ "hist", "d4/de5/SinCosCordic_2check_8m.html#a830650b433e45a176a073802b30c5a64", null ],
    [ "legend", "d4/de5/SinCosCordic_2check_8m.html#a8ac8e99c11d0dfa8f9f953329db8754b", null ],
    [ "plot", "d4/de5/SinCosCordic_2check_8m.html#a2336717392cf3b7f0f95d784bcf3f8e9", null ],
    [ "size", "d4/de5/SinCosCordic_2check_8m.html#a7e3acad24bd49cb870d4a58d79f6dbf1", null ],
    [ "cos_dut", "d4/de5/SinCosCordic_2check_8m.html#a0e5f38ff9e7e111b098fffcac5ac1a6e", null ],
    [ "cos_expect", "d4/de5/SinCosCordic_2check_8m.html#a04994be7c744db1d305f98bbf8646eb4", null ],
    [ "fid", "d4/de5/SinCosCordic_2check_8m.html#ae9011d40c6f13e68e6f07156e0da7c5d", null ],
    [ "figure", "d4/de5/SinCosCordic_2check_8m.html#a391e34f2de441d79152a7b3d6e4c9c86", null ],
    [ "oSub", "d4/de5/SinCosCordic_2check_8m.html#a5ee4eab5e44e2693f96f706df72a057f", null ],
    [ "sin_dut", "d4/de5/SinCosCordic_2check_8m.html#abe0f7f84b2a3d2bd54ebc548db4a852b", null ],
    [ "sin_expect", "d4/de5/SinCosCordic_2check_8m.html#ab0eec4431ead1f90558fc801bdc7b5c3", null ],
    [ "T", "d4/de5/SinCosCordic_2check_8m.html#adf1f3edb9115acb0a1e04209b7a9937b", null ],
    [ "thetaSub", "d4/de5/SinCosCordic_2check_8m.html#acefe51a316a96e53ca3cb16175136792", null ]
];