var classCUDAAccel =
[
    [ "CUDAAccel", "d4/d18/classCUDAAccel.html#a5f3b49a1d7e359fcde44e95f83460600", null ],
    [ "~CUDAAccel", "d4/d18/classCUDAAccel.html#af29ce72d1250a8decce52acf1494e384", null ],
    [ "changeDevice", "d4/d18/classCUDAAccel.html#a88f3b0fba2b67b951ea0fcaaa78a7af0", null ],
    [ "copyDirectedSurfaceEnergy", "d4/d18/classCUDAAccel.html#aa155b89b2b6fb06becaae7772ba4cf04", null ],
    [ "copyDirectedSurfaceEnergy", "d4/d18/classCUDAAccel.html#a061fe9795c1ef4259e3f391625ce5301", null ],
    [ "copySurfaceExitEnergy", "d4/d18/classCUDAAccel.html#a136e888cfa2a499bf9448267a076530b", null ],
    [ "copySurfaceExitEnergy", "d4/d18/classCUDAAccel.html#aaa355df771692c8534eec2536a63e7be", null ],
    [ "copyTetraEnergy", "d4/d18/classCUDAAccel.html#aa1936994ccad4c51cce4abe6001456a7", null ],
    [ "copyTetraEnergy", "d4/d18/classCUDAAccel.html#a06955101c392a7de761473799bcf91ab", null ],
    [ "currentDeviceID", "d4/d18/classCUDAAccel.html#a8fb936b16c5398bcc3fc5306028de81f", null ],
    [ "destroy", "d4/d18/classCUDAAccel.html#a8471d19eff936b50fe3fc8f2806bf8b1", null ],
    [ "init", "d4/d18/classCUDAAccel.html#a40f25e632ba3a338e1f89e745279e7df", null ],
    [ "isDeviceCapable", "d4/d18/classCUDAAccel.html#ac7eef85952f0b361b98d4aea52afed5f", null ],
    [ "isDeviceCapable", "d4/d18/classCUDAAccel.html#ae51913de3b0dae404f8ce69d83d5f6e6", null ],
    [ "launch", "d4/d18/classCUDAAccel.html#a7012b583c510a272175b3de69d4c73c8", null ],
    [ "listDevices", "d4/d18/classCUDAAccel.html#a03f1d1777aa772bc25595dd1c0e81211", null ],
    [ "maxPacketsPossible", "d4/d18/classCUDAAccel.html#aae9b3f6075740d16ea0909f96328d6c3", null ],
    [ "maxTetraEstimate", "d4/d18/classCUDAAccel.html#ab47667fd20ea9c95d05b7b42f9117862", null ],
    [ "sync", "d4/d18/classCUDAAccel.html#a1e152bece7166230402c7568228a52aa", null ],
    [ "devId", "d4/d18/classCUDAAccel.html#ae84787e42864696bae9e33d1458f8cf8", null ],
    [ "faceEnergy1_d", "d4/d18/classCUDAAccel.html#aace6c3491c48baa8e57d22af0c2613e1", null ],
    [ "faceEnergy2_d", "d4/d18/classCUDAAccel.html#a39c74f5cd8bbf8a97cd81e00075df076", null ],
    [ "facesCount", "d4/d18/classCUDAAccel.html#a9a8cea78604e52eef665baac670c3ed8", null ],
    [ "lastPacketCount", "d4/d18/classCUDAAccel.html#ad74f0d079cf015c47dd8456ce5438012", null ],
    [ "launchedPackets_d", "d4/d18/classCUDAAccel.html#a526210d407c920302311fadf0f3fa8ab", null ],
    [ "materialsCount", "d4/d18/classCUDAAccel.html#a1800fcd49cb215f26cca4f98b59ca341", null ],
    [ "maxBlocks", "d4/d18/classCUDAAccel.html#ac04b9fcf87fd12bca811206514080777", null ],
    [ "maxThreadsPerBlock", "d4/d18/classCUDAAccel.html#a9e8bae0b9af5745f035e8d6616d4d5f9", null ],
    [ "meshExitEnergy_d", "d4/d18/classCUDAAccel.html#acad59fd69c98f515935cb79b4a222859", null ],
    [ "NhitMax", "d4/d18/classCUDAAccel.html#aef7a2d103b40991497aba50dfdd5170c", null ],
    [ "NstepMax", "d4/d18/classCUDAAccel.html#ab3a627d0874b33d5db05a414d26f6775", null ],
    [ "prefThreadsPerBlock", "d4/d18/classCUDAAccel.html#a162fc8e12024a92bd0964f1913f71ed4", null ],
    [ "prWin", "d4/d18/classCUDAAccel.html#a15baeb4b4ba2acfadadd7aeacaac6927", null ],
    [ "rngState_d", "d4/d18/classCUDAAccel.html#acd102815d19cb0e97394fe9dff2a6bdf", null ],
    [ "scoreDirectedSurface", "d4/d18/classCUDAAccel.html#a3c7c3199163f8a8db448fd0c54449237", null ],
    [ "scoreSurfaceExit", "d4/d18/classCUDAAccel.html#a3fe40ad45d8cc97eb11389132c4e0ae2", null ],
    [ "scoreVolume", "d4/d18/classCUDAAccel.html#a43e133020ef3446bc86801a11d8d6a31", null ],
    [ "seed", "d4/d18/classCUDAAccel.html#ab3f383179d21775dc7b4029f666bcb9e", null ],
    [ "tetraEnergy_d", "d4/d18/classCUDAAccel.html#a36880da7a7836b31aa8580e81e2801a4", null ],
    [ "tetras_d", "d4/d18/classCUDAAccel.html#ac891234eab85502baa080605060333e0", null ],
    [ "tetrasCount", "d4/d18/classCUDAAccel.html#a61e4f17f88154bf68f67760ef6d7b33d", null ],
    [ "wMin", "d4/d18/classCUDAAccel.html#a63f073d147bece3fbfe5d12c61be5312", null ]
];