var structBoundaryTraits_1_1Output =
[
    [ "age", "d4/d77/structBoundaryTraits_1_1Output.html#ab0b8273da15f1f455ed836af6c0e84ce", null ],
    [ "costheta", "d4/d77/structBoundaryTraits_1_1Output.html#ab5d1d1f100d46aa532ff82d26c64ee9f", null ],
    [ "dir", "d4/d77/structBoundaryTraits_1_1Output.html#adcde73ca5b9ba35a83f9a8e021ae4977", null ],
    [ "ifcID", "d4/d77/structBoundaryTraits_1_1Output.html#a9ba4516984b28e3365e2415040f66df9", null ],
    [ "l", "d4/d77/structBoundaryTraits_1_1Output.html#a6e74b40d98dead06da50c296c284a355", null ],
    [ "matID", "d4/d77/structBoundaryTraits_1_1Output.html#a63ee9780427da94523ce2314415337ee", null ],
    [ "pos", "d4/d77/structBoundaryTraits_1_1Output.html#aa92ece659ee29aab79974574db2c77d8", null ],
    [ "refractive", "d4/d77/structBoundaryTraits_1_1Output.html#a6b329a19d8d9651c408444268ed5d3d4", null ],
    [ "tag", "d4/d77/structBoundaryTraits_1_1Output.html#ad1bc305f9a08c68ede023215479b36ac", null ],
    [ "tetID", "d4/d77/structBoundaryTraits_1_1Output.html#a1b95e1edb4fba0438675e1703e8b910b", null ],
    [ "tetraID", "d4/d77/structBoundaryTraits_1_1Output.html#a7c667b49203d8e0b8586ee55413daf90", null ],
    [ "w", "d4/d77/structBoundaryTraits_1_1Output.html#abe6ef72e7db923b896e00cffd6c9f297", null ]
];