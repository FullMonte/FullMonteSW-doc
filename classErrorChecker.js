var classErrorChecker =
[
    [ "ErrorChecker", "classErrorChecker.html#ab5ef76a2621ed8a914811f8498691d66", null ],
    [ "~ErrorChecker", "classErrorChecker.html#ad09b2ebb17895e0ed21b772976599e65", null ],
    [ "check", "classErrorChecker.html#ae99d4c6255ba0f589d1fa37d7a47d76f", null ],
    [ "clear", "classErrorChecker.html#a98d5d330c4ba0010faa3191f526c192c", null ],
    [ "currentError", "classErrorChecker.html#a5321032723a45d142162164599918b89", null ],
    [ "errors", "classErrorChecker.html#a4b4c722295f236361539ed74e1883844", null ],
    [ "nextError", "classErrorChecker.html#a4e22e06bd0b09c28310630ecf6d643a5", null ],
    [ "nextTestCase", "classErrorChecker.html#ab564eb639bac907150470a46b34acbd8", null ],
    [ "prevError", "classErrorChecker.html#a6229c7d4bb139d120b1a19c3707acba0", null ],
    [ "setup", "classErrorChecker.html#a729423694c1b020b696fdf21b7f236a2", null ],
    [ "skipToError", "classErrorChecker.html#a1b04cdca35b60cc58ecb9d25e959f1bb", null ],
    [ "test", "classErrorChecker.html#a67528e27784ae496ff9156a5b4a2275b", null ],
    [ "tested", "classErrorChecker.html#a0bfa3ff20a85ba138404b0d44ed3e2cb", null ]
];