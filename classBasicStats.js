var classBasicStats =
[
    [ "BasicStats", "classBasicStats.html#a58ad53ccb4fe8cdff36c6cc7b2ca9206", null ],
    [ "~BasicStats", "classBasicStats.html#aea167c819d68e813b27d576e1b3d1d18", null ],
    [ "clear", "classBasicStats.html#accd687303635df9801a03f605cc6d1ed", null ],
    [ "cv", "classBasicStats.html#ac821907e4cd30d99004d2b7815c6cc16", null ],
    [ "doUpdate", "classBasicStats.html#a4b0856033540eee742fe1bed06a1e8ca", null ],
    [ "max", "classBasicStats.html#aef1854e00c80845bfeac3710cc3b8bb3", null ],
    [ "mean", "classBasicStats.html#aebcfc66c429460eebb21a4548ade328f", null ],
    [ "min", "classBasicStats.html#a04f31e84ac2958c1267cbc53f2be48dd", null ],
    [ "nnz", "classBasicStats.html#a4566b2a18b30d4ed2863c702a43c7cc3", null ],
    [ "stddev", "classBasicStats.html#abb2d720be9ec5eae5fd706f3c653444c", null ],
    [ "sum", "classBasicStats.html#ae1d9e585565afa6fb455143b61ef637b", null ],
    [ "variance", "classBasicStats.html#a9f91abce37d84444d6f7d83fa088fbd8", null ]
];