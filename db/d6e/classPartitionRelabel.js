var classPartitionRelabel =
[
    [ "PartitionRelabel", "db/d6e/classPartitionRelabel.html#a866b0adb27df2e75c4f68a290e38e1d4", null ],
    [ "~PartitionRelabel", "db/d6e/classPartitionRelabel.html#af8ab5069608f449b8115fd3305644750", null ],
    [ "newRegionFor", "db/d6e/classPartitionRelabel.html#ac7c5360b7062c0f6370f590e77bd01e7", null ],
    [ "partition", "db/d6e/classPartitionRelabel.html#a95d468f8b8429ce1b4e8cbb2eb4a2e66", null ],
    [ "passthroughAll", "db/d6e/classPartitionRelabel.html#a1005281df0bf3df1f705c500842363db", null ],
    [ "relabel", "db/d6e/classPartitionRelabel.html#a91a21b605e8cd152545c1417ac6041b3", null ],
    [ "relabelAllTo", "db/d6e/classPartitionRelabel.html#a5d7e3caff9f0596a675c2822399279b0", null ],
    [ "result", "db/d6e/classPartitionRelabel.html#a58f6524f8ad2652ee1d3ba11390a28fa", null ],
    [ "update", "db/d6e/classPartitionRelabel.html#af8e8ac13e54070a132f3e43432c79f48", null ],
    [ "m_defaultRegion", "db/d6e/classPartitionRelabel.html#a33e31cba895efcff1aa2fd086f5b9154", null ],
    [ "m_newRegion", "db/d6e/classPartitionRelabel.html#a36f41133a4caac91bad6525505771180", null ],
    [ "m_partition", "db/d6e/classPartitionRelabel.html#a4c050ceb3a95b3adbbd715ede243caf1", null ]
];