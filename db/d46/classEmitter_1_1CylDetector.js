var classEmitter_1_1CylDetector =
[
    [ "onCylinder", "db/d46/classEmitter_1_1CylDetector.html#adee84760c500ca06a98ac1fa5c0330c8", [
      [ "SURFACE", "db/d46/classEmitter_1_1CylDetector.html#adee84760c500ca06a98ac1fa5c0330c8ae05ac26040a766503c277e5d987bcabd", null ],
      [ "INSIDE", "db/d46/classEmitter_1_1CylDetector.html#adee84760c500ca06a98ac1fa5c0330c8acdb5fe71fa4ab290bc0b9fa653fce7c8", null ],
      [ "OUTSIDE", "db/d46/classEmitter_1_1CylDetector.html#adee84760c500ca06a98ac1fa5c0330c8a20f3094795138a87e780729d28640e67", null ]
    ] ],
    [ "CylDetector", "db/d46/classEmitter_1_1CylDetector.html#aa4e2d0f3321c285a72e09dee8a8b6bdd", null ],
    [ "~CylDetector", "db/d46/classEmitter_1_1CylDetector.html#a0376e0875170209336862f4d02f590d7", null ],
    [ "closestPointOnLine", "db/d46/classEmitter_1_1CylDetector.html#a2fbf9daad6cef8bb1f402dd8072ec5a7", null ],
    [ "detect", "db/d46/classEmitter_1_1CylDetector.html#adf772a29ed60aea7723fbc0d43095550", null ],
    [ "detectedWeight", "db/d46/classEmitter_1_1CylDetector.html#aa1a73e51b1d7f550f11a57902e9f6a40", null ],
    [ "detectionType", "db/d46/classEmitter_1_1CylDetector.html#a513c8156790dda95c21f9fa00665eba7", null ],
    [ "detectorID", "db/d46/classEmitter_1_1CylDetector.html#ad079de4ff8bc6432966cf5f459bc9888", null ],
    [ "direction", "db/d46/classEmitter_1_1CylDetector.html#a62f2b96490e04d0b0e6b581f162ddabc", null ],
    [ "dirNormal", "db/d46/classEmitter_1_1CylDetector.html#aa92dc44dc200f90902a68b717164e3a7", null ],
    [ "endpoint", "db/d46/classEmitter_1_1CylDetector.html#aee8e6e5f691ff31b67fe392922956f20", null ],
    [ "getRandPerpendicularVector", "db/d46/classEmitter_1_1CylDetector.html#aa0d2442d9efdba2adc5ba8516d085c8e", null ],
    [ "height", "db/d46/classEmitter_1_1CylDetector.html#aa32a39ac30e4e02733fdc8910432fd93", null ],
    [ "isLineIntersects", "db/d46/classEmitter_1_1CylDetector.html#a7c647cfcde9b90da9666916d24eff625", null ],
    [ "isLineIntersectsCaps", "db/d46/classEmitter_1_1CylDetector.html#a155552e293e8330ceb6aaf2e87407130", null ],
    [ "isPointInside", "db/d46/classEmitter_1_1CylDetector.html#abca5c7eedd0755840d33d3ade30c1bd0", null ],
    [ "numericalAperture", "db/d46/classEmitter_1_1CylDetector.html#af4ddfbd72574a12e277aa6fcd3aa4b89", null ],
    [ "radius", "db/d46/classEmitter_1_1CylDetector.html#a4a092461067b8cd05af2e17c2cc80c83", null ],
    [ "resetDetectedWeight", "db/d46/classEmitter_1_1CylDetector.html#a313694be17816ebfb5b3eead9a48dfcd", null ],
    [ "setDetectionType", "db/d46/classEmitter_1_1CylDetector.html#a659020e0a969503ccba7278c09923ff1", null ],
    [ "tagEnclosingTetra", "db/d46/classEmitter_1_1CylDetector.html#a492d389fb142f331c0008fcbccfbb28f", null ],
    [ "tagEnclosingTetraRtree", "db/d46/classEmitter_1_1CylDetector.html#a7b4ce2dc9ee1a0404f9891cff490f3f0", null ],
    [ "expProbFast", "db/d46/classEmitter_1_1CylDetector.html#a75e7bd641579423fcafcd478d78dc7ab", null ],
    [ "m_detectedWeight", "db/d46/classEmitter_1_1CylDetector.html#a0134d016b602b355f690bb0b06f27551", null ],
    [ "m_detectionType", "db/d46/classEmitter_1_1CylDetector.html#a34a034403d671e16f97f8c4e70272f18", null ],
    [ "m_detectorID", "db/d46/classEmitter_1_1CylDetector.html#a080a4c24c1d96402f7f1bf5395e7851c", null ],
    [ "m_dir", "db/d46/classEmitter_1_1CylDetector.html#acf622cc5043a5c82d99704d7af0d2085", null ],
    [ "m_end0_q", "db/d46/classEmitter_1_1CylDetector.html#ace54cbe3f79317d958093288174ac666", null ],
    [ "m_end1_q", "db/d46/classEmitter_1_1CylDetector.html#a5afe9fbfb6444e13529e74705c2294c3", null ],
    [ "m_endpoint", "db/d46/classEmitter_1_1CylDetector.html#a79f5caf08c35d72af1a8b97cdd73567d", null ],
    [ "m_height", "db/d46/classEmitter_1_1CylDetector.html#a2edee65d2002f03124ad5b2aa6e3f9d9", null ],
    [ "m_mesh", "db/d46/classEmitter_1_1CylDetector.html#aafaa27c4c9740cde2d4c391651dfc91b", null ],
    [ "m_midpoint", "db/d46/classEmitter_1_1CylDetector.html#ad5ae55cbf64e197fbd16d49876e93687", null ],
    [ "m_numericalAperture", "db/d46/classEmitter_1_1CylDetector.html#a7ae236d84a522432f75653741e9e7431", null ],
    [ "m_Q", "db/d46/classEmitter_1_1CylDetector.html#a5abcede2eac94dcb9a4b46c9eb8bfaed", null ],
    [ "m_radius", "db/d46/classEmitter_1_1CylDetector.html#a979284bf8513a08913d303c28f1ec129", null ],
    [ "m_rtree", "db/d46/classEmitter_1_1CylDetector.html#a2e3bf19836f8c732721239186fc76e68", null ],
    [ "m_sse_dir", "db/d46/classEmitter_1_1CylDetector.html#aa76f836971edb160f8aa9aa825e3df6a", null ],
    [ "m_sse_end0", "db/d46/classEmitter_1_1CylDetector.html#ab6663f3f3ad1ad8ef49007e24f6b6a6a", null ],
    [ "m_sse_end1", "db/d46/classEmitter_1_1CylDetector.html#a6389ad0ba34907c0bdb320346a241f52", null ],
    [ "m_tetras", "db/d46/classEmitter_1_1CylDetector.html#ab1019f2c69db88604916506060dd6bca", null ],
    [ "odeProb", "db/d46/classEmitter_1_1CylDetector.html#adb4bca377d6dadc2a6c960fb9a3a9981", null ]
];