var opencl_8cpp =
[
    [ "_checkError", "db/dca/opencl_8cpp.html#a2ce13cd7272eaf6125b49278edd228f4", null ],
    [ "alignedFree", "db/dca/opencl_8cpp.html#ac6192b30f98727a43139f9f1b8b211fb", null ],
    [ "alignedMalloc", "db/dca/opencl_8cpp.html#adad76a5b63ec2259c4d8751886bc7753", null ],
    [ "createProgramFromBinary", "db/dca/opencl_8cpp.html#abdc8b1048e537da84d2e6892bced185e", null ],
    [ "fileExists", "db/dca/opencl_8cpp.html#aa4e1b53becdb354c70e1d083fcfd3f90", null ],
    [ "findPlatform", "db/dca/opencl_8cpp.html#a21f2d6fe5e3c2b3644779413e328b125", null ],
    [ "getBoardBinaryFile", "db/dca/opencl_8cpp.html#a3b769719b83c93344deeb5fd55bd6674", null ],
    [ "getCurrentTimestamp", "db/dca/opencl_8cpp.html#a6a503356aff347ca684c9e96b54eff47", null ],
    [ "getDeviceName", "db/dca/opencl_8cpp.html#afec35b4109bad93114c2e4c0099082c3", null ],
    [ "getDevices", "db/dca/opencl_8cpp.html#a903382a4bacbc5d633875e6d29bdb6f4", null ],
    [ "getPlatformName", "db/dca/opencl_8cpp.html#a9f1bfa559f86c9ab7025c148bfb44890", null ],
    [ "getStartEndTime", "db/dca/opencl_8cpp.html#a14c078e95e04a6daae9ec168d0fea8ee", null ],
    [ "getStartEndTime", "db/dca/opencl_8cpp.html#af18eb694855657be00d3f80931797b7a", null ],
    [ "loadBinaryFile", "db/dca/opencl_8cpp.html#ad600cef36637401a7e18a85fa3ef4276", null ],
    [ "oclContextCallback", "db/dca/opencl_8cpp.html#a7e4486a95239e8a4d869d09da49407f5", null ],
    [ "printError", "db/dca/opencl_8cpp.html#a8a81775e8c6f7197aeec0697ad7d20f3", null ],
    [ "setCwdToExeDir", "db/dca/opencl_8cpp.html#af21781dcb319fba099f4a2d92d5d79ec", null ],
    [ "waitMilliseconds", "db/dca/opencl_8cpp.html#abe55a802d3195f4c0a0cc2cb72955758", null ],
    [ "AOCL_ALIGNMENT", "db/dca/opencl_8cpp.html#a9ab0306ffeeb7b950ed90d3d5739cb1e", null ],
    [ "VERSION_STR", "db/dca/opencl_8cpp.html#a6e33172d130220bdd7eb078d332da218", null ]
];