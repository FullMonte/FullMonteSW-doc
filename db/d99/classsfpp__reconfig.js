var classsfpp__reconfig =
[
    [ "sfpp_reconfig.rtl", "d5/df6/classsfpp__reconfig_1_1rtl.html", "d5/df6/classsfpp__reconfig_1_1rtl" ],
    [ "alt_xcvr_reconfig_0_ch0_1_from_xcvr_reconfig_from_xcvr", "db/d99/classsfpp__reconfig.html#afa3cb63675625b48051e555cd1a3bff7", null ],
    [ "alt_xcvr_reconfig_0_ch0_1_to_xcvr_reconfig_to_xcvr", "db/d99/classsfpp__reconfig.html#a75e8a7e0d249af291ad68d9dc6456ff8", null ],
    [ "alt_xcvr_reconfig_0_ch2_3_from_xcvr_reconfig_from_xcvr", "db/d99/classsfpp__reconfig.html#a02adcd9d8592d650584f66eb3b337bf8", null ],
    [ "alt_xcvr_reconfig_0_ch2_3_to_xcvr_reconfig_to_xcvr", "db/d99/classsfpp__reconfig.html#a58ef2284feb75e9ee6c97ce82c92b8f9", null ],
    [ "alt_xcvr_reconfig_0_reconfig_busy_reconfig_busy", "db/d99/classsfpp__reconfig.html#acf1fc0e398c0fa6c8e544a561aa92184", null ],
    [ "clk_clk", "db/d99/classsfpp__reconfig.html#a15ad8a3b1978a95d40933dfca7492326", null ],
    [ "IEEE", "db/d99/classsfpp__reconfig.html#a07a91e8825f59c1935af1f9b64f37c9f", null ],
    [ "numeric_std", "db/d99/classsfpp__reconfig.html#a426e01ecf33afc60f8c5ee1595338957", null ],
    [ "reset_reset_n", "db/d99/classsfpp__reconfig.html#a3f90d0f6c4e935f7561f26a516ffdf75", null ],
    [ "std_logic_1164", "db/d99/classsfpp__reconfig.html#a9a9effbfdcdbab2e4981523ed80181db", null ]
];