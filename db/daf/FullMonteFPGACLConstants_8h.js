var FullMonteFPGACLConstants_8h =
[
    [ "DOUBLE2FIXED", "db/daf/FullMonteFPGACLConstants_8h.html#a464c019f15a13af1349c63457e72460c", null ],
    [ "FIXED2DOUBLE", "db/daf/FullMonteFPGACLConstants_8h.html#af1f64744e808fdb731db04c4decace9a", null ],
    [ "FIXED_POINT_FRACTIONAL_BITS", "db/daf/FullMonteFPGACLConstants_8h.html#a34e9affc315925e72a6cb9c731d2b2ed", null ],
    [ "FIXED_POINT_INTEGRAL_BITS", "db/daf/FullMonteFPGACLConstants_8h.html#a2ab1801e19d9016947ba13f626f5f87c", null ],
    [ "MAX_FPGACL_MATERIALS", "db/daf/FullMonteFPGACLConstants_8h.html#a143259faacb4402b8d8902da62ca1942", null ],
    [ "MAX_FPGACL_ONCHIP_TETRAS", "db/daf/FullMonteFPGACLConstants_8h.html#a8cdb4faef3eede7da3f751a6e781bc63", null ],
    [ "MAX_FPGACL_PIPELINES", "db/daf/FullMonteFPGACLConstants_8h.html#a3c0207658697071179e9bad3ba82e5ae", null ],
    [ "SPEED_OF_LIGHT_MM_PER_NS", "db/daf/FullMonteFPGACLConstants_8h.html#a0617a7cc7b8424e8f4a8c014a818446c", null ]
];