var structMCEventCounts =
[
    [ "clear", "db/d33/structMCEventCounts.html#a85de19698ff91812047d3e4cc5c4f2a6", null ],
    [ "operator+=", "db/d33/structMCEventCounts.html#ab7f89c991f4be6ce10969ef5d74920b7", null ],
    [ "Nabnormal", "db/d33/structMCEventCounts.html#a7e6542974c31b94a2fc9dc5c721e48d5", null ],
    [ "Nabsorb", "db/d33/structMCEventCounts.html#ab8da1e3d29d7662f3d4b91b5a59e4eb6", null ],
    [ "Nbound", "db/d33/structMCEventCounts.html#aa98e916a285e2e5b069b611faa6cb451", null ],
    [ "Ndie", "db/d33/structMCEventCounts.html#adc8dc0a0ad7b52430987915b110309cc", null ],
    [ "Nexit", "db/d33/structMCEventCounts.html#a798c4e7b4957b6c011d96ddc9ec1b5cf", null ],
    [ "Nfresnel", "db/d33/structMCEventCounts.html#a76c27587b91cdd5cdc567b1abb78031b", null ],
    [ "Ninterface", "db/d33/structMCEventCounts.html#a824f835563bce349a51cffb338191850", null ],
    [ "Nlaunch", "db/d33/structMCEventCounts.html#ab8d684e0220c11a5a6830e9f2f7562a1", null ],
    [ "Nnohit", "db/d33/structMCEventCounts.html#aebe3fbc391d933bd4d654a14f912daff", null ],
    [ "Nrefr", "db/d33/structMCEventCounts.html#a25772399df8c4613f16e2a895a7f3dda", null ],
    [ "Nscatter", "db/d33/structMCEventCounts.html#ad1f045acae40447eb015a2534932f6ee", null ],
    [ "NspecialAbsorb", "db/d33/structMCEventCounts.html#abc12bf69ee3bb059af01845331b68863", null ],
    [ "NspecialReflect", "db/d33/structMCEventCounts.html#abdac738044e10b8fcc1454f4e22cdc04", null ],
    [ "NspecialTerm", "db/d33/structMCEventCounts.html#a655767bbfcfa75ecfd9b45423fa1d523", null ],
    [ "NspecialTransmit", "db/d33/structMCEventCounts.html#acdea03b300a97bda59472b365d391967", null ],
    [ "Ntime", "db/d33/structMCEventCounts.html#a31e6b7062867843f4b4374b090d44ffd", null ],
    [ "Ntir", "db/d33/structMCEventCounts.html#a6283cf14da3507c807fea55d2817376d", null ],
    [ "Nwin", "db/d33/structMCEventCounts.html#ae4459680233ccd2b0be7fad4fd090e23", null ]
];