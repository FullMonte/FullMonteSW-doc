var classaocl__utils_1_1scoped__aligned__ptr =
[
    [ "this_type", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#a5fa79df3ddff8e4f5da1aa81715c9e0a", null ],
    [ "scoped_aligned_ptr", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#a7bda7835a83a7da833596de1d134a8af", null ],
    [ "scoped_aligned_ptr", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#ab9bbc7b710ff2b9576670fd338d350f8", null ],
    [ "scoped_aligned_ptr", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#a2e1e88ab6bd7c94ce6f218da9858274b", null ],
    [ "~scoped_aligned_ptr", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#a51079002e10b173a857e95f507bb8e5c", null ],
    [ "scoped_aligned_ptr", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#a75ed58ef47661f43d39f838564215216", null ],
    [ "get", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#a8ea7ea6d75021add3bc2d465a6659025", null ],
    [ "operator T*", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#ac8307c3bc2f41d1895facfac412fa77e", null ],
    [ "operator*", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#a31e63da2b403e60510ae6f7a5f045184", null ],
    [ "operator->", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#a0b1cd3f77d275f91e3f93ca0b0171366", null ],
    [ "operator=", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#ab9d32e0abeac79ff805a830298961351", null ],
    [ "operator=", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#a6c5caca8a0758a11a1655637ef137a9c", null ],
    [ "operator[]", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#a2e689fb4a0046c19f891f3058f4bc094", null ],
    [ "release", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#aa8d95fb2165ac1358b4083ed0ccbd212", null ],
    [ "reset", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#a30de9c7ea5c08a772b2f577a76bdc5b6", null ],
    [ "reset", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#ab59d02b50066c0f0191fb267cda468cd", null ],
    [ "m_ptr", "db/dfd/classaocl__utils_1_1scoped__aligned__ptr.html#ac0c6a0700cb5cbba371d44b9d837b753", null ]
];