var classMMCJSONCase =
[
    [ "SourceType", "db/d11/classMMCJSONCase.html#ac9d72d4adf1b993672bb4baa89f509f4", [
      [ "Unknown", "db/d11/classMMCJSONCase.html#ac9d72d4adf1b993672bb4baa89f509f4a148b9f06ba7393af3600835d85e315fb", null ],
      [ "Point", "db/d11/classMMCJSONCase.html#ac9d72d4adf1b993672bb4baa89f509f4a360822ddda8bf4247a3547bb23b48703", null ],
      [ "Pencil", "db/d11/classMMCJSONCase.html#ac9d72d4adf1b993672bb4baa89f509f4a284dbb6ed1ff0492310859ffa3ed74e4", null ]
    ] ],
    [ "MMCJSONCase", "db/d11/classMMCJSONCase.html#a65e85b00e8afcfba19c7c51567b93372", null ],
    [ "~MMCJSONCase", "db/d11/classMMCJSONCase.html#aa33da7e3a27be90b3616f40553198d22", null ],
    [ "clone", "db/d11/classMMCJSONCase.html#aeb02d2e01158a9ee9b89cacbd8a135b9", null ],
    [ "createSource", "db/d11/classMMCJSONCase.html#aa6d2cbb85dd9d796097039b5d43a0cdf", null ],
    [ "initElem", "db/d11/classMMCJSONCase.html#a476b7e1a3cf24ae52c035d9fa7bc5f55", null ],
    [ "initElem", "db/d11/classMMCJSONCase.html#a65a36923a3a07df88f35a68a16748bf1", null ],
    [ "meshId", "db/d11/classMMCJSONCase.html#a765365ce6cbdc42348bce66a1628ca82", null ],
    [ "meshId", "db/d11/classMMCJSONCase.html#ac6daf2acc27b0192af52102d626c4939", null ],
    [ "photons", "db/d11/classMMCJSONCase.html#a1b06e8a02d8d241bc8058a14cc403e46", null ],
    [ "photons", "db/d11/classMMCJSONCase.html#af1be949892f8ad7364cc3285513cd17d", null ],
    [ "print", "db/d11/classMMCJSONCase.html#a9f39383435b696685c103f371ae37ee2", null ],
    [ "sessionId", "db/d11/classMMCJSONCase.html#a0b4cc5d1a8e1a17050e822076c45e54a", null ],
    [ "sessionId", "db/d11/classMMCJSONCase.html#a26324e18d74b6623978bb8cf93ba07aa", null ],
    [ "setPencilSource", "db/d11/classMMCJSONCase.html#a0bb7de6000f5abf7b6731bad05104973", null ],
    [ "setPointSource", "db/d11/classMMCJSONCase.html#a86ab02417ba06adc7142d7564cdb5e54", null ],
    [ "m_initElem", "db/d11/classMMCJSONCase.html#a1d1d041c520eb02b236c36a0fa1061d1", null ],
    [ "m_meshId", "db/d11/classMMCJSONCase.html#aef78763ed60184564a9569658f68fd0f", null ],
    [ "m_photons", "db/d11/classMMCJSONCase.html#af7ab381c147a720bc415206f13044eef", null ],
    [ "m_sessionId", "db/d11/classMMCJSONCase.html#ae1a47582bb90ac5b5762e6fe3f6cd1de", null ],
    [ "m_srcDir", "db/d11/classMMCJSONCase.html#aab4377d07c30a3432374aef17a680d7c", null ],
    [ "m_srcPos", "db/d11/classMMCJSONCase.html#a6af96e89a449fd8e1d8621e9b22dd653", null ],
    [ "m_srcType", "db/d11/classMMCJSONCase.html#a8e7ccf704a59ba40739c8f93f93e05f3", null ]
];