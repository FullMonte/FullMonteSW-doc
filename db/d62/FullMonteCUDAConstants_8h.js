var FullMonteCUDAConstants_8h =
[
    [ "MAX_CUDA_MATERIALS", "db/d62/FullMonteCUDAConstants_8h.html#acd795ebc26712c53a14dfc924b275478", null ],
    [ "MIN_COMPUTE_CAPABILITY_MAJOR", "db/d62/FullMonteCUDAConstants_8h.html#a80bd7b78f46dc8196a04a0f77d7e5dce", null ],
    [ "MIN_COMPUTE_CAPABILITY_MINOR", "db/d62/FullMonteCUDAConstants_8h.html#a65092e8a4da5ed239a1ce0b777167ab2", null ],
    [ "SPEED_OF_LIGHT_MM_PER_NS", "db/d62/FullMonteCUDAConstants_8h.html#a0617a7cc7b8424e8f4a8c014a818446c", null ]
];