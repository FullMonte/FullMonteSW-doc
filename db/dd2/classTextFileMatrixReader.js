var classTextFileMatrixReader =
[
    [ "TextFileMatrixReader", "db/dd2/classTextFileMatrixReader.html#a790943e512b76408b7c4478aa0b4de8a", null ],
    [ "~TextFileMatrixReader", "db/dd2/classTextFileMatrixReader.html#a1b16b4d2fb6738319821e7e742fd010f", null ],
    [ "clear", "db/dd2/classTextFileMatrixReader.html#a62709b5328d0f2a0caa636f4666b4653", null ],
    [ "filename", "db/dd2/classTextFileMatrixReader.html#add005ac4ee8a916717491d2af42004f3", null ],
    [ "output", "db/dd2/classTextFileMatrixReader.html#a457fe6ee10b39109a455ba96b158fff2", null ],
    [ "read", "db/dd2/classTextFileMatrixReader.html#ac12798fc50d95bd1478a1ec61f126945", null ],
    [ "size", "db/dd2/classTextFileMatrixReader.html#a8d6d47f8f47d6be1759bcaf2112936ca", null ],
    [ "m_D", "db/dd2/classTextFileMatrixReader.html#a4e5cc8a32d23ebd28ebdb2d3bb280e1c", null ],
    [ "m_data", "db/dd2/classTextFileMatrixReader.html#a86b28c4dfd30013c892fe991c8ee0de7", null ],
    [ "m_dataType", "db/dd2/classTextFileMatrixReader.html#a38fd6635657ef564ef7ffa96d04b113d", null ],
    [ "m_dims", "db/dd2/classTextFileMatrixReader.html#a0e23c32320e1fda64616d5ee72fb65f8", null ],
    [ "m_filename", "db/dd2/classTextFileMatrixReader.html#acb7835e67d84254322494b088949e397", null ],
    [ "m_unitType", "db/dd2/classTextFileMatrixReader.html#a2afa941d6367412089a18a2c23ff35e1", null ]
];