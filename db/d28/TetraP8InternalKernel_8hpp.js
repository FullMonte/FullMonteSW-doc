var TetraP8InternalKernel_8hpp =
[
    [ "TetraP8InternalKernel", "d8/d67/classTetraP8InternalKernel.html", "d8/d67/classTetraP8InternalKernel" ],
    [ "MemcopyWED", "d8/de8/structTetraP8InternalKernel_1_1MemcopyWED.html", "d8/de8/structTetraP8InternalKernel_1_1MemcopyWED" ],
    [ "DEVICE_STRING", "db/d28/TetraP8InternalKernel_8hpp.html#a0cc1f594a089044000af260be46f2309", null ],
    [ "STATUS_DONE", "db/d28/TetraP8InternalKernel_8hpp.html#a3c7966dcc2aec8e20459e036f5823fc2", null ],
    [ "STATUS_READY", "db/d28/TetraP8InternalKernel_8hpp.html#a64f314e17a1be8e6c8e09bb90b9ecb68", null ],
    [ "STATUS_RUNNING", "db/d28/TetraP8InternalKernel_8hpp.html#a799b04d03f49612e3e636b56c3972b80", null ],
    [ "STATUS_WAITING", "db/d28/TetraP8InternalKernel_8hpp.html#a4eb8ee21eeb7699dbe5fcee4c30d7b72", null ]
];