var classBoundaryTraits =
[
    [ "Input", "d1/ddb/structBoundaryTraits_1_1Input.html", "d1/ddb/structBoundaryTraits_1_1Input" ],
    [ "Output", "d4/d77/structBoundaryTraits_1_1Output.html", "d4/d77/structBoundaryTraits_1_1Output" ],
    [ "input_container_type", "db/d00/classBoundaryTraits.html#a15e47e6fc93c2a2cc37ce7f6278f3d6d", null ],
    [ "input_type", "db/d00/classBoundaryTraits.html#a32bb2ac46c14164f6c15f76619c75755", null ],
    [ "output_container_type", "db/d00/classBoundaryTraits.html#a096e5cfca3d58e08195fe06f088526da", null ],
    [ "output_type", "db/d00/classBoundaryTraits.html#acb5af0106f70fd76380021c7fd2cbe31", null ],
    [ "packed_input_type", "db/d00/classBoundaryTraits.html#a3108bb9fb93bf3c3765a5ac6d105b14d", null ],
    [ "packed_output_type", "db/d00/classBoundaryTraits.html#a89f2cb200dbba4f3d6d3b9528d0c89db", null ],
    [ "convertFromNativeType", "db/d00/classBoundaryTraits.html#a720fd38d17f5d4ad29f45f271082b77d", null ],
    [ "convertToNativeType", "db/d00/classBoundaryTraits.html#a1b30285bf3009cf732c0671d751e97f3", null ],
    [ "convertToNativeType", "db/d00/classBoundaryTraits.html#a92893269a360fac190393049d4f8ead9", null ],
    [ "input_bits", "db/d00/classBoundaryTraits.html#ad845ebd16e53b0ce94f702d9366c8d63", null ],
    [ "output_bits", "db/d00/classBoundaryTraits.html#afa9fe976827f1534c09ce501b15053da", null ]
];