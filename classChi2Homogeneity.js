var classChi2Homogeneity =
[
    [ "Chi2Homogeneity", "classChi2Homogeneity.html#aef7015342d7f272eadb021ef8a1d6c9d", null ],
    [ "~Chi2Homogeneity", "classChi2Homogeneity.html#ac8e6a54eb54a0fc0396f66be06aff7dd", null ],
    [ "allowedCoefficientOfVariationOfMean", "classChi2Homogeneity.html#a8d6dee427631fb029b73e975678dc18b", null ],
    [ "chi2stat", "classChi2Homogeneity.html#a63bf89d75d69ec501a6c736f42f3d6d9", null ],
    [ "clear", "classChi2Homogeneity.html#abd05b506f230c6637153572fcafebbc8", null ],
    [ "contributions", "classChi2Homogeneity.html#a58ba378d58d2630fc7a4ecdd4254d208", null ],
    [ "criticalValue", "classChi2Homogeneity.html#a8698ab697a6cf8dce4380444e608f62d", null ],
    [ "degreesOfFreedom", "classChi2Homogeneity.html#a898f5ddafa34f78b4318866cd5fe7179", null ],
    [ "doUpdate", "classChi2Homogeneity.html#a206c2e8d3a9275fb2191e8cbf0fbf688", null ],
    [ "leftExcluded", "classChi2Homogeneity.html#ab3e0cc5f18daeca8d22ad23e79b90b18", null ],
    [ "leftPackets", "classChi2Homogeneity.html#a8738368485bd8e919f6a1781d87a1b56", null ],
    [ "leftPackets", "classChi2Homogeneity.html#ac0128b166fa34bbc9b7675db3e504b9c", null ],
    [ "normalizeLeft", "classChi2Homogeneity.html#a24e095c3c04bee44d18002454a82c724", null ],
    [ "normalizeRight", "classChi2Homogeneity.html#a05b7251f6ece9d7f1be48cb1689cf712", null ],
    [ "pValue", "classChi2Homogeneity.html#a0539fe9b60ee0e0f826dcf3877b0e9ce", null ],
    [ "rightPackets", "classChi2Homogeneity.html#a27ced431a641ae1d2ddbd0d78b3a45f7", null ],
    [ "rightPackets", "classChi2Homogeneity.html#ac686867d1cd71b8bd38a1e242f6d7287", null ],
    [ "rightRuns", "classChi2Homogeneity.html#a2772df24271ca3b1656a22b65951233a", null ],
    [ "rightRuns", "classChi2Homogeneity.html#a4ba988bbd4a1f0dbbf68e3498fb6e121", null ],
    [ "rightVariance", "classChi2Homogeneity.html#adda9817f30c2342f654a9c145ed0e440", null ]
];