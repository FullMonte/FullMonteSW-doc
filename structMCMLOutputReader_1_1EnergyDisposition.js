var structMCMLOutputReader_1_1EnergyDisposition =
[
    [ "absorption", "structMCMLOutputReader_1_1EnergyDisposition.html#a48d79cd36ab10acd5be5c98db67c1499", null ],
    [ "diffuseReflectance", "structMCMLOutputReader_1_1EnergyDisposition.html#a1cfed2dc94e578b901633dc8abed942c", null ],
    [ "specularReflectance", "structMCMLOutputReader_1_1EnergyDisposition.html#a3cc311186977a5cbfac518e0924c834f", null ],
    [ "transmission", "structMCMLOutputReader_1_1EnergyDisposition.html#ab42561e70a52be370d3f7d0a2862e427", null ]
];