var classHenyeyGreenstein8f =
[
    [ "input_type", "de/dde/classHenyeyGreenstein8f.html#a4c75d76c7b2ca7d7ea26e30492ccfc27", null ],
    [ "result_type", "de/dde/classHenyeyGreenstein8f.html#a3901b564650520e7a558d821eb9433a2", null ],
    [ "calculate", "de/dde/classHenyeyGreenstein8f.html#a0f2a32ff860fec5048daef6fa2afcebf", null ],
    [ "g", "de/dde/classHenyeyGreenstein8f.html#a33bf8e317ce9e23d45b6a90ab8e4fa53", null ],
    [ "gParam", "de/dde/classHenyeyGreenstein8f.html#ad208f24e79cb83051ca125ea814f1f55", null ],
    [ "gParam", "de/dde/classHenyeyGreenstein8f.html#af436aaefb5f4ad68060c6b60ca2b4325", null ],
    [ "one_minus_gg", "de/dde/classHenyeyGreenstein8f.html#ab2fd614b2670e688da1941f0e8228b70", null ],
    [ "one_plus_gg", "de/dde/classHenyeyGreenstein8f.html#aafec278e2897a7b934faaa3119589b39", null ],
    [ "recip_2g", "de/dde/classHenyeyGreenstein8f.html#a1cf89760f1ff0c1d0d5cc9780dd6bf66", null ],
    [ "InputBlockSize", "de/dde/classHenyeyGreenstein8f.html#a8cc07965204f7b3166ed27b6735ce448", null ],
    [ "m_isotropic", "de/dde/classHenyeyGreenstein8f.html#ad9d2db58908deacf7bdb1f71599fe34d", null ],
    [ "m_params", "de/dde/classHenyeyGreenstein8f.html#a676ff0b89f25da1f1ab4492ec2bc2f6e", null ],
    [ "OutputElementSize", "de/dde/classHenyeyGreenstein8f.html#a4e838685aba7ab6e081cade3e095b200", null ],
    [ "OutputsPerInputBlock", "de/dde/classHenyeyGreenstein8f.html#af95789209ff9813296d010f463aa4dea", null ]
];