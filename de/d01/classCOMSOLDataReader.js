var classCOMSOLDataReader =
[
    [ "COMSOLDataReader", "de/d01/classCOMSOLDataReader.html#a92d11e84550afd3207018f75716113ab", null ],
    [ "~COMSOLDataReader", "de/d01/classCOMSOLDataReader.html#ae61924d84e313e3657f533f9c108652e", null ],
    [ "data", "de/d01/classCOMSOLDataReader.html#a35818607042863e0a42af13843070691", null ],
    [ "filename", "de/d01/classCOMSOLDataReader.html#aab8a70dc38d11e7081c22d68a69a6083", null ],
    [ "points", "de/d01/classCOMSOLDataReader.html#a59869998d5243aa03fd9bb6ae43b9cb2", null ],
    [ "read", "de/d01/classCOMSOLDataReader.html#a38f88780e66be8e2825a839cde909698", null ],
    [ "setZeroOffset", "de/d01/classCOMSOLDataReader.html#a00cd7f828481620eb20c32ff12e537a5", null ],
    [ "m_data", "de/d01/classCOMSOLDataReader.html#aa77ee793bb181150a8741c610e7b7935", null ],
    [ "m_filename", "de/d01/classCOMSOLDataReader.html#a5998f2eefc1e6eaf1ef32d9d8f940f24", null ],
    [ "m_points", "de/d01/classCOMSOLDataReader.html#a0a030945de166a156fc7a5158c31367d", null ],
    [ "m_zeroOffset", "de/d01/classCOMSOLDataReader.html#ad97704b8f30ee58ba76805144637743b", null ]
];