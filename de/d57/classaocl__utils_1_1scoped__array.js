var classaocl__utils_1_1scoped__array =
[
    [ "this_type", "de/d57/classaocl__utils_1_1scoped__array.html#aa8f5e5e8b1889baa44fff1d7fcfda948", null ],
    [ "scoped_array", "de/d57/classaocl__utils_1_1scoped__array.html#a07ee2440670860fb5a452d32ffa8e5f1", null ],
    [ "scoped_array", "de/d57/classaocl__utils_1_1scoped__array.html#a6817c2357334a0266adb6aaf44d6508c", null ],
    [ "scoped_array", "de/d57/classaocl__utils_1_1scoped__array.html#a224e1d1316d5ae034c9811ff7f34c9d3", null ],
    [ "~scoped_array", "de/d57/classaocl__utils_1_1scoped__array.html#a64497aea0ef9fd6711fe23118c2f98e4", null ],
    [ "scoped_array", "de/d57/classaocl__utils_1_1scoped__array.html#ac1d72ba20686989c8e1bfffcc9214826", null ],
    [ "get", "de/d57/classaocl__utils_1_1scoped__array.html#a4c606c481459c620f58bf0cceea2d152", null ],
    [ "operator T*", "de/d57/classaocl__utils_1_1scoped__array.html#ae2b46043a50b7bfeba45f0fbad1af3b2", null ],
    [ "operator*", "de/d57/classaocl__utils_1_1scoped__array.html#a74aa0079c57c31075fe63474c19879b5", null ],
    [ "operator->", "de/d57/classaocl__utils_1_1scoped__array.html#aa0a6ec11cdd1774795315e9a97be64a4", null ],
    [ "operator=", "de/d57/classaocl__utils_1_1scoped__array.html#acae45b245321cb96b5420a6a7bf40c16", null ],
    [ "operator=", "de/d57/classaocl__utils_1_1scoped__array.html#ae840695d9cefda4c5f6aad0584dea410", null ],
    [ "operator[]", "de/d57/classaocl__utils_1_1scoped__array.html#a7fc79a75730cc6cc37110244525f07ef", null ],
    [ "release", "de/d57/classaocl__utils_1_1scoped__array.html#a710e24d4ad3bb12239740d29e3714e23", null ],
    [ "reset", "de/d57/classaocl__utils_1_1scoped__array.html#ae848d4b543b000aa18290add9e664a85", null ],
    [ "reset", "de/d57/classaocl__utils_1_1scoped__array.html#a6dcbd930d8f6e6f571b52db3eb5bd6f5", null ],
    [ "m_ptr", "de/d57/classaocl__utils_1_1scoped__array.html#a4fcbde16f5f75fa8263e3337e04c071e", null ]
];