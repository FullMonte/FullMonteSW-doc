var capi__memcpy_8c =
[
    [ "wed", "df/d41/structwed.html", "df/d41/structwed" ],
    [ "AFU_MMIO_REG_SIZE", "de/d0e/capi__memcpy_8c.html#acb6f22f30138b8e404cf2234fe918101", null ],
    [ "CACHELINE_BYTES", "de/d0e/capi__memcpy_8c.html#a4538b5ec4a295a2b8a52560e61575041", null ],
    [ "DEVICE", "de/d0e/capi__memcpy_8c.html#a775d096fbc3988fb7ed858b79ef44e22", null ],
    [ "MMIO_DMY1_ADDR", "de/d0e/capi__memcpy_8c.html#a8265f160c5f7ce2dbd11e49cd49ae111", null ],
    [ "MMIO_GO_ADDR", "de/d0e/capi__memcpy_8c.html#a33b3441bdadce020c098658c3462e792", null ],
    [ "MMIO_STOP_ADDR", "de/d0e/capi__memcpy_8c.html#a219dd6141553b0a125b0bd68e4a14b62", null ],
    [ "MMIO_TRACE_ADDR", "de/d0e/capi__memcpy_8c.html#ae4cd85d8be34814bf123f43790ce2c6b", null ],
    [ "alloc_test", "de/d0e/capi__memcpy_8c.html#aae1f9eecf6b359ed9fc0a9f8d123e315", null ],
    [ "check_errors", "de/d0e/capi__memcpy_8c.html#a0345f2c2b3296aee2fcaef1f84e8a20b", null ],
    [ "dump_trace", "de/d0e/capi__memcpy_8c.html#aeea1bc1e5d5607319fdba70c0e50b904", null ],
    [ "main", "de/d0e/capi__memcpy_8c.html#a0ddf1224851353fc92bfbff6f499fa97", null ],
    [ "print_help", "de/d0e/capi__memcpy_8c.html#a4b1eda355cc297254b5045080fd162ab", null ],
    [ "aligned", "de/d0e/capi__memcpy_8c.html#a17ce951d51df9991239c8e474e121806", null ],
    [ "buffer_cl", "de/d0e/capi__memcpy_8c.html#a232a52e3b3062e62010d33f79695543a", null ],
    [ "timeout", "de/d0e/capi__memcpy_8c.html#a25af7eefd48048fb067a60b8e295caf1", null ],
    [ "verbose", "de/d0e/capi__memcpy_8c.html#a0b2caeb4b6f130be43e5a2f0267dd453", null ]
];