var classScatterChecker =
[
    [ "input_type", "de/dc6/classScatterChecker.html#a571cbb841b6438db39e57313043245e8", null ],
    [ "output_type", "de/dc6/classScatterChecker.html#a14979bc02013028f8d6ffe7a86fecde4", null ],
    [ "ScatterChecker", "de/dc6/classScatterChecker.html#a2f7cff03a3e9aca3f5f43dfee40f8a75", null ],
    [ "check", "de/dc6/classScatterChecker.html#af72450920aa35a80cd6026a5dcecdb3a", null ],
    [ "clear", "de/dc6/classScatterChecker.html#a98b8602147c6d95f18866c693cac7bc5", null ],
    [ "m_cosineEps", "de/dc6/classScatterChecker.html#ade0b5d27d23f0c8bb20eb53eef5b2dca", null ],
    [ "m_directionCheck", "de/dc6/classScatterChecker.html#a4a06502bcbcd962a2b47f9efe3e37d1e", null ],
    [ "m_printToStdout", "de/dc6/classScatterChecker.html#ade74af2ce3ff3c6200ad8be092d1baaa", null ],
    [ "m_unitVector2Check", "de/dc6/classScatterChecker.html#a6c73aa1e9bc528870581c4ba4e385b01", null ]
];