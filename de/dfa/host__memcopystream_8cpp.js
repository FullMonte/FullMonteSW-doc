var host__memcopystream_8cpp =
[
    [ "MemcopyWED", "d7/d69/structMemcopyWED.html", "d7/d69/structMemcopyWED" ],
    [ "DEVICE_STRING", "de/dfa/host__memcopystream_8cpp.html#a0cc1f594a089044000af260be46f2309", null ],
    [ "HARDWARE", "de/dfa/host__memcopystream_8cpp.html#ae63cc10269ee1bb0c967a74ac5a0d0d5", null ],
    [ "STATUS_DONE", "de/dfa/host__memcopystream_8cpp.html#a3c7966dcc2aec8e20459e036f5823fc2", null ],
    [ "STATUS_READY", "de/dfa/host__memcopystream_8cpp.html#a64f314e17a1be8e6c8e09bb90b9ecb68", null ],
    [ "STATUS_RUNNING", "de/dfa/host__memcopystream_8cpp.html#a799b04d03f49612e3e636b56c3972b80", null ],
    [ "STATUS_WAITING", "de/dfa/host__memcopystream_8cpp.html#a4eb8ee21eeb7699dbe5fcee4c30d7b72", null ],
    [ "main", "de/dfa/host__memcopystream_8cpp.html#a0ddf1224851353fc92bfbff6f499fa97", null ],
    [ "timer_end", "de/dfa/host__memcopystream_8cpp.html#a91adc4ad42a1ba51b4ecdacd36d30ea6", null ],
    [ "timer_start", "de/dfa/host__memcopystream_8cpp.html#ad7a84cbdd9cee3c8099b4f9d1b720405", null ]
];