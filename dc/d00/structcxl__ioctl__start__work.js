var structcxl__ioctl__start__work =
[
    [ "amr", "dc/d00/structcxl__ioctl__start__work.html#a0145bb47db002813db9574f5c1d4743d", null ],
    [ "flags", "dc/d00/structcxl__ioctl__start__work.html#acfbda39c3e5e7f593a0411ba7445dd1a", null ],
    [ "num_interrupts", "dc/d00/structcxl__ioctl__start__work.html#ade3b6dc4540dfb2a68f66b0f507cd53d", null ],
    [ "reserved1", "dc/d00/structcxl__ioctl__start__work.html#ad4ce0d1606d473fefe8f9fff18a5a7d0", null ],
    [ "reserved2", "dc/d00/structcxl__ioctl__start__work.html#a817efb3b4afc8936fcf158bc9b6576a2", null ],
    [ "reserved3", "dc/d00/structcxl__ioctl__start__work.html#acb3cd882e73820ff6b1501d8193221bd", null ],
    [ "reserved4", "dc/d00/structcxl__ioctl__start__work.html#adee554ccef9147d925faf05e5fac3742", null ],
    [ "reserved5", "dc/d00/structcxl__ioctl__start__work.html#a0abcb649a0271f4df18f638080f9426c", null ],
    [ "tid", "dc/d00/structcxl__ioctl__start__work.html#a25aa25a2ec45c309705c3d522ac58962", null ],
    [ "work_element_descriptor", "dc/d00/structcxl__ioctl__start__work.html#ab5748ed08bb41265a583d0ce9b7855fe", null ]
];