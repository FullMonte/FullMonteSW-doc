var structFaceTetraLink =
[
    [ "TetraLink", "d2/d72/structFaceTetraLink_1_1TetraLink.html", "d2/d72/structFaceTetraLink_1_1TetraLink" ],
    [ "downTet", "dc/d04/structFaceTetraLink.html#ad10a86fd9f1035064b9a51b34980aa25", null ],
    [ "upTet", "dc/d04/structFaceTetraLink.html#ab39ebe39b59de7cdaadcf26bd9842075", null ],
    [ "pointIDs", "dc/d04/structFaceTetraLink.html#aab035d983d2ce86d7b2e9c3288a1d3ba", null ],
    [ "tets", "dc/d04/structFaceTetraLink.html#afd1818408b0a4b329ab236e964b3a53d", null ]
];