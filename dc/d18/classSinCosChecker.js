var classSinCosChecker =
[
    [ "input_type", "dc/d18/classSinCosChecker.html#a240ef2263830546728c33433c614f722", null ],
    [ "output_type", "dc/d18/classSinCosChecker.html#a042d9c77077b4e301565c376d025bf86", null ],
    [ "SinCosChecker", "dc/d18/classSinCosChecker.html#a428670e360a388e97b2fb74e90c5920a", null ],
    [ "clear", "dc/d18/classSinCosChecker.html#a40a13c05af74b82d99a030083c013301", null ],
    [ "operator()", "dc/d18/classSinCosChecker.html#a93727a5cbfd2d088ab5d2ee70774ec27", null ],
    [ "m_epsNorm", "dc/d18/classSinCosChecker.html#a230618d86d49d0404a0bc8730d6012c3", null ],
    [ "m_epsValue", "dc/d18/classSinCosChecker.html#a3cef6b1f5a108ab70bcf770736e94c0b", null ],
    [ "m_history", "dc/d18/classSinCosChecker.html#a74ac44665ff1981da8d4b83c49126eb9", null ]
];