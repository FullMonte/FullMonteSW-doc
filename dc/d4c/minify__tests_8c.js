var minify__tests_8c =
[
    [ "cjson_minify_should_minify_json", "dc/d4c/minify__tests_8c.html#a10f3d30e494600e717641b12b6effe6d", null ],
    [ "cjson_minify_should_not_loop_infinitely", "dc/d4c/minify__tests_8c.html#ab089bf82c3a3ed5cbc26e0ae7ed658b9", null ],
    [ "cjson_minify_should_not_modify_strings", "dc/d4c/minify__tests_8c.html#aaa97a1eb49a4a4b815159138a30302d9", null ],
    [ "cjson_minify_should_not_overflow_buffer", "dc/d4c/minify__tests_8c.html#aaa2c07732bce3b97ad7a32f06e866f57", null ],
    [ "cjson_minify_should_remove_multiline_comments", "dc/d4c/minify__tests_8c.html#a62f2306efbf2bfb87c8437526d702d36", null ],
    [ "cjson_minify_should_remove_single_line_comments", "dc/d4c/minify__tests_8c.html#a44da525c415c98813e000777359fc91f", null ],
    [ "cjson_minify_should_remove_spaces", "dc/d4c/minify__tests_8c.html#a418dd84eef18277ec6cdd9e36e895475", null ],
    [ "main", "dc/d4c/minify__tests_8c.html#a9fd408b0fa7364c093bb678cb12f9b83", null ]
];