var makeStim_8m =
[
    [ "fclose", "dc/d45/makeStim_8m.html#a5e769bbbabcaddc548203741c7100228", null ],
    [ "fprintf", "dc/d45/makeStim_8m.html#af1a351e26d624dcdc096febb4f11ff4a", null ],
    [ "makeStim", "dc/d45/makeStim_8m.html#a063e2b13e45a6ba3381b1f7775f9ad1d", null ],
    [ "costheta", "dc/d45/makeStim_8m.html#a7c71162fdc7b6f34c540fcbbcef2b45c", null ],
    [ "d", "dc/d45/makeStim_8m.html#a1aabac6d068eef6a7bad3fdf50a05cc8", null ],
    [ "fid", "dc/d45/makeStim_8m.html#ae9011d40c6f13e68e6f07156e0da7c5d", null ],
    [ "h", "dc/d45/makeStim_8m.html#a11dd570e5abab8a6805a547c731f07cb", null ],
    [ "l", "dc/d45/makeStim_8m.html#a5b54c0a045f179bcbbbc9abcb8b5cd4c", null ],
    [ "matID", "dc/d45/makeStim_8m.html#a27ab21972bef96c82cf4b3a77c82e0bd", null ],
    [ "mu_t", "dc/d45/makeStim_8m.html#ab724a32d4bef28fe2b06a27d06b4b9a0", null ],
    [ "o", "dc/d45/makeStim_8m.html#ae47ca7a09cf6781e29634502345930a7", null ],
    [ "p", "dc/d45/makeStim_8m.html#ac483f6ce851c9ecd9fb835ff7551737c", null ],
    [ "rnd", "dc/d45/makeStim_8m.html#a9d5692cfe96690460535b9a995040bcd", null ],
    [ "tetID", "dc/d45/makeStim_8m.html#a6557dd49e2bdb2ee980c3c250a5a68dd", null ]
];