var classTextFileMatrixWriter =
[
    [ "TextFileMatrixWriter", "dc/da1/classTextFileMatrixWriter.html#a5b5a3bd468ced4d0ced6f5bd56ae55d4", null ],
    [ "~TextFileMatrixWriter", "dc/da1/classTextFileMatrixWriter.html#a1f41ab1b14082ffa428ef4c239b07c9f", null ],
    [ "columns", "dc/da1/classTextFileMatrixWriter.html#a6cb80192e6cc3b7b0ceb559160eb0bfe", null ],
    [ "filename", "dc/da1/classTextFileMatrixWriter.html#a3a77cde4dd7cdb339d70ebbaf1fb99c8", null ],
    [ "precision", "dc/da1/classTextFileMatrixWriter.html#adcb48bacb94371e2fb62bcf6d1175b3b", null ],
    [ "source", "dc/da1/classTextFileMatrixWriter.html#a3e0c48474de335d3ec4d8b29931288e8", null ],
    [ "source", "dc/da1/classTextFileMatrixWriter.html#ac816ee32f192b751975c2020776c5fbb", null ],
    [ "width", "dc/da1/classTextFileMatrixWriter.html#a5043c978e73283c2dec7cf54df0b1c6f", null ],
    [ "write", "dc/da1/classTextFileMatrixWriter.html#a939e3e82a9bcca06909a00ca4cc4e082", null ],
    [ "m_columns", "dc/da1/classTextFileMatrixWriter.html#a09e9856063a465b58547b81f08d14295", null ],
    [ "m_data", "dc/da1/classTextFileMatrixWriter.html#a00ef00ef529524296c242a0a2b6aa702", null ],
    [ "m_filename", "dc/da1/classTextFileMatrixWriter.html#af8f68e01c7caa189487e7c92103499d3", null ],
    [ "m_precision", "dc/da1/classTextFileMatrixWriter.html#ab3ebcaf82af4b912eeba9235d70c4960", null ],
    [ "m_width", "dc/da1/classTextFileMatrixWriter.html#af0ac13a2a955174e21a5833a146cf425", null ]
];