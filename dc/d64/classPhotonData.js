var classPhotonData =
[
    [ "Posdir", "dc/d64/classPhotonData.html#a2076657ba3d9ec424da6ee5732c027d4", null ],
    [ "PhotonData", "dc/d64/classPhotonData.html#a72926b437c899e5ba5cb26257ff4c5c9", null ],
    [ "~PhotonData", "dc/d64/classPhotonData.html#a1fe780b9b399dd4c186e9c22ea81312b", null ],
    [ "ACCEPT_VISITOR_METHOD", "dc/d64/classPhotonData.html#ac35f768b322f41019a0572cf83d2c5f3", null ],
    [ "clone", "dc/d64/classPhotonData.html#a49e3cd1a8cc021bf94d487916d04ad4c", null ],
    [ "ray", "dc/d64/classPhotonData.html#aed9904624d0d0416875ea7700795b135", null ],
    [ "ray", "dc/d64/classPhotonData.html#a998558153a8e03a28c269359d409987b", null ],
    [ "weight", "dc/d64/classPhotonData.html#aad8d8ed2df3e9c0d02a0315b91b8c2a9", null ],
    [ "weight", "dc/d64/classPhotonData.html#a5201d14ba400ef017385e20dbb4ba288", null ],
    [ "m_name", "dc/d64/classPhotonData.html#ac8a2770299c91d0d712a35e4fb4cc0c1", null ],
    [ "m_ray", "dc/d64/classPhotonData.html#aa18fc4a05ce0e3ac5b26f6546ba8d9a3", null ],
    [ "m_weight", "dc/d64/classPhotonData.html#a98602d12a8e63fcb18477218c91592a6", null ]
];