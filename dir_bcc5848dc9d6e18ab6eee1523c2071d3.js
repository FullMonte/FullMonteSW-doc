var dir_bcc5848dc9d6e18ab6eee1523c2071d3 =
[
    [ "Emitters", "dir_b6f8862b1cb89e512072f8269d523560.html", "dir_b6f8862b1cb89e512072f8269d523560" ],
    [ "Logger", "dir_379fd1481d61baaecc8a570d5728c229.html", "dir_379fd1481d61baaecc8a570d5728c229" ],
    [ "AutoStreamBuffer.hpp", "d3/dd6/AutoStreamBuffer_8hpp.html", "d3/dd6/AutoStreamBuffer_8hpp" ],
    [ "avx_mathfun.h", "d7/d0e/avx__mathfun_8h.html", "d7/d0e/avx__mathfun_8h" ],
    [ "BlockRandomDistribution.hpp", "d4/dc9/BlockRandomDistribution_8hpp.html", "d4/dc9/BlockRandomDistribution_8hpp" ],
    [ "BlockRNGAdaptor.hpp", "d7/d8e/BlockRNGAdaptor_8hpp.html", "d7/d8e/BlockRNGAdaptor_8hpp" ],
    [ "FloatPM1Distribution.hpp", "d0/d4b/FloatPM1Distribution_8hpp.html", [
      [ "FloatPM1Distribution", "d8/dba/classFloatPM1Distribution.html", "d8/dba/classFloatPM1Distribution" ]
    ] ],
    [ "FloatU01Distribution.hpp", "d4/d33/FloatU01Distribution_8hpp.html", [
      [ "FloatU01Distribution", "d9/d40/classFloatU01Distribution.html", "d9/d40/classFloatU01Distribution" ]
    ] ],
    [ "FloatUnitExpDistribution.hpp", "d6/d88/FloatUnitExpDistribution_8hpp.html", [
      [ "FloatUnitExpDistribution", "d4/d41/classFloatUnitExpDistribution.html", "d4/d41/classFloatUnitExpDistribution" ]
    ] ],
    [ "FloatUVect2Distribution.hpp", "d8/df9/FloatUVect2Distribution_8hpp.html", [
      [ "FloatUVect2Distribution", "df/d96/classFloatUVect2Distribution.html", "df/d96/classFloatUVect2Distribution" ]
    ] ],
    [ "FloatVectorBase.cpp", "d4/db1/FloatVectorBase_8cpp.html", "d4/db1/FloatVectorBase_8cpp" ],
    [ "FloatVectorBase.hpp", "d9/d21/FloatVectorBase_8hpp.html", "d9/d21/FloatVectorBase_8hpp" ],
    [ "FloatVectorBase_NonSSE.cpp", "d3/db2/FloatVectorBase__NonSSE_8cpp.html", "d3/db2/FloatVectorBase__NonSSE_8cpp" ],
    [ "FloatVectorBase_NonSSE.hpp", "dd/d6c/FloatVectorBase__NonSSE_8hpp.html", "dd/d6c/FloatVectorBase__NonSSE_8hpp" ],
    [ "HenyeyGreenstein.cpp", "d1/d5c/HenyeyGreenstein_8cpp.html", "d1/d5c/HenyeyGreenstein_8cpp" ],
    [ "HenyeyGreenstein.hpp", "d2/dfc/HenyeyGreenstein_8hpp.html", "d2/dfc/HenyeyGreenstein_8hpp" ],
    [ "Material.cpp", "dd/dda/Kernels_2Software_2Material_8cpp.html", null ],
    [ "Material.hpp", "d0/d52/Kernels_2Software_2Material_8hpp.html", [
      [ "Material", "d6/dda/classx86Kernel_1_1Material.html", "d6/dda/classx86Kernel_1_1Material" ]
    ] ],
    [ "MCMLKernel.cpp", "d8/d91/MCMLKernel_8cpp.html", "d8/d91/MCMLKernel_8cpp" ],
    [ "MCMLKernel.hpp", "d2/d86/MCMLKernel_8hpp.html", "d2/d86/MCMLKernel_8hpp" ],
    [ "Packet.hpp", "db/d1b/Packet_8hpp.html", [
      [ "LaunchPacket", "d8/ddd/structLaunchPacket.html", "d8/ddd/structLaunchPacket" ],
      [ "Packet", "d7/d97/classPacket.html", "d7/d97/classPacket" ]
    ] ],
    [ "PacketDirection.cpp", "d8/de7/PacketDirection_8cpp.html", null ],
    [ "PacketDirection.hpp", "d1/d65/PacketDirection_8hpp.html", [
      [ "PacketDirection", "d0/d2b/structPacketDirection.html", "d0/d2b/structPacketDirection" ]
    ] ],
    [ "RNG_SFMT_AVX.hpp", "d5/d2a/RNG__SFMT__AVX_8hpp.html", [
      [ "RNG_SFMT_AVX", "d6/dfa/classRNG__SFMT__AVX.html", "d6/dfa/classRNG__SFMT__AVX" ]
    ] ],
    [ "SFMTWrapper.hpp", "d4/d77/SFMTWrapper_8hpp.html", "d4/d77/SFMTWrapper_8hpp" ],
    [ "sse.hpp", "df/d5b/sse_8hpp.html", "df/d5b/sse_8hpp" ],
    [ "sse_mathfun.h", "d1/dda/sse__mathfun_8h.html", "d1/dda/sse__mathfun_8h" ],
    [ "SSEConvert.hpp", "d5/d98/SSEConvert_8hpp.html", "d5/d98/SSEConvert_8hpp" ],
    [ "SSEMath.cpp", "df/df4/SSEMath_8cpp.html", "df/df4/SSEMath_8cpp" ],
    [ "SSEMath.hpp", "de/df2/SSEMath_8hpp.html", "de/df2/SSEMath_8hpp" ],
    [ "Tetra.cpp", "df/d9c/Tetra_8cpp.html", "df/d9c/Tetra_8cpp" ],
    [ "Tetra.hpp", "da/dde/Tetra_8hpp.html", "da/dde/Tetra_8hpp" ],
    [ "TetraInternalKernel.cpp", "dc/db2/TetraInternalKernel_8cpp.html", null ],
    [ "TetraInternalKernel.hpp", "d7/ddf/TetraInternalKernel_8hpp.html", "d7/ddf/TetraInternalKernel_8hpp" ],
    [ "TetraMCKernel.cpp", "db/d99/TetraMCKernel_8cpp.html", null ],
    [ "TetraMCKernel.hpp", "da/d87/TetraMCKernel_8hpp.html", [
      [ "TetraKernel", "d9/d26/classTetraKernel.html", "d9/d26/classTetraKernel" ],
      [ "ReflectiveFaceDef", "da/d81/structTetraKernel_1_1ReflectiveFaceDef.html", "da/d81/structTetraKernel_1_1ReflectiveFaceDef" ],
      [ "TetraMCKernel", "d6/d5e/classTetraMCKernel.html", "d6/d5e/classTetraMCKernel" ]
    ] ],
    [ "TetraMCKernelThread.hpp", "da/d41/TetraMCKernelThread_8hpp.html", "da/d41/TetraMCKernelThread_8hpp" ],
    [ "TetrasFromLayered.cpp", "de/dc2/TetrasFromLayered_8cpp.html", null ],
    [ "TetrasFromLayered.hpp", "d0/d28/TetrasFromLayered_8hpp.html", [
      [ "TetrasFromLayered", "dc/d02/classTetrasFromLayered.html", "dc/d02/classTetrasFromLayered" ]
    ] ],
    [ "TetrasFromTetraMesh.cpp", "d5/d04/TetrasFromTetraMesh_8cpp.html", null ],
    [ "TetrasFromTetraMesh.hpp", "dc/d50/TetrasFromTetraMesh_8hpp.html", [
      [ "TetrasFromTetraMesh", "de/db6/classTetrasFromTetraMesh.html", "de/db6/classTetrasFromTetraMesh" ]
    ] ],
    [ "TetraSurfaceKernel.hpp", "d9/d19/TetraSurfaceKernel_8hpp.html", "d9/d19/TetraSurfaceKernel_8hpp" ],
    [ "TetraSVKernel.hpp", "d3/df6/TetraSVKernel_8hpp.html", "d3/df6/TetraSVKernel_8hpp" ],
    [ "TetraTraceKernel.hpp", "da/d11/TetraTraceKernel_8hpp.html", "da/d11/TetraTraceKernel_8hpp" ],
    [ "TetraVolumeKernel.hpp", "d9/d50/TetraVolumeKernel_8hpp.html", "d9/d50/TetraVolumeKernel_8hpp" ],
    [ "ThreadedMCKernelBase.cpp", "d3/dfa/ThreadedMCKernelBase_8cpp.html", null ],
    [ "ThreadedMCKernelBase.hpp", "d5/d96/ThreadedMCKernelBase_8hpp.html", [
      [ "ThreadedMCKernelBase", "d8/d4c/classThreadedMCKernelBase.html", "d8/d4c/classThreadedMCKernelBase" ],
      [ "Thread", "da/d38/classThreadedMCKernelBase_1_1Thread.html", "da/d38/classThreadedMCKernelBase_1_1Thread" ]
    ] ],
    [ "UniformUI32Distribution.hpp", "d4/d15/UniformUI32Distribution_8hpp.html", [
      [ "UniformUI32Distribution", "df/d80/classUniformUI32Distribution.html", "df/d80/classUniformUI32Distribution" ]
    ] ]
];