var classAccumulatorTraits =
[
    [ "Input", "d4/dcc/structAccumulatorTraits_1_1Input.html", "d4/dcc/structAccumulatorTraits_1_1Input" ],
    [ "Output", "d2/dc5/structAccumulatorTraits_1_1Output.html", "d2/dc5/structAccumulatorTraits_1_1Output" ],
    [ "input_container_type", "d9/d93/classAccumulatorTraits.html#a23d28f258dd7dcdde6118c343388f2d8", null ],
    [ "input_type", "d9/d93/classAccumulatorTraits.html#ac3e7b621c2921507b06bde13e009b146", null ],
    [ "output_container_type", "d9/d93/classAccumulatorTraits.html#a8ccec83f2add104b8c89bcf5de10bc73", null ],
    [ "output_type", "d9/d93/classAccumulatorTraits.html#a8084df9f0ffa7a39ab908d174c5b4e08", null ],
    [ "packed_input_type", "d9/d93/classAccumulatorTraits.html#ada34c2d430270b5ee3fa585f9c2ac6b2", null ],
    [ "packed_output_type", "d9/d93/classAccumulatorTraits.html#a924423aa60ddff640368bd94a8cd275b", null ],
    [ "convertFromNativeType", "d9/d93/classAccumulatorTraits.html#afc757a500f158801722f299995243095", null ],
    [ "convertToNativeType", "d9/d93/classAccumulatorTraits.html#acad3c7906dba75d56338cdf65c0c711a", null ],
    [ "convertToNativeType", "d9/d93/classAccumulatorTraits.html#a4dc6d46482c519b735531f3bd9d46be9", null ],
    [ "input_bits", "d9/d93/classAccumulatorTraits.html#a9d8065e3921e1446d5d36d729efc6c72", null ],
    [ "output_bits", "d9/d93/classAccumulatorTraits.html#a7c906271a9b75861e5f061d43414bc47", null ]
];