var classpr__controller =
[
    [ "pr_controller.rtl", "d0/de1/classpr__controller_1_1rtl.html", "d0/de1/classpr__controller_1_1rtl" ],
    [ "alt_pr_0_clk_clk", "d9/dd5/classpr__controller.html#a2c2c9a3de4e1897d2d70daa0e9b05e79", null ],
    [ "alt_pr_0_data_data", "d9/dd5/classpr__controller.html#aac2181ae414a99a61b9b0bbd1812933e", null ],
    [ "alt_pr_0_data_read_data_read", "d9/dd5/classpr__controller.html#a76b5050cbb3186b97f6334517438eadc", null ],
    [ "alt_pr_0_data_valid_data_valid", "d9/dd5/classpr__controller.html#aaaca1998d517f73b37a2ce5f350125ec", null ],
    [ "alt_pr_0_double_pr_double_pr", "d9/dd5/classpr__controller.html#a0ef789c00a0776d58b1f11d1afef184e", null ],
    [ "alt_pr_0_freeze_freeze", "d9/dd5/classpr__controller.html#a7ad5cccb2258bb963f473970847585b0", null ],
    [ "alt_pr_0_nreset_reset", "d9/dd5/classpr__controller.html#adeadf5cfb0968ca196b705f36f04e750", null ],
    [ "alt_pr_0_pr_start_pr_start", "d9/dd5/classpr__controller.html#a7537d06cc5ba4deea31685ca4af07f5f", null ],
    [ "alt_pr_0_status_status", "d9/dd5/classpr__controller.html#ac5e8769411a577c15c68c2e0d50ef059", null ],
    [ "IEEE", "d9/dd5/classpr__controller.html#a89677f6266e2f140378900fc0affa697", null ],
    [ "numeric_std", "d9/dd5/classpr__controller.html#a9518aa3705b3771e2849180cc5689c5e", null ],
    [ "std_logic_1164", "d9/dd5/classpr__controller.html#ab984b7e959e0f8abf399fa3fbbbac685", null ]
];