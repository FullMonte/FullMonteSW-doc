var classTetraKernel =
[
    [ "ReflectiveFaceDef", "da/d81/structTetraKernel_1_1ReflectiveFaceDef.html", "da/d81/structTetraKernel_1_1ReflectiveFaceDef" ],
    [ "~TetraKernel", "d9/d26/classTetraKernel.html#a6f4840a593361a55da0f26a68423dfda", null ],
    [ "allocateReflectiveFaceTable", "d9/d26/classTetraKernel.html#a47273ee50cbd9e5f1ad648c420f9c8c9", null ],
    [ "applyReflectiveSurface", "d9/d26/classTetraKernel.html#a9aacc70499e94fda5d76decaaf8bea84", null ],
    [ "applyReflectiveSurfaces", "d9/d26/classTetraKernel.html#a2cf3da88c32ef45fccd6efd235cae784", null ],
    [ "clearReflectiveSurfaces", "d9/d26/classTetraKernel.html#a3e075f6351c243fb6894886bd1fc1a79", null ],
    [ "deleteReflectiveFaceTable", "d9/d26/classTetraKernel.html#a5f683de81171f2a838194a08efa0ee3f", null ],
    [ "markReflectiveSurface", "d9/d26/classTetraKernel.html#afbc6bbfc1a8049f5a69de5310102e265", null ],
    [ "markTetraFaceForFluenceScoring", "d9/d26/classTetraKernel.html#a8a542d2b4887397a8b1009370d29f0f5", null ],
    [ "unmarkAllReflectiveSurfaces", "d9/d26/classTetraKernel.html#aa7f88b210eb88c30c5ce7336656de170", null ],
    [ "unmarkReflectiveSurface", "d9/d26/classTetraKernel.html#a11f3af8e03fb5263f37872e91c6678da", null ],
    [ "m_reflectiveFaces", "d9/d26/classTetraKernel.html#a3d59831033a19ec1e1f35a7051d6c530", null ],
    [ "m_reflectiveFaceTable", "d9/d26/classTetraKernel.html#a9d9f44eac2c5fa59f5b82fe719367235", null ],
    [ "m_tetras", "d9/d26/classTetraKernel.html#a9f35eb38fdd2baf89a09ecc6cd0fe7ab", null ]
];