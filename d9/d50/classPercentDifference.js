var classPercentDifference =
[
    [ "Mode", "d9/d50/classPercentDifference.html#aae765a4d2d0d93afbf165c5a58ead40b", [
      [ "Average", "d9/d50/classPercentDifference.html#aae765a4d2d0d93afbf165c5a58ead40ba5c8873e4defc231a78a749954da1cc88", null ],
      [ "A", "d9/d50/classPercentDifference.html#aae765a4d2d0d93afbf165c5a58ead40baa0a66d628ffd0e0c3dbe5b95f172128b", null ],
      [ "B", "d9/d50/classPercentDifference.html#aae765a4d2d0d93afbf165c5a58ead40ba84c2b3d0fb44181b507f603a8a9872a3", null ]
    ] ],
    [ "PercentDifference", "d9/d50/classPercentDifference.html#a3c10e4895a1d036c062c0875256fb22f", null ],
    [ "~PercentDifference", "d9/d50/classPercentDifference.html#a51acba7faef7fb23f1018af119222fd0", null ],
    [ "dimCheck", "d9/d50/classPercentDifference.html#a2dd2800c5a7caff45b0627e01df48c14", null ],
    [ "doUpdate", "d9/d50/classPercentDifference.html#a5e77e01b31e9be2ba8da276345ee82bc", null ],
    [ "useAverageForReference", "d9/d50/classPercentDifference.html#aa4298bea0f0edac0523331509d6e0c56", null ],
    [ "m_denominatorMode", "d9/d50/classPercentDifference.html#ac1ea3bf17212dfc0d67dad6cdce066e1", null ]
];