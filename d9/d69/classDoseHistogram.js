var classDoseHistogram =
[
    [ "Element", "da/d73/structDoseHistogram_1_1Element.html", "da/d73/structDoseHistogram_1_1Element" ],
    [ "const_iterator", "d9/d69/classDoseHistogram.html#a2acb5a984b26b574da6877ed9e68aa9e", null ],
    [ "Element", "d9/d69/classDoseHistogram.html#a8febba6b420548869f0953ec686fd3f1", null ],
    [ "DoseHistogram", "d9/d69/classDoseHistogram.html#af18e7dc44f38da69fd58a1999184580d", null ],
    [ "~DoseHistogram", "d9/d69/classDoseHistogram.html#abe807ca43aa5817df232843f329440e9", null ],
    [ "DoseHistogram", "d9/d69/classDoseHistogram.html#ab560310498c5fd976095f49708b0ae8f", null ],
    [ "ACCEPT_VISITOR_METHOD", "d9/d69/classDoseHistogram.html#aa309034000129fa27764e18574e4dd10", null ],
    [ "begin", "d9/d69/classDoseHistogram.html#ac428f909c4dfed2a26b664098028bded", null ],
    [ "CLONE_METHOD", "d9/d69/classDoseHistogram.html#a3ef84cebb997451cf50787aa33268409", null ],
    [ "dim", "d9/d69/classDoseHistogram.html#a27ca91d6172d3d5e98b46a95aea72743", null ],
    [ "doseAtPercentile", "d9/d69/classDoseHistogram.html#a06d8382d5a0fc19780b79b92670a17dc", null ],
    [ "end", "d9/d69/classDoseHistogram.html#ab003bf27269a665c197d89800d2e5b7c", null ],
    [ "get", "d9/d69/classDoseHistogram.html#a55b270b4518365f3fc39d8e97ef4fcb4", null ],
    [ "percentileOfDose", "d9/d69/classDoseHistogram.html#a36904bb22bd084c0cfe1c3704c6f09ed", null ],
    [ "staticType", "d9/d69/classDoseHistogram.html#a34db5c5e20bf87304125aee9c9c65b5c", null ],
    [ "totalMeasure", "d9/d69/classDoseHistogram.html#a2cef44041a133da6e52674b58051cbc6", null ],
    [ "type", "d9/d69/classDoseHistogram.html#a03fb8d2469419a6ac7adaf908db89152", null ],
    [ "m_histogram", "d9/d69/classDoseHistogram.html#a6908d2a191c12ae039b0f87d36b5e62d", null ],
    [ "s_type", "d9/d69/classDoseHistogram.html#a63cbcb07fbfbfcc2f58101a7bbaa6ce9", null ]
];