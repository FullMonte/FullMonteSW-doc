var structFullMonteHW_1_1CacheStats =
[
    [ "serialize", "d9/dcb/structFullMonteHW_1_1CacheStats.html#a99f6c2abcf5fe6f4c24b94c96f880edc", null ],
    [ "operator<<", "d9/dcb/structFullMonteHW_1_1CacheStats.html#a5c1d2628657dc41c21b1dd525e710270", null ],
    [ "bits", "d9/dcb/structFullMonteHW_1_1CacheStats.html#a7691919fe83e69fe14b31cf3a4303729", null ],
    [ "nAccess", "d9/dcb/structFullMonteHW_1_1CacheStats.html#a6ba8997c5f17b596a8aa667eb772ff17", null ],
    [ "nHit", "d9/dcb/structFullMonteHW_1_1CacheStats.html#ac1f7631b2fd402f5e3020a393b22690b", null ],
    [ "nMiss", "d9/dcb/structFullMonteHW_1_1CacheStats.html#afe6072d7ff8eec5a66245edc7e337c65", null ]
];