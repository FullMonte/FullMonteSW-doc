var unity__output__Spy_8c =
[
    [ "UnityOutputCharSpy_Create", "d9/d4b/unity__output__Spy_8c.html#a85e56ca3f8f83e96e1e2bbfbb4a15a2f", null ],
    [ "UnityOutputCharSpy_Destroy", "d9/d4b/unity__output__Spy_8c.html#a0915ac11fce4fd69d1c961c7cd050f8e", null ],
    [ "UnityOutputCharSpy_Enable", "d9/d4b/unity__output__Spy_8c.html#a5a5b8c2fe4d1edd31559deb7000c7d7f", null ],
    [ "UnityOutputCharSpy_Get", "d9/d4b/unity__output__Spy_8c.html#a014a3109626a78fb0c9ffeec76846e35", null ],
    [ "UnityOutputCharSpy_OutputChar", "d9/d4b/unity__output__Spy_8c.html#a4b35b5d169eba72055b3b487b3579ddf", null ],
    [ "buffer", "d9/d4b/unity__output__Spy_8c.html#aff2566f4c366b48d73479bef43ee4d2e", null ],
    [ "count", "d9/d4b/unity__output__Spy_8c.html#ad43c3812e6d13e0518d9f8b8f463ffcf", null ],
    [ "size", "d9/d4b/unity__output__Spy_8c.html#a439227feff9d7f55384e8780cfc2eb82", null ],
    [ "spy_enable", "d9/d4b/unity__output__Spy_8c.html#ad979992bfbe72e0c950e5112dc52c310", null ]
];