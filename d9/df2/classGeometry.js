var classGeometry =
[
    [ "~Geometry", "d9/df2/classGeometry.html#ad55e832122ab3a2833dcaa6507867678", null ],
    [ "Geometry", "d9/df2/classGeometry.html#a4c301c163c63d21ed08c17b0f4e131d3", null ],
    [ "directedSurfaceAreas", "d9/df2/classGeometry.html#a022a4c950be6acd56999cda8e6f49c7e", null ],
    [ "elementVolumes", "d9/df2/classGeometry.html#aa4a55cb9b98809dd04d5b3c93f8153df", null ],
    [ "getUnit", "d9/df2/classGeometry.html#a39ff67ff0237c89fb915895d15535b47", null ],
    [ "regions", "d9/df2/classGeometry.html#a885919f0d8c30ec58ed057f38470500d", null ],
    [ "regions", "d9/df2/classGeometry.html#a88625709691b00c72c22690d60b6e76c", null ],
    [ "surfaceAreas", "d9/df2/classGeometry.html#a2d2d39b04a5b4f6dce855857c096dd98", null ],
    [ "unitDimension", "d9/df2/classGeometry.html#abea95895a7c88b6ebce15d51fc7ecb64", null ],
    [ "unitDimension", "d9/df2/classGeometry.html#a48a0b047f86cd030451591837f82c0a3", null ],
    [ "m_regions", "d9/df2/classGeometry.html#a098e5c78a136ae1e61121b0ea55748e8", null ],
    [ "m_unit", "d9/df2/classGeometry.html#a4a55d65eb164f83a4e3361cd3ce16a0d", null ]
];