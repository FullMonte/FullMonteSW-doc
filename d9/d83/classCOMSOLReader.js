var classCOMSOLReader =
[
    [ "COMSOLReader", "d9/d83/classCOMSOLReader.html#ab7a9bb2a7c01d240b0120640458b9f4a", null ],
    [ "~COMSOLReader", "d9/d83/classCOMSOLReader.html#aa36410b72221ac010b04d10f8972f080", null ],
    [ "filename", "d9/d83/classCOMSOLReader.html#a59c053c3a09f0fdda498e3d83d82ac2a", null ],
    [ "mesh", "d9/d83/classCOMSOLReader.html#a343c34516ba51b12ee2ec1b6b66e0736", null ],
    [ "read", "d9/d83/classCOMSOLReader.html#a011d754af3bb60cca3ffc2a7911649c5", null ],
    [ "tetRegionsReadOK", "d9/d83/classCOMSOLReader.html#ada195015f44ad166c4744b6db58346af", null ],
    [ "m_builder", "d9/d83/classCOMSOLReader.html#a3de6fad0785d56ebdb22d30b5de19a25", null ],
    [ "m_edgeRegions", "d9/d83/classCOMSOLReader.html#a7b7c444216a9f03ef25233d240dc8a00", null ],
    [ "m_edges", "d9/d83/classCOMSOLReader.html#a4c61c0746e7ffdfc623a437ca8396c13", null ],
    [ "m_faceRegions", "d9/d83/classCOMSOLReader.html#a718ed06cf69b853018624dd63e9ae259", null ],
    [ "m_faces", "d9/d83/classCOMSOLReader.html#ab9a4de830a67d914028db5d82a7cdcca", null ],
    [ "m_filename", "d9/d83/classCOMSOLReader.html#ad1eba1dae452aaaf0e7efec85cb7f7af", null ],
    [ "m_points", "d9/d83/classCOMSOLReader.html#a13e8f659156606fb4e119fe7707fd8dc", null ],
    [ "m_tetras", "d9/d83/classCOMSOLReader.html#ab2ff8db1b720fe33a31df455d97aedf5", null ],
    [ "m_tetRegions", "d9/d83/classCOMSOLReader.html#a664faf101f5577d03409379af28a8b05", null ],
    [ "m_tetRegionsOK", "d9/d83/classCOMSOLReader.html#a17bdc648a30f50e9857d71b5c15e4b6e", null ],
    [ "m_vertRegions", "d9/d83/classCOMSOLReader.html#a56dd633e9bdc53d4330218977d9105b8", null ],
    [ "m_verts", "d9/d83/classCOMSOLReader.html#a4516abe836905a22e12b99441b630584", null ]
];