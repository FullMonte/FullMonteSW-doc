var searchData=
[
  ['j',['j',['../d2/dc5/classSpinMatrix.html#ae43de0fe10bb84a76c6051c5fad27b5f',1,'SpinMatrix']]],
  ['j_5fcm',['J_cm',['../d0/d86/classOutputData.html#af72dca41b98b54fa2b98cfc76b9fec78a78f22576ba3853a68eef9bc0794ee984',1,'OutputData']]],
  ['j_5fm',['J_m',['../d0/d86/classOutputData.html#af72dca41b98b54fa2b98cfc76b9fec78aaa02edefccc94e3f9011f188fc5a0529',1,'OutputData']]],
  ['j_5fmm',['J_mm',['../d0/d86/classOutputData.html#af72dca41b98b54fa2b98cfc76b9fec78a091faa9a0ad7875dfbd15063d5218ea5',1,'OutputData']]],
  ['joule',['Joule',['../d1/db8/classKernel.html#a3bbeb1fd0cd6ed53f1813e2e23bc6a13a7de203da8ce48f48e6a3db9a357c7bfa',1,'Kernel']]],
  ['joulesperoutputenergyunit',['joulesPerOutputEnergyUnit',['../db/d07/UnitConverter_8hpp.html#ada7a106c9c0469091ae378a74b7c84e8',1,'joulesPerOutputEnergyUnit(float j):&#160;UnitConverter.hpp'],['../db/d07/UnitConverter_8hpp.html#a951f8ae05586dc3e4c6ba20c7506d537',1,'joulesPerOutputEnergyUnit() const:&#160;UnitConverter.hpp']]]
];
