var searchData=
[
  ['r',['r',['../de/d75/structCylCoord.html#a920175863212352ddb0fa8e917308bd0',1,'CylCoord']]],
  ['randlaunch',['randLaunch',['../d8/d42/classTetraMCP8Kernel.html#aec2b66aabbe7d38ce20f5906a8cba8b3',1,'TetraMCP8Kernel']]],
  ['readwritelatencyhosthw_5fmean',['readWriteLatencyHostHW_mean',['../d8/d42/classTetraMCP8Kernel.html#a5983ba8b649ffed6b6248a9ea31a8713',1,'TetraMCP8Kernel']]],
  ['region',['region',['../d4/d08/structDoseHistogramGenerator_1_1ElRegion.html#a7219f413ae8a3dfac6c2ad390aefbcbd',1,'DoseHistogramGenerator::ElRegion']]],
  ['resv',['resv',['../d2/d37/structTetraMCP8Kernel_1_1MemcopyWED.html#ac937fd3b48cefdb925aad7157dbc9e63',1,'TetraMCP8Kernel::MemcopyWED']]],
  ['returnblockbytesize',['returnBlockByteSize',['../d3/df6/classBlockRNGAdaptor.html#a7a1564987e3500df07255711ec69b156',1,'BlockRNGAdaptor']]],
  ['returnblocksize',['returnBlockSize',['../d3/df6/classBlockRNGAdaptor.html#a47fb6f588d05c6e7a17976c0e355ba80',1,'BlockRNGAdaptor']]],
  ['ri',['ri',['../d5/dd5/structCylIndex.html#a371594d1764cb7a416b788e85bb92a24',1,'CylIndex']]],
  ['rngseed_5f',['rngSeed_',['../d0/ddf/classMCKernelBase.html#acca6d0d4a03bca6daed6fd26b9bbcef5',1,'MCKernelBase']]]
];
