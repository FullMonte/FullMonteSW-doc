var searchData=
[
  ['uniform',['UNIFORM',['../df/d6a/classSource_1_1Cylinder.html#aa3d6c082e15d3eeb8e93a08ae53a3c86afbb08369047fc1d88bb8e714c9fa61fb',1,'Source::Cylinder::UNIFORM()'],['../d8/d31/ThetaDistribution_8hpp.html#a6b01d02cb7fe9011b597a648aefb3aaea8f44784d154005a214e0fe94119d28ef',1,'UNIFORM():&#160;ThetaDistribution.hpp']]],
  ['unknown',['Unknown',['../db/d11/classMMCJSONCase.html#ac9d72d4adf1b993672bb4baa89f509f4a148b9f06ba7393af3600835d85e315fb',1,'MMCJSONCase::Unknown()'],['../d8/dba/classvtkDoseHistogramWrapper.html#ab464f897d2d7ebc624e50377cd864faba634e8e1750f546d110d30c755bbe87b1',1,'vtkDoseHistogramWrapper::Unknown()']]],
  ['unknownfield',['UnknownField',['../de/dee/classEnergyToFluence.html#a9ff3bb79b7017303fee806312bc73d7dac72ccd6c633dd9f55f8a39ef1f760fc4',1,'EnergyToFluence']]],
  ['unknownmeasuretype',['UnknownMeasureType',['../de/dee/classEnergyToFluence.html#a3608f4636186ef2f794bdb2a5adca1efa01ad253c85e2803e9440b3375861dff7',1,'EnergyToFluence']]],
  ['unknownoutputtype',['UnknownOutputType',['../d0/d86/classOutputData.html#a4825fb70f99fdb498b34915a4b71fee7a450df4d6db3f7d6f0774a3d53708733b',1,'OutputData']]],
  ['unknownspacetype',['UnknownSpaceType',['../d6/d12/classAbstractSpatialMap.html#a1ee0d0fc61533fd02c5b0c6f737ead8ba843786843e32de5658937c5de813ae09',1,'AbstractSpatialMap']]],
  ['unknownunittype',['UnknownUnitType',['../d0/d86/classOutputData.html#af72dca41b98b54fa2b98cfc76b9fec78a9cac73f81ed21aec3fda81790ce4d977',1,'OutputData']]],
  ['unknownvaluetype',['UnknownValueType',['../d6/d12/classAbstractSpatialMap.html#a0bc128e855a84bad42c6587707074c57aa9bb69977dee37e28acc92b0404c7ab6',1,'AbstractSpatialMap']]]
];
