var searchData=
[
  ['m256_5fequiv1imm',['M256_EQUIV1IMM',['../d7/d0e/avx__mathfun_8h.html#a676b162ab311d154e96748e060ed55d0',1,'avx_mathfun.h']]],
  ['m256_5fequiv2',['M256_EQUIV2',['../d7/d0e/avx__mathfun_8h.html#a2c2ae3dceec65418cc62c6e4521059af',1,'avx_mathfun.h']]],
  ['m256_5fsplit',['M256_SPLIT',['../d7/d0e/avx__mathfun_8h.html#a0b76b1567ec826428edd589eb5a760de',1,'avx_mathfun.h']]],
  ['m256i_5fequiv1imm',['M256I_EQUIV1IMM',['../d7/d0e/avx__mathfun_8h.html#aafca483df25307485e5d36cf8bfbc0ab',1,'avx_mathfun.h']]],
  ['m256i_5fequiv2',['M256I_EQUIV2',['../d7/d0e/avx__mathfun_8h.html#aa6a7f517deee0170cd8fd9eef82cd51a',1,'avx_mathfun.h']]],
  ['m256i_5fsplit',['M256I_SPLIT',['../d7/d0e/avx__mathfun_8h.html#a490dcaa095f5b10a6dbec19058571a7e',1,'avx_mathfun.h']]],
  ['maybe',['MAYBE',['../d0/d5a/TetraEnclosingPointByLinearSearch_8cpp.html#af33ed35b7ee50ce7cfd03b517fe9b72e',1,'TetraEnclosingPointByLinearSearch.cpp']]]
];
