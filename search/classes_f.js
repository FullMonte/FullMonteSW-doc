var searchData=
[
  ['randominplane',['RandomInPlane',['../dd/df6/classEmitter_1_1RandomInPlane.html',1,'Emitter']]],
  ['ray',['Ray',['../d9/dce/classRay.html',1,'']]],
  ['raythroughmesh',['RayThroughMesh',['../df/d89/classRayThroughMesh.html',1,'']]],
  ['raywalkiterator',['RayWalkIterator',['../d4/d0e/classRayWalkIterator.html',1,'']]],
  ['reader',['Reader',['../d6/d87/classReader.html',1,'']]],
  ['reflectivefacedef',['ReflectiveFaceDef',['../da/d81/structTetraKernel_1_1ReflectiveFaceDef.html',1,'TetraKernel']]],
  ['regioncomp',['RegionComp',['../d3/d3f/structDoseHistogramGenerator_1_1ElRegion_1_1RegionComp.html',1,'DoseHistogramGenerator::ElRegion']]],
  ['remapregions',['RemapRegions',['../df/dea/classRemapRegions.html',1,'']]],
  ['remaptable',['RemapTable',['../d4/d38/classRemapTable.html',1,'']]],
  ['rescale',['Rescale',['../dc/d36/classRescale.html',1,'']]],
  ['rlerecord',['RLERecord',['../d3/dd6/structMemTraceScorer_1_1RLERecord.html',1,'MemTraceScorer']]],
  ['rng_5fsfmt_5favx',['RNG_SFMT_AVX',['../d6/dfa/classRNG__SFMT__AVX.html',1,'']]],
  ['rtintersection',['RTIntersection',['../d9/de5/structRTIntersection.html',1,'']]]
];
