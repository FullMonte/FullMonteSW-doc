var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~",
  1: "abcdefghiklmopqrstuvwxz",
  2: "dekrstux",
  3: "abcdefghiklmnopqrstuvwz",
  4: "_abcdefghijklmnopqrstuvwz~",
  5: "_abcdefghilmnopqrstuvwxz",
  6: "acdefikmprstuvx",
  7: "cdefgmopstuv",
  8: "abcdefijlmnoprstuvw",
  9: "cdopt",
  10: "_abcdilmnoprswy",
  11: "bcdhilrv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Pages"
};

