var classRemapRegions =
[
    [ "RemapRegions", "classRemapRegions.html#a4a03a41ffb487a15d9ee9eaa4083d613", null ],
    [ "~RemapRegions", "classRemapRegions.html#acb5f0175d8d966c1f5ae52bcf9ddab15", null ],
    [ "addMapping", "classRemapRegions.html#a034c8b3efe8c87c074a90e14e6b3e214", null ],
    [ "keepUnchanged", "classRemapRegions.html#a65968df86ff54eba936e92f1cad886a0", null ],
    [ "leaveUnspecifiedAsIs", "classRemapRegions.html#a9463a1587e49a5da335af7d51ee08f47", null ],
    [ "partition", "classRemapRegions.html#ac18d0abae66b91838286620f25c22eab", null ],
    [ "remapUnspecifiedTo", "classRemapRegions.html#aac6aa5da103fb886c0910d8aface6666", null ],
    [ "removeMapping", "classRemapRegions.html#ad88884bcbfb1c5b934a08ae540e2a251", null ],
    [ "result", "classRemapRegions.html#a903a86ece375b8066bf632328cf3cc7c", null ],
    [ "update", "classRemapRegions.html#a98fdd5925de96ea38f50f9130206b2b2", null ]
];