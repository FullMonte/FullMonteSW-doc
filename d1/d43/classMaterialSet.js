var classMaterialSet =
[
    [ "MaterialSet", "d1/d43/classMaterialSet.html#aec0bba020d1c7fbe4bf95be2a87e7094", null ],
    [ "~MaterialSet", "d1/d43/classMaterialSet.html#ae95c7e7a89bab39f1ae278a9cc877bf1", null ],
    [ "append", "d1/d43/classMaterialSet.html#ab01b8a524b35710f494dde23702b58a6", null ],
    [ "clone", "d1/d43/classMaterialSet.html#a1bbbf22c30ea3645a94b2cc6b6b40bde", null ],
    [ "exterior", "d1/d43/classMaterialSet.html#aed312f2d38d2676da0713eb899bcfa91", null ],
    [ "exterior", "d1/d43/classMaterialSet.html#af8e6e5a49aa52eb932fcbd995f9f8e89", null ],
    [ "matchedBoundary", "d1/d43/classMaterialSet.html#abe25c3b196151b2963bdfddcadea2b29", null ],
    [ "matchedBoundary", "d1/d43/classMaterialSet.html#aa5c4c8d0182575f18bc7192dec7a4662", null ],
    [ "material", "d1/d43/classMaterialSet.html#a228c51b4e7fecc2bc945b7da28c9dbc4", null ],
    [ "set", "d1/d43/classMaterialSet.html#aac060822f4b2c04daff0842379f4c729", null ],
    [ "m_matched", "d1/d43/classMaterialSet.html#a2b4c55dafb7df4b6a6a3dac370377856", null ]
];