var structBoundaryTraits_1_1Input =
[
    [ "age", "d1/ddb/structBoundaryTraits_1_1Input.html#a23ef6c22800a23b17a623fd2d81b8e0a", null ],
    [ "costheta", "d1/ddb/structBoundaryTraits_1_1Input.html#a4589844f3f414366cc6a9046dfee86d0", null ],
    [ "dir", "d1/ddb/structBoundaryTraits_1_1Input.html#a21d9896a58d063d54de82bdfbd1285fb", null ],
    [ "height", "d1/ddb/structBoundaryTraits_1_1Input.html#af5ff3784f6f79d4a2bacc51974cb4713", null ],
    [ "idx", "d1/ddb/structBoundaryTraits_1_1Input.html#a949d9ef6da715e5707fa582b369e0720", null ],
    [ "ifcID", "d1/ddb/structBoundaryTraits_1_1Input.html#a0e0e3289374ec7bba8d249c138b2f351", null ],
    [ "l", "d1/ddb/structBoundaryTraits_1_1Input.html#a68b0ff63f19d7a32af0d0200e8a262fc", null ],
    [ "matID", "d1/ddb/structBoundaryTraits_1_1Input.html#a4ad00fb0b96ccfd9e13caeeb29fce27e", null ],
    [ "pos", "d1/ddb/structBoundaryTraits_1_1Input.html#affebae1caeefa4b897b1efec1e695713", null ],
    [ "tag", "d1/ddb/structBoundaryTraits_1_1Input.html#aeff6446fcb4af15d7fbc56085360bff4", null ],
    [ "tetID", "d1/ddb/structBoundaryTraits_1_1Input.html#a37935464a29a6650a8a9db3ea8626544", null ],
    [ "tetraID", "d1/ddb/structBoundaryTraits_1_1Input.html#aef6b7aca4bc122a71daab45cd8bcb294", null ],
    [ "w", "d1/ddb/structBoundaryTraits_1_1Input.html#a75bf9c4c6399543d88dba40f1e18d962", null ]
];