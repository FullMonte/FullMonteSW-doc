var structrecord =
[
    [ "address", "d1/d49/structrecord.html#abfdcc46d8017e680b8dc1bc373cc988e", null ],
    [ "city", "d1/d49/structrecord.html#a14039bf4a0520076003f1494832f7776", null ],
    [ "country", "d1/d49/structrecord.html#afcec0e174404ecb1d9f60b0796bdb316", null ],
    [ "lat", "d1/d49/structrecord.html#aead5d6a207526cb625d716c030fa3d4d", null ],
    [ "lon", "d1/d49/structrecord.html#a36c8c25419ba59e1288560da3dd6bfd0", null ],
    [ "precision", "d1/d49/structrecord.html#a1a2c753f8662ba3b36c6724bd68a79e6", null ],
    [ "state", "d1/d49/structrecord.html#a5c25c1774d401255c03eddb9476bd3f4", null ],
    [ "zip", "d1/d49/structrecord.html#a351a82e368a89d7ff90ef9ece2520aa8", null ]
];