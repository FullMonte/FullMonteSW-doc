var structafu__descriptor =
[
    [ "AFU_CR_len", "d1/d8d/structafu__descriptor.html#a22435559edd72a3093d1e90396d6199f", null ],
    [ "AFU_CR_offset", "d1/d8d/structafu__descriptor.html#aab63aa9acc6aed815cdc791803c277bc", null ],
    [ "AFU_EB_len", "d1/d8d/structafu__descriptor.html#a2f0981a65d1f5e46bd3aa724a30896bc", null ],
    [ "AFU_EB_offset", "d1/d8d/structafu__descriptor.html#a2c2026944e4f9536e2928ebdb2cf00da", null ],
    [ "crptr", "d1/d8d/structafu__descriptor.html#a21b446eb514cb8ad0d5dcfe76a5eb3df", null ],
    [ "num_ints_per_process", "d1/d8d/structafu__descriptor.html#a33b53db6833cb00698c5f5e288dc99e6", null ],
    [ "num_of_afu_CRs", "d1/d8d/structafu__descriptor.html#ac06603ac9c448c3d2a6fd09e59209b0d", null ],
    [ "num_of_processes", "d1/d8d/structafu__descriptor.html#ab3b14ffd42cb3e05243618b3fbf260f8", null ],
    [ "PerProcessPSA", "d1/d8d/structafu__descriptor.html#aaded42f3cdb576a03979004c9310acf1", null ],
    [ "PerProcessPSA_offset", "d1/d8d/structafu__descriptor.html#adf04c58abda6a59a0b83dea104eb845a", null ],
    [ "req_prog_model", "d1/d8d/structafu__descriptor.html#a2b6bd59ae158281edd7b0dc277841437", null ],
    [ "reserved1", "d1/d8d/structafu__descriptor.html#aa86a2ba54ef01db6b5eb3e22ecc5b086", null ],
    [ "reserved2", "d1/d8d/structafu__descriptor.html#a2c63266eb162943d57c2b7fd5b818026", null ],
    [ "reserved3", "d1/d8d/structafu__descriptor.html#abc7ce4f101de15f886f8033db4f7118f", null ]
];