var checkPacketDirection_8m =
[
    [ "checkOrthogonal", "d1/df3/checkPacketDirection_8m.html#a26eb2fb85be8be66f680917de9459e01", null ],
    [ "checkOrthogonal", "d1/df3/checkPacketDirection_8m.html#aab2f273f13cca89f2466d9bb271ee923", null ],
    [ "checkOrthogonal", "d1/df3/checkPacketDirection_8m.html#a8776c90b79e612ae48d9390f15ae1815", null ],
    [ "checkUnitVector", "d1/df3/checkPacketDirection_8m.html#a3cbd797e5656ea0d09489cfcc4934f27", null ],
    [ "checkUnitVector", "d1/df3/checkPacketDirection_8m.html#af28de73b574da015b30cc6f69287b56f", null ],
    [ "checkUnitVector", "d1/df3/checkPacketDirection_8m.html#a0176697287995bd2d079b2296e183335", null ],
    [ "hist", "d1/df3/checkPacketDirection_8m.html#a10283f98b25fb0d1c8d0a410d2b7b606", null ],
    [ "hist", "d1/df3/checkPacketDirection_8m.html#ab7ad55c51c7ca4cb3c894333b37908eb", null ],
    [ "plot3", "d1/df3/checkPacketDirection_8m.html#ac933447fc4f35250375e57b7a46c6209", null ],
    [ "printf", "d1/df3/checkPacketDirection_8m.html#a14a8eb3f1f8bd99508e0cb0c6dee0b32", null ],
    [ "printf", "d1/df3/checkPacketDirection_8m.html#a7a3944f57e388358e8d625690fbe319a", null ],
    [ "title", "d1/df3/checkPacketDirection_8m.html#a0366f31e1a089fa43c757389b9aeaba2", null ],
    [ "title", "d1/df3/checkPacketDirection_8m.html#a31740e75a084b494a1b2b2c624c7d042", null ],
    [ "title", "d1/df3/checkPacketDirection_8m.html#a10298070f17740fd0438e24946745dbe", null ],
    [ "figure", "d1/df3/checkPacketDirection_8m.html#a391e34f2de441d79152a7b3d6e4c9c86", null ]
];