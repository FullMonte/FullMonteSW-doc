var classAbstractScorer =
[
    [ "~AbstractScorer", "d1/d56/classAbstractScorer.html#a0c2761a7e318024455282a3e0abd0e3e", null ],
    [ "AbstractScorer", "d1/d56/classAbstractScorer.html#a53f639673f315b30262699c5084f84fb", null ],
    [ "clear", "d1/d56/classAbstractScorer.html#ae975e7cfe4c30c68c68cb9f02c345363", null ],
    [ "getDimension", "d1/d56/classAbstractScorer.html#ada4b91623290d0d03f72bee7b9636859", null ],
    [ "getUnit", "d1/d56/classAbstractScorer.html#a68b67b672359abd0cf20d44e7222d5a4", null ],
    [ "postResults", "d1/d56/classAbstractScorer.html#a373bc719793e8488f79f7d6a8ea490b6", null ],
    [ "prepare", "d1/d56/classAbstractScorer.html#a3e4f293a46ce7339cc7e9c19fd8c049a", null ],
    [ "setDimension", "d1/d56/classAbstractScorer.html#a2e9d97250736670a7906b8621a23e6db", null ],
    [ "setUnit", "d1/d56/classAbstractScorer.html#af177e3baf42b5bb492c42aa4ecd8558b", null ],
    [ "m_dim", "d1/d56/classAbstractScorer.html#ac802fc96a054116297323f2091570a38", null ],
    [ "m_unit", "d1/d56/classAbstractScorer.html#abb3ac7c3f2c513dace4afa92ddc2c462", null ]
];