var pslse_2libcxl_2libcxl__internal_8h =
[
    [ "int_req", "da/d72/structint__req.html", "da/d72/structint__req" ],
    [ "open_req", "de/da9/structopen__req.html", "de/da9/structopen__req" ],
    [ "attach_req", "de/d48/structattach__req.html", "de/d48/structattach__req" ],
    [ "mmio_req", "d2/d16/structmmio__req.html", "d2/d16/structmmio__req" ],
    [ "cxl_afu_h", "d1/d5b/structcxl__afu__h.html", "d1/d5b/structcxl__afu__h" ],
    [ "cxl_adapter_h", "d6/ddd/structcxl__adapter__h.html", "d6/ddd/structcxl__adapter__h" ],
    [ "EVENT_QUEUE_MAX", "d1/daa/pslse_2libcxl_2libcxl__internal_8h.html#a77253a53d2e91766f7613b02870b5d5f", null ],
    [ "libcxl_req_state", "d1/daa/pslse_2libcxl_2libcxl__internal_8h.html#abeff1f54fbeb471c6fb6f9ea9857a76a", [
      [ "LIBCXL_REQ_IDLE", "d1/daa/pslse_2libcxl_2libcxl__internal_8h.html#abeff1f54fbeb471c6fb6f9ea9857a76aa93205ed44a059e6f788ac9a6405c25ee", null ],
      [ "LIBCXL_REQ_REQUEST", "d1/daa/pslse_2libcxl_2libcxl__internal_8h.html#abeff1f54fbeb471c6fb6f9ea9857a76aab93343837e7c8c444e945f84e9ee4c98", null ],
      [ "LIBCXL_REQ_PENDING", "d1/daa/pslse_2libcxl_2libcxl__internal_8h.html#abeff1f54fbeb471c6fb6f9ea9857a76aa50c2c70f82eebb8f473cc99647f26cc4", null ]
    ] ]
];