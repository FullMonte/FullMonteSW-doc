var structPhotonStruct =
[
    [ "dead", "structPhotonStruct.html#a8ff975db41b02281e6f8587270adf901", null ],
    [ "layer", "structPhotonStruct.html#ace9bfc48044dc8121ead711354b8de03", null ],
    [ "s", "structPhotonStruct.html#ab2ce8de18a649347efb50ac93bc40dff", null ],
    [ "sleft", "structPhotonStruct.html#aada73bf1b707a6fe82e38d97ac6ca42d", null ],
    [ "ux", "structPhotonStruct.html#a110da9c89fad68379f9e925d387c2f17", null ],
    [ "uy", "structPhotonStruct.html#a3e6e861c0a8019b6553077dd75e8d503", null ],
    [ "uz", "structPhotonStruct.html#aebb779d563bf8b77f67849295202eb78", null ],
    [ "w", "structPhotonStruct.html#aa9b62a68d9cbd50e04e8850f25b67904", null ],
    [ "x", "structPhotonStruct.html#acb83226d76591baa09c9b6fa492a5ebb", null ],
    [ "y", "structPhotonStruct.html#a91f783591b8d73265d3428bf4e2b49fd", null ],
    [ "z", "structPhotonStruct.html#ab1d31402ecef1e358619657a1fc31df5", null ]
];