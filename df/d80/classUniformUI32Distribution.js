var classUniformUI32Distribution =
[
    [ "input_type", "df/d80/classUniformUI32Distribution.html#a07eecc4a6619f1362b322b29b8bdaa3d", null ],
    [ "result_type", "df/d80/classUniformUI32Distribution.html#a49fbb5e38b81e8d151e74686d2364374", null ],
    [ "calculate", "df/d80/classUniformUI32Distribution.html#a42f2a1d1455da2763c73260271d30863", null ],
    [ "InputBlockSize", "df/d80/classUniformUI32Distribution.html#a805a9d6959a5337a2dc59b24d09124cb", null ],
    [ "OutputElementSize", "df/d80/classUniformUI32Distribution.html#add6a13464bf4b1e7bd35faf6b3a7e36a", null ],
    [ "OutputsPerInputBlock", "df/d80/classUniformUI32Distribution.html#aa4ad0100f869b3ad99550443cf294d6d", null ]
];