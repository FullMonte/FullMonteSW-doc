var classIntersectionChecker =
[
    [ "IntersectionChecker", "df/dba/classIntersectionChecker.html#a45936cce7d6288fd58f219e6cf2871e1", null ],
    [ "check", "df/dba/classIntersectionChecker.html#a5a127262edf50bf34c62ecf2420ae08c", null ],
    [ "checkStats", "df/dba/classIntersectionChecker.html#a2a349e7dcaf7218e687515f8867857e8", null ],
    [ "clear", "df/dba/classIntersectionChecker.html#adc81da29549a34060492ec6d8e066045", null ],
    [ "m_epsCosTheta", "df/dba/classIntersectionChecker.html#afa43ae0bd131419101bc8d79ca52a9da", null ],
    [ "m_epsFaceDistance", "df/dba/classIntersectionChecker.html#aba8952fde62b018e833513408fe49413", null ],
    [ "m_epsHeight", "df/dba/classIntersectionChecker.html#a9285a5066525cf1327168dbe1d754d7d", null ],
    [ "m_fixture", "df/dba/classIntersectionChecker.html#a9be4c3a43ae4f131b382414052b42c8a", null ],
    [ "m_verbose", "df/dba/classIntersectionChecker.html#afbbd22d95561f4e39ee6d19f819f608a", null ]
];