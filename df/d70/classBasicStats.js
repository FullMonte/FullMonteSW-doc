var classBasicStats =
[
    [ "BasicStats", "df/d70/classBasicStats.html#a58ad53ccb4fe8cdff36c6cc7b2ca9206", null ],
    [ "~BasicStats", "df/d70/classBasicStats.html#aea167c819d68e813b27d576e1b3d1d18", null ],
    [ "clear", "df/d70/classBasicStats.html#accd687303635df9801a03f605cc6d1ed", null ],
    [ "cv", "df/d70/classBasicStats.html#ac821907e4cd30d99004d2b7815c6cc16", null ],
    [ "doUpdate", "df/d70/classBasicStats.html#a4b0856033540eee742fe1bed06a1e8ca", null ],
    [ "max", "df/d70/classBasicStats.html#aef1854e00c80845bfeac3710cc3b8bb3", null ],
    [ "mean", "df/d70/classBasicStats.html#aebcfc66c429460eebb21a4548ade328f", null ],
    [ "min", "df/d70/classBasicStats.html#a04f31e84ac2958c1267cbc53f2be48dd", null ],
    [ "nnz", "df/d70/classBasicStats.html#a4566b2a18b30d4ed2863c702a43c7cc3", null ],
    [ "stddev", "df/d70/classBasicStats.html#abb2d720be9ec5eae5fd706f3c653444c", null ],
    [ "sum", "df/d70/classBasicStats.html#ae1d9e585565afa6fb455143b61ef637b", null ],
    [ "variance", "df/d70/classBasicStats.html#a9f91abce37d84444d6f7d83fa088fbd8", null ],
    [ "m_max", "df/d70/classBasicStats.html#a88b9973abcdd7de7b07bb39b913bcd9f", null ],
    [ "m_min", "df/d70/classBasicStats.html#a30b8971fc3a71a4599e4b13ebf0678d0", null ],
    [ "m_N", "df/d70/classBasicStats.html#ace2728437e45da0a818c61a0ae20c7e8", null ],
    [ "m_nnz", "df/d70/classBasicStats.html#a2fa79ee159bc133d5356fdd3c86ae27a", null ],
    [ "m_sum_x", "df/d70/classBasicStats.html#aa0c085248088dd446166d9323ad2ed65", null ],
    [ "m_sum_xx", "df/d70/classBasicStats.html#a38d074bf09121cc98846c8fd9876c987", null ]
];