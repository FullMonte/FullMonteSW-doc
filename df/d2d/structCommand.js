var structCommand =
[
    [ "CommandStates", "df/d2d/structCommand.html#a17fcb64cf70783420bade9d81775575a", [
      [ "IDLE", "df/d2d/structCommand.html#a17fcb64cf70783420bade9d81775575aae48e6920ace64e47d1255c1a6406d03c", null ],
      [ "WAITING_RESPONSE", "df/d2d/structCommand.html#a17fcb64cf70783420bade9d81775575aa28de5808d0b578180ebaec37f0582e0c", null ],
      [ "WAITING_DATA", "df/d2d/structCommand.html#a17fcb64cf70783420bade9d81775575aaa48e711bf93570476fb3e6a1d00f1250", null ],
      [ "WAITING_READ", "df/d2d/structCommand.html#a17fcb64cf70783420bade9d81775575aaef09a8bb20b69f367910b1f53b148224", null ]
    ] ],
    [ "Command", "df/d2d/structCommand.html#a16d91c1cea2409717a3a3bf874aae180", null ],
    [ "~ Command", "df/d2d/structCommand.html#a853ed809dc5a1af044731a8ddcecac54", null ],
    [ "get_tag", "df/d2d/structCommand.html#acaaac4eba3a3569201e70c183f22d7e8", null ],
    [ "is_completed", "df/d2d/structCommand.html#a78ad8590d94004ecb150409de476e671", null ],
    [ "is_restart", "df/d2d/structCommand.html#af50b2ff7eed8db375469db77ac5175c4", null ],
    [ "process_command", "df/d2d/structCommand.html#a2dd7075a5b675085b968081139468ece", null ],
    [ "send_command", "df/d2d/structCommand.html#a9f36089da616f11b2f2e26d7c26d55f4", null ],
    [ "addr", "df/d2d/structCommand.html#a005a8bfdf00623ca5a929f2ffc6f35b8", null ],
    [ "buffer_read_parity", "df/d2d/structCommand.html#aa8d1966e8acbda58cb8be024dcb7b7a2", null ],
    [ "code", "df/d2d/structCommand.html#a920e0bacf6e26530018af948efc1ff96", null ],
    [ "command_address_parity", "df/d2d/structCommand.html#adfa37aa4949dda6453bbf1b73fea8ed3", null ],
    [ "command_code_parity", "df/d2d/structCommand.html#a1b884d606f11980f70089e15491c165a", null ],
    [ "command_tag_parity", "df/d2d/structCommand.html#af55d3916d696d1c3bf3207fb194b5cf4", null ],
    [ "completed", "df/d2d/structCommand.html#a818cb9353516148222655d311dedf413", null ],
    [ "data", "df/d2d/structCommand.html#ab8889a12c3b9fb1ad14733054b60d621", null ],
    [ "state", "df/d2d/structCommand.html#a0f733ada7222df7bce6ee6de35ad60fe", null ],
    [ "tag", "df/d2d/structCommand.html#a2a4bf97e98267a21c7ba8c98919b4662", null ],
    [ "time", "df/d2d/structCommand.html#aef9534525abb078b74a35e1e16d5135f", null ],
    [ "write", "df/d2d/structCommand.html#a69af0ef0aeb733b7b58a0f2396c401e4", null ]
];