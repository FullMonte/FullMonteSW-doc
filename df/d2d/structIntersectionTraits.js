var structIntersectionTraits =
[
    [ "input_type", "d9/d9f/structIntersectionTraits_1_1input__type.html", "d9/d9f/structIntersectionTraits_1_1input__type" ],
    [ "output_type", "d6/d49/structIntersectionTraits_1_1output__type.html", "d6/d49/structIntersectionTraits_1_1output__type" ],
    [ "packed_output_type", "d2/d66/structIntersectionTraits_1_1packed__output__type.html", "d2/d66/structIntersectionTraits_1_1packed__output__type" ],
    [ "input_container_type", "df/d2d/structIntersectionTraits.html#ab6519b917d2ce4c819c28de558fe863c", null ],
    [ "output_container_type", "df/d2d/structIntersectionTraits.html#aa054975ce0425bfb54a96bcc1132c634", null ],
    [ "packed_input_type", "df/d2d/structIntersectionTraits.html#af759f6dae915a9666066d07b5d22cafe", null ],
    [ "convertToNativeType", "df/d2d/structIntersectionTraits.html#abde6c0157b0cf31e24fd6727c531f647", null ],
    [ "convertToNativeType", "df/d2d/structIntersectionTraits.html#a308faa0337a628de9081d4958be2339f", null ],
    [ "convertToPackedType", "df/d2d/structIntersectionTraits.html#a05b879d147a017f12c27c75e8ae3e2c9", null ],
    [ "input_bits", "df/d2d/structIntersectionTraits.html#a457d3259af42a8128daf59a1b6407e56", null ],
    [ "output_bits", "df/d2d/structIntersectionTraits.html#a5be894c1f1c5a5da065bb60d201ad522", null ]
];