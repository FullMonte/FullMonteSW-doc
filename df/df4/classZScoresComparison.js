var classZScoresComparison =
[
    [ "ZScoresComparison", "df/df4/classZScoresComparison.html#a6f554f4fb86b4afed9835167f6f63299", null ],
    [ "~ZScoresComparison", "df/df4/classZScoresComparison.html#abcf74cbcab985c5baef8882ae0c2777f", null ],
    [ "outputZ", "df/df4/classZScoresComparison.html#aacd9fa7d438abeb619359086d0c2d980", null ],
    [ "referenceMean", "df/df4/classZScoresComparison.html#a10510654a612c4aa9c6ea329b03e91d1", null ],
    [ "referenceStdDev", "df/df4/classZScoresComparison.html#ae268623cab0998ec69c496e8e1d7eff6", null ],
    [ "testInput", "df/df4/classZScoresComparison.html#a5caa4885c76798e6020f571dd91fbfa5", null ],
    [ "update", "df/df4/classZScoresComparison.html#a372813276aeb11cd228e7ac789b9e2be", null ],
    [ "m_input", "df/df4/classZScoresComparison.html#a1231908040a8fe846285ab1cc02c4483", null ],
    [ "m_referenceMean", "df/df4/classZScoresComparison.html#a54d9ab38db223b0cdf29fce2f64f2a5e", null ],
    [ "m_referenceStdDev", "df/df4/classZScoresComparison.html#afe019e7d9aaef700bac6ae1045e9b3a3", null ],
    [ "m_zScore", "df/df4/classZScoresComparison.html#a0159555c7fa33a63de16363db85f3552", null ]
];