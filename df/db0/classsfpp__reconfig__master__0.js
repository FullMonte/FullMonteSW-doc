var classsfpp__reconfig__master__0 =
[
    [ "sfpp_reconfig_master_0.rtl", "d9/d51/classsfpp__reconfig__master__0_1_1rtl.html", "d9/d51/classsfpp__reconfig__master__0_1_1rtl" ],
    [ "clk_clk", "df/db0/classsfpp__reconfig__master__0.html#a4fe3c97be6291ee36adbef2eb01181c8", null ],
    [ "clk_reset_reset", "df/db0/classsfpp__reconfig__master__0.html#ae480b15c974698c9f98d4729e48ba1ad", null ],
    [ "FIFO_DEPTHS", "df/db0/classsfpp__reconfig__master__0.html#a76f35f7814b67973ef6c09c4096f51a3", null ],
    [ "IEEE", "df/db0/classsfpp__reconfig__master__0.html#a7cbee7690867fab53d191d0e1934bd3e", null ],
    [ "master_address", "df/db0/classsfpp__reconfig__master__0.html#ab1966e42a81b06c9468e1f94bbd237bf", null ],
    [ "master_byteenable", "df/db0/classsfpp__reconfig__master__0.html#a84b9f3273f933997420853ef18b99313", null ],
    [ "master_read", "df/db0/classsfpp__reconfig__master__0.html#a96cce02de1503d9cb6705f562ab1a5f9", null ],
    [ "master_readdata", "df/db0/classsfpp__reconfig__master__0.html#ad15d3130a2be3f9177e15ba20989da91", null ],
    [ "master_readdatavalid", "df/db0/classsfpp__reconfig__master__0.html#a8f3ee1f3deb9f80762f0ed215904d765", null ],
    [ "master_reset_reset", "df/db0/classsfpp__reconfig__master__0.html#a58e4005dd58071e9d4a9dd7ce5134d29", null ],
    [ "master_waitrequest", "df/db0/classsfpp__reconfig__master__0.html#add5c981a61929c44950c465599755b82", null ],
    [ "master_write", "df/db0/classsfpp__reconfig__master__0.html#ab9e76620f011c8db7a6a2fe1f4350daf", null ],
    [ "master_writedata", "df/db0/classsfpp__reconfig__master__0.html#a9421bf2a43cce8b6f2774a8fd4533b85", null ],
    [ "numeric_std", "df/db0/classsfpp__reconfig__master__0.html#a431939a7ec8de1c344cc841b5ce70d1e", null ],
    [ "PLI_PORT", "df/db0/classsfpp__reconfig__master__0.html#a0efccb9b240523a6c2caca11b50d403f", null ],
    [ "std_logic_1164", "df/db0/classsfpp__reconfig__master__0.html#a95c807f07d517e1e54bd70123a79df95", null ],
    [ "USE_PLI", "df/db0/classsfpp__reconfig__master__0.html#a156f5f7e738337f3abd2e6552084cc06", null ]
];