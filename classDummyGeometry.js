var classDummyGeometry =
[
    [ "DummyGeometry", "classDummyGeometry.html#aecc6e75794a4c505635f3af790a143c9", null ],
    [ "~DummyGeometry", "classDummyGeometry.html#ace6a42bd382f966e0538ccb34389e07c", null ],
    [ "directedSurfaceAreas", "classDummyGeometry.html#a6239c379e09ba6bc0bdc4d2700e14cf9", null ],
    [ "elementVolumes", "classDummyGeometry.html#ae35c30b5665d02080197f2f8e6a804ec", null ],
    [ "surfaceAreas", "classDummyGeometry.html#acbf40f84845cbb46017a5a8184c03b51", null ]
];