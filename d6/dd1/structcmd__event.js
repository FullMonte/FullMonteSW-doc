var structcmd__event =
[
    [ "_next", "d6/dd1/structcmd__event.html#ae61beab7ef7eb94078d20da692a220e3", null ],
    [ "abort", "d6/dd1/structcmd__event.html#aa0c8e89ddc8898b0c8ac8c27bb7304c5", null ],
    [ "abt", "d6/dd1/structcmd__event.html#a65d938fdf46a6667e3ad339e7b53adfe", null ],
    [ "addr", "d6/dd1/structcmd__event.html#a1afc0045c709c0487f10f8bbcb63bbb8", null ],
    [ "buffer_activity", "d6/dd1/structcmd__event.html#a9b21438a86c976654eaf70e55ca547f9", null ],
    [ "client_state", "d6/dd1/structcmd__event.html#a0f3cb579661aae95d3d6ce7cd090e871", null ],
    [ "command", "d6/dd1/structcmd__event.html#a51e719da926d3e1f45c32d7ba98e3fd3", null ],
    [ "context", "d6/dd1/structcmd__event.html#a44775388f416047da27efb69383fbec1", null ],
    [ "data", "d6/dd1/structcmd__event.html#a32825f46dc74dc69e8579faabaeb0fe4", null ],
    [ "parity", "d6/dd1/structcmd__event.html#a6636e134c3f6ec5303dcbe1077ba6e48", null ],
    [ "resp", "d6/dd1/structcmd__event.html#ac19ae4ecf1338358c7dddee1169eff8e", null ],
    [ "size", "d6/dd1/structcmd__event.html#a41ae03a824c5a1efa535a99dd8c0f5fb", null ],
    [ "state", "d6/dd1/structcmd__event.html#a0f307ca3c63e385574ff1a1ee536edc6", null ],
    [ "tag", "d6/dd1/structcmd__event.html#a85b97a50c1d2eb030960b720234c5545", null ],
    [ "type", "d6/dd1/structcmd__event.html#a13da39c4b78ad8c56495311db939b2b7", null ],
    [ "unlock", "d6/dd1/structcmd__event.html#ad01ba31e91e9c888d9d05f56f12287e8", null ]
];