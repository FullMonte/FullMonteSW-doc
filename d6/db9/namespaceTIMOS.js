var namespaceTIMOS =
[
    [ "FaceSource", "d3/d70/structTIMOS_1_1FaceSource.html", "d3/d70/structTIMOS_1_1FaceSource" ],
    [ "GenericSource", "d8/dcf/structTIMOS_1_1GenericSource.html", "d8/dcf/structTIMOS_1_1GenericSource" ],
    [ "Material", "da/d18/structTIMOS_1_1Material.html", "da/d18/structTIMOS_1_1Material" ],
    [ "Optical", "da/d04/structTIMOS_1_1Optical.html", "da/d04/structTIMOS_1_1Optical" ],
    [ "PencilBeamSource", "d5/dbd/structTIMOS_1_1PencilBeamSource.html", "d5/dbd/structTIMOS_1_1PencilBeamSource" ],
    [ "PointSource", "d8/dd9/structTIMOS_1_1PointSource.html", "d8/dd9/structTIMOS_1_1PointSource" ],
    [ "TetraSource", "d5/dd7/structTIMOS_1_1TetraSource.html", "d5/dd7/structTIMOS_1_1TetraSource" ]
];