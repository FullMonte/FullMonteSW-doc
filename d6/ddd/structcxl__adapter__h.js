var structcxl__adapter__h =
[
    [ "_head", "d6/ddd/structcxl__adapter__h.html#aec0949b43767ae121a0714c495839865", null ],
    [ "_next", "d6/ddd/structcxl__adapter__h.html#a4b446a652d1c6a20b9a4b4d15772b177", null ],
    [ "afu_list", "d6/ddd/structcxl__adapter__h.html#a9306aa0efab2b44fe7ee74d0ab29e3b5", null ],
    [ "caia_major", "d6/ddd/structcxl__adapter__h.html#ac5465c1dc04179a2a55b6e392670a6ed", null ],
    [ "caia_minor", "d6/ddd/structcxl__adapter__h.html#a307487b806ddfae46c1f9d1ba87014b0", null ],
    [ "enum_dir", "d6/ddd/structcxl__adapter__h.html#af4c3e52959a4827fe8eaf0b89be2aa50", null ],
    [ "enum_ent", "d6/ddd/structcxl__adapter__h.html#a833fba639d147ea4ab0c64b3ced1cb43", null ],
    [ "fd", "d6/ddd/structcxl__adapter__h.html#afba397931bb7d39633048221e576e284", null ],
    [ "id", "d6/ddd/structcxl__adapter__h.html#a28dd5b0b1c2d24438caef05726fb1e2b", null ],
    [ "map", "d6/ddd/structcxl__adapter__h.html#a76b97da005d443a32c7aece9930122b2", null ],
    [ "mask", "d6/ddd/structcxl__adapter__h.html#ada98e910fbecc5ab7b5f07f9efd0fdb0", null ],
    [ "position", "d6/ddd/structcxl__adapter__h.html#a619253deefcf93ae6e70d7e31d297bf9", null ],
    [ "pslse_version", "d6/ddd/structcxl__adapter__h.html#ab04137a528b5b9e6685c7286d335f1f9", null ],
    [ "sysfs_path", "d6/ddd/structcxl__adapter__h.html#a4b8a536a4301ac783821d4316145e4aa", null ]
];