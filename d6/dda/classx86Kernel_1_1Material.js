var classx86Kernel_1_1Material =
[
    [ "Material", "d6/dda/classx86Kernel_1_1Material.html#a137e987401b63eb7c6c27c3e38bc74b5", null ],
    [ "Material", "d6/dda/classx86Kernel_1_1Material.html#aa8262c7da4cba0ad0bb9c1b7db0f45a4", null ],
    [ "Material", "d6/dda/classx86Kernel_1_1Material.html#a891c322f71da50c6f11b747d0b14eabb", null ],
    [ "absfrac", "d6/dda/classx86Kernel_1_1Material.html#a62e2bcac355fbca549557eddce4d0c98", null ],
    [ "c0", "d6/dda/classx86Kernel_1_1Material.html#a0a5b178032984b11282511ba39eaf64c", null ],
    [ "m_init", "d6/dda/classx86Kernel_1_1Material.html#a7ff851218c22ba9289bb5c93d6566e91", null ],
    [ "m_prop", "d6/dda/classx86Kernel_1_1Material.html#a6afdb3a333d01c2dc76afb9dba714d8e", null ],
    [ "muT", "d6/dda/classx86Kernel_1_1Material.html#a4ba3644cf35d63c708a630a96dd88988", null ],
    [ "n", "d6/dda/classx86Kernel_1_1Material.html#a4abaa9af15eb52c93131af79f939d4ac", null ],
    [ "scatters", "d6/dda/classx86Kernel_1_1Material.html#a7c3c52a1aacfc8196d9aecf34ef6e48a", null ]
];