var structMemLoadWED =
[
    [ "MemLoadWED", "d6/da2/structMemLoadWED.html#a986a4d999de840190025d312110025d5", null ],
    [ "dst", "d6/da2/structMemLoadWED.html#a01622be51e9d049944f5c91789df9550", null ],
    [ "iSize", "d6/da2/structMemLoadWED.html#a95fd2aebec54e970b3dd074f67bc8843", null ],
    [ "oSize", "d6/da2/structMemLoadWED.html#aa6f0450398cacd755fb974ce34d76ed9", null ],
    [ "pad", "d6/da2/structMemLoadWED.html#ac0961421dddbeef17a5f1f377342056d", null ],
    [ "pDst", "d6/da2/structMemLoadWED.html#aff040187ec369359b457f97fed05b32d", null ],
    [ "pSrc", "d6/da2/structMemLoadWED.html#a2b2bebd30ae32beb35bd70475c7ad852", null ],
    [ "resv", "d6/da2/structMemLoadWED.html#a0401a3ba5db49fb02f80dfc63c4a3520", null ],
    [ "size", "d6/da2/structMemLoadWED.html#a9869535c9a999e718639fb4718aa0183", null ],
    [ "src", "d6/da2/structMemLoadWED.html#a0811a7503cd301b920e1410e6465a732", null ]
];