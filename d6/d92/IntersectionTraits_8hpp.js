var IntersectionTraits_8hpp =
[
    [ "GenericTetra", "de/d2a/structGenericTetra.html", "de/d2a/structGenericTetra" ],
    [ "IntersectionTraits", "df/d2d/structIntersectionTraits.html", "df/d2d/structIntersectionTraits" ],
    [ "input_type", "d9/d9f/structIntersectionTraits_1_1input__type.html", "d9/d9f/structIntersectionTraits_1_1input__type" ],
    [ "output_type", "d6/d49/structIntersectionTraits_1_1output__type.html", "d6/d49/structIntersectionTraits_1_1output__type" ],
    [ "packed_output_type", "d2/d66/structIntersectionTraits_1_1packed__output__type.html", "d2/d66/structIntersectionTraits_1_1packed__output__type" ],
    [ "Kernel3f", "d6/d92/IntersectionTraits_8hpp.html#a3bcb1ecbc9e73118d59ed9d50bf92f47", null ]
];