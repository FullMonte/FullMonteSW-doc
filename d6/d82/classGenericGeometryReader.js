var classGenericGeometryReader =
[
    [ "GenericGeometryReader", "d6/d82/classGenericGeometryReader.html#a858bfcf241678db73419a482fc0c9a41", null ],
    [ "~GenericGeometryReader", "d6/d82/classGenericGeometryReader.html#a5e1703f7b82339a6efd54286229c4992", null ],
    [ "filename", "d6/d82/classGenericGeometryReader.html#aae437e98e29cdf74dfd1b5eebb813131", null ],
    [ "geometry", "d6/d82/classGenericGeometryReader.html#a3b2299d184df43aff73f12aa9c6d277b", null ],
    [ "read", "d6/d82/classGenericGeometryReader.html#af2d88ee55cb42dc92e802f30980c09b1", null ],
    [ "split_at_last", "d6/d82/classGenericGeometryReader.html#aae7d50f73d05c6cc0ea0df9295b60a39", null ],
    [ "m_filename", "d6/d82/classGenericGeometryReader.html#a29352697b9e35fd6814c23c79c5313a5", null ],
    [ "m_geometry", "d6/d82/classGenericGeometryReader.html#ad4452a1752355921020ccd5a89e123aa", null ]
];