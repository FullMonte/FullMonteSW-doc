var classVector =
[
    [ "Vector", "d6/da1/classVector.html#aac8bc4cd6cea86d69d49aab2b8a8dc5a", null ],
    [ "Vector", "d6/da1/classVector.html#a8fc3cf4e3e611705ca2ae36b8c947469", null ],
    [ "Vector", "d6/da1/classVector.html#a3e109ed1d19bb07a766627cd0488ff12", null ],
    [ "Vector", "d6/da1/classVector.html#ad0af5b0fe35426a508f5d0bc46cf59f3", null ],
    [ "dot", "d6/da1/classVector.html#a449ea04d9ea7d787a1c3155d0aba75f7", null ],
    [ "norm2_l2", "d6/da1/classVector.html#a20cee2a45c2f8486157041b0afa6322f", null ],
    [ "norm_l1", "d6/da1/classVector.html#a786abf794770ce5be99f098584fdeb0d", null ],
    [ "norm_l2", "d6/da1/classVector.html#a73feea8b6f22bd935ee9dda39b8e9de1", null ],
    [ "operator*", "d6/da1/classVector.html#af9b4afe1d09e8d65cee788c5b4b34327", null ],
    [ "operator*=", "d6/da1/classVector.html#a75025f3b413de39e32454dbc3f6af556", null ],
    [ "operator+=", "d6/da1/classVector.html#a5fd41a4fdeb32def32c3463373ee0dc5", null ],
    [ "operator-", "d6/da1/classVector.html#addae2d288cdf48331a17b2e0c240a490", null ],
    [ "operator-=", "d6/da1/classVector.html#ab3e58f4b9bfc78e4124622c66af32415", null ],
    [ "operator/", "d6/da1/classVector.html#ae20ba22154de297faa4d7bf3730370da", null ],
    [ "operator/=", "d6/da1/classVector.html#a4138f918c439238f3f8c5891e303f63b", null ]
];