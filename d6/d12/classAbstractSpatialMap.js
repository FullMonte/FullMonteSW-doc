var classAbstractSpatialMap =
[
    [ "SpaceType", "d6/d12/classAbstractSpatialMap.html#a1ee0d0fc61533fd02c5b0c6f737ead8b", [
      [ "UnknownSpaceType", "d6/d12/classAbstractSpatialMap.html#a1ee0d0fc61533fd02c5b0c6f737ead8ba843786843e32de5658937c5de813ae09", null ],
      [ "Point", "d6/d12/classAbstractSpatialMap.html#a1ee0d0fc61533fd02c5b0c6f737ead8ba06482732ce921d120d4c3a4e9498ba2d", null ],
      [ "Line", "d6/d12/classAbstractSpatialMap.html#a1ee0d0fc61533fd02c5b0c6f737ead8bacb58be9a8fe2f08f810b52fc93c948c8", null ],
      [ "Surface", "d6/d12/classAbstractSpatialMap.html#a1ee0d0fc61533fd02c5b0c6f737ead8bae6756ed7cb0eeacf58a141a5aadf0cf7", null ],
      [ "Volume", "d6/d12/classAbstractSpatialMap.html#a1ee0d0fc61533fd02c5b0c6f737ead8bae6dfe4d77deef4eafa9bc114eb994cc2", null ],
      [ "DirectedSurface", "d6/d12/classAbstractSpatialMap.html#a1ee0d0fc61533fd02c5b0c6f737ead8ba63840398071489e4605dbce1192ab5d9", null ]
    ] ],
    [ "ValueType", "d6/d12/classAbstractSpatialMap.html#a0bc128e855a84bad42c6587707074c57", [
      [ "Scalar", "d6/d12/classAbstractSpatialMap.html#a0bc128e855a84bad42c6587707074c57a50bba1fbbaa37f6215d8dac888ccbf59", null ],
      [ "Vector", "d6/d12/classAbstractSpatialMap.html#a0bc128e855a84bad42c6587707074c57a82ebcbfbfca86ed9409ed07720bfbe2b", null ],
      [ "UnknownValueType", "d6/d12/classAbstractSpatialMap.html#a0bc128e855a84bad42c6587707074c57aa9bb69977dee37e28acc92b0404c7ab6", null ]
    ] ],
    [ "AbstractSpatialMap", "d6/d12/classAbstractSpatialMap.html#ae9d98ea97107081b8dd5144b5c660de5", null ],
    [ "~AbstractSpatialMap", "d6/d12/classAbstractSpatialMap.html#a75de0c6dcf52f3638611fc0bf7ed298a", null ],
    [ "dim", "d6/d12/classAbstractSpatialMap.html#ab917b270300390062b972cf40e8669ae", null ],
    [ "dim", "d6/d12/classAbstractSpatialMap.html#a26a3b2502e120c1a85bd90d7e806fe14", null ],
    [ "spatialType", "d6/d12/classAbstractSpatialMap.html#a7286afc5069f97757edd917b79a20fc8", null ],
    [ "spatialType", "d6/d12/classAbstractSpatialMap.html#a73cf0d2608ba19e3fb093a2568827c71", null ],
    [ "type", "d6/d12/classAbstractSpatialMap.html#a2d681305fd5312456d5d5c2018a09594", null ],
    [ "valueType", "d6/d12/classAbstractSpatialMap.html#a052ec3711148196a46bf84bd946fea46", null ],
    [ "valueType", "d6/d12/classAbstractSpatialMap.html#a7db3947dbfec00bdf476359bf75ac3ec", null ],
    [ "m_spaceType", "d6/d12/classAbstractSpatialMap.html#acc3d350fe7e19611ca6dbb13b5d18954", null ],
    [ "m_valueType", "d6/d12/classAbstractSpatialMap.html#ac5867a4a834a1cbce84593b31686e33d", null ]
];