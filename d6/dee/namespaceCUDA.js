var namespaceCUDA =
[
    [ "FMALIGN", "d6/d75/structCUDA_1_1FMALIGN.html", "d6/d75/structCUDA_1_1FMALIGN" ],
    [ "LaunchPacket", "d0/dc4/structCUDA_1_1LaunchPacket.html", "d0/dc4/structCUDA_1_1LaunchPacket" ],
    [ "Material", "dd/de4/structCUDA_1_1Material.html", "dd/de4/structCUDA_1_1Material" ],
    [ "Packet", "d5/d37/structCUDA_1_1Packet.html", "d5/d37/structCUDA_1_1Packet" ],
    [ "PacketDirection", "d1/d24/structCUDA_1_1PacketDirection.html", "d1/d24/structCUDA_1_1PacketDirection" ],
    [ "StepResult", "d4/dcd/structCUDA_1_1StepResult.html", "d4/dcd/structCUDA_1_1StepResult" ]
];