var classRay =
[
    [ "Ray", "classRay.html#a9a669178dde5b0324edce26a766160de", null ],
    [ "Ray", "classRay.html#ad8b554273897e4e1a63cffb4c3c33baa", null ],
    [ "getDirection", "classRay.html#adeee6462af31cb49d648e548791ac1df", null ],
    [ "getOrigin", "classRay.html#aae057045c5e8c73229f4f609b34520aa", null ],
    [ "operator()", "classRay.html#a869c3320c34cb47cda76d49a83b74a2a", null ],
    [ "setOrigin", "classRay.html#a61bea2c39707de5665a8309c0a777986", null ]
];