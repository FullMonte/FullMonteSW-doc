var classDoseHistogram =
[
    [ "Element", "structDoseHistogram_1_1Element.html", "structDoseHistogram_1_1Element" ],
    [ "const_iterator", "classDoseHistogram.html#a2acb5a984b26b574da6877ed9e68aa9e", null ],
    [ "DoseHistogram", "classDoseHistogram.html#af18e7dc44f38da69fd58a1999184580d", null ],
    [ "DoseHistogram", "classDoseHistogram.html#ab560310498c5fd976095f49708b0ae8f", null ],
    [ "begin", "classDoseHistogram.html#ac428f909c4dfed2a26b664098028bded", null ],
    [ "dim", "classDoseHistogram.html#a27ca91d6172d3d5e98b46a95aea72743", null ],
    [ "doseAtPercentile", "classDoseHistogram.html#a06d8382d5a0fc19780b79b92670a17dc", null ],
    [ "end", "classDoseHistogram.html#ab003bf27269a665c197d89800d2e5b7c", null ],
    [ "percentileOfDose", "classDoseHistogram.html#a36904bb22bd084c0cfe1c3704c6f09ed", null ],
    [ "totalMeasure", "classDoseHistogram.html#a2cef44041a133da6e52674b58051cbc6", null ],
    [ "type", "classDoseHistogram.html#a03fb8d2469419a6ac7adaf908db89152", null ]
];