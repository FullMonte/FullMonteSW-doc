var classChi2Similarity =
[
    [ "Chi2Entry", "structChi2Similarity_1_1Chi2Entry.html", "structChi2Similarity_1_1Chi2Entry" ],
    [ "Chi2Similarity", "classChi2Similarity.html#a34afd52cfbd52e7c0cc0609264aa575a", null ],
    [ "~Chi2Similarity", "classChi2Similarity.html#ae489855123b5f3e0c731f36fe669c0c3", null ],
    [ "extraCvToAdd", "classChi2Similarity.html#af2801f3912cf65d87a9d2b6a242d71ac", null ],
    [ "extraCvToAdd", "classChi2Similarity.html#a0505fe76ff4ce2217daa8614064e6575", null ],
    [ "extraStdDevMultiplier", "classChi2Similarity.html#a976bb065c04e804aab68b2199e5bb11c", null ],
    [ "extraStdDevMultiplier", "classChi2Similarity.html#a7f3ee1826e32bdd835082aabdd681db6", null ],
    [ "getByCVThreshold", "classChi2Similarity.html#ac63a9d79d64346bd4fb71784921cb317", null ],
    [ "getByEnergyFraction", "classChi2Similarity.html#a6844362cd176450d7aa6d7f46936c705", null ],
    [ "partition", "classChi2Similarity.html#a36ea2626bae5ca8d34b625e60a7bf9fd", null ],
    [ "reference", "classChi2Similarity.html#acc41b2a123a7555ea7c55be5b0d5923b", null ],
    [ "test", "classChi2Similarity.html#ae6ea74f449029cd2d984bc9af1b3e3ce", null ],
    [ "testPacketCount", "classChi2Similarity.html#afcef3e80fec82d30792419e5d73839a0", null ],
    [ "update", "classChi2Similarity.html#ab2a6633153f7b4eb5d66ba295a2843c1", null ]
];