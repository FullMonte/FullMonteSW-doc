var classDoseHistogramCollection =
[
    [ "DoseHistogramCollection", "classDoseHistogramCollection.html#aa82b26671241b573aa7e0eccdd35946c", null ],
    [ "DoseHistogramCollection", "classDoseHistogramCollection.html#a05039ce39a73c25c16537420dcfdfb21", null ],
    [ "~DoseHistogramCollection", "classDoseHistogramCollection.html#abe5e64eee86b9ebdca07d6556fb94f05", null ],
    [ "add", "classDoseHistogramCollection.html#a45efd8ffaabcd8842639e8f6dba32997", null ],
    [ "clone", "classDoseHistogramCollection.html#ade2cf93f556ad18d329d60cd044af392", null ],
    [ "count", "classDoseHistogramCollection.html#aaee22187ba25b9308645527a0d9138db", null ],
    [ "get", "classDoseHistogramCollection.html#a2e479878cc1fe11184392d2729ddc6dd", null ],
    [ "remove", "classDoseHistogramCollection.html#a94144d6e9cd9d8227be626b011a36686", null ],
    [ "type", "classDoseHistogramCollection.html#afaa09eec963a9214a4ce9570b970de94", null ]
];