var namespaceSSE =
[
    [ "Scalar", "classSSE_1_1Scalar.html", "classSSE_1_1Scalar" ],
    [ "SSEBase", "classSSE_1_1SSEBase.html", "classSSE_1_1SSEBase" ],
    [ "SSEKernel", "classSSE_1_1SSEKernel.html", "classSSE_1_1SSEKernel" ],
    [ "UnitVector", "classSSE_1_1UnitVector.html", "classSSE_1_1UnitVector" ],
    [ "Vector", "classSSE_1_1Vector.html", "classSSE_1_1Vector" ]
];