var dir_477c6f2613a421e86dad0c5c3ae3f7a1 =
[
    [ "ConeFixture.hpp", "ConeFixture_8hpp.html", [
      [ "ConeFixture", "structConeFixture.html", "structConeFixture" ]
    ] ],
    [ "DirectedFixture.hpp", "DirectedFixture_8hpp.html", [
      [ "DirectedFixture", "structDirectedFixture.html", "structDirectedFixture" ]
    ] ],
    [ "DiskFixture.hpp", "DiskFixture_8hpp.html", [
      [ "DiskFixture", "structDiskFixture.html", "structDiskFixture" ]
    ] ],
    [ "ImplicitPlane.hpp", "Kernels_2Software_2Emitters_2Test_2ImplicitPlane_8hpp.html", [
      [ "ImplicitPlane", "classImplicitPlane.html", "classImplicitPlane" ]
    ] ],
    [ "IsotropicFixture.hpp", "IsotropicFixture_8hpp.html", [
      [ "Polar", "structPolar.html", "structPolar" ],
      [ "IsotropicFixture", "structIsotropicFixture.html", "structIsotropicFixture" ]
    ] ],
    [ "LineFixture.hpp", "LineFixture_8hpp.html", [
      [ "LineFixture", "structLineFixture.html", "structLineFixture" ]
    ] ],
    [ "OrthogonalFixture.hpp", "OrthogonalFixture_8hpp.html", [
      [ "OrthogonalFixture", "structOrthogonalFixture.html", null ],
      [ "AbsoluteOrthogonalFixture", "structAbsoluteOrthogonalFixture.html", "structAbsoluteOrthogonalFixture" ]
    ] ],
    [ "PacketDirectionFixture.hpp", "PacketDirectionFixture_8hpp.html", [
      [ "PacketDirectionFixture", "structPacketDirectionFixture.html", "structPacketDirectionFixture" ]
    ] ],
    [ "PlaneFixture.hpp", "PlaneFixture_8hpp.html", [
      [ "PlaneFixture", "structPlaneFixture.html", "structPlaneFixture" ]
    ] ],
    [ "PointFixture.hpp", "PointFixture_8hpp.html", [
      [ "PointFixture", "structPointFixture.html", "structPointFixture" ]
    ] ],
    [ "SourceFixture.hpp", "SourceFixture_8hpp.html", [
      [ "SourceFixture", "structSourceFixture.html", "structSourceFixture" ]
    ] ],
    [ "Test_Emitters.cpp", "Test__Emitters_8cpp.html", "Test__Emitters_8cpp" ],
    [ "Test_FiberConeEmitter.cpp", "Test__FiberConeEmitter_8cpp.html", "Test__FiberConeEmitter_8cpp" ],
    [ "Tetra.hpp", "Emitters_2Test_2Tetra_8hpp.html", [
      [ "Tetra", "structTest_1_1Tetra.html", "structTest_1_1Tetra" ]
    ] ],
    [ "TetraFixture.hpp", "TetraFixture_8hpp.html", [
      [ "TetraFixture", "structTetraFixture.html", "structTetraFixture" ]
    ] ],
    [ "Triangle.hpp", "Test_2Triangle_8hpp.html", [
      [ "Triangle", "structTriangle.html", "structTriangle" ]
    ] ],
    [ "TriangleFixture.hpp", "TriangleFixture_8hpp.html", [
      [ "TriangleFixture", "structTriangleFixture.html", "structTriangleFixture" ]
    ] ],
    [ "UnitVectorFixture.hpp", "UnitVectorFixture_8hpp.html", [
      [ "UnitVectorFixture", "structUnitVectorFixture.html", "structUnitVectorFixture" ]
    ] ],
    [ "VTKLineCluster.cpp", "VTKLineCluster_8cpp.html", null ],
    [ "VTKLineCluster.hpp", "VTKLineCluster_8hpp.html", [
      [ "VTKLineCluster", "classVTKLineCluster.html", "classVTKLineCluster" ]
    ] ],
    [ "VTKPointCloud.cpp", "VTKPointCloud_8cpp.html", null ],
    [ "VTKPointCloud.hpp", "VTKPointCloud_8hpp.html", [
      [ "VTKPointCloud", "classVTKPointCloud.html", "classVTKPointCloud" ]
    ] ]
];