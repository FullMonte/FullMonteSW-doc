var NonSSE_8cpp =
[
    [ "add128", "da/d83/NonSSE_8cpp.html#a532a830bf2888ff0f3146f2b56df8c5b", null ],
    [ "addsub128", "da/d83/NonSSE_8cpp.html#a825f23c9c25718e3bcb95ac9fee36912", null ],
    [ "blend128", "da/d83/NonSSE_8cpp.html#a7c50b6fa16fcff7e91d0803c0f3991a4", null ],
    [ "cmpeq128", "da/d83/NonSSE_8cpp.html#a23198a7fc6db42c484db7a5b2168adcb", null ],
    [ "complo128", "da/d83/NonSSE_8cpp.html#aee2de8f782cf938ad4e9ebb51e26e719", null ],
    [ "div128", "da/d83/NonSSE_8cpp.html#a439320ef44f470bda8e8f104aca266b8", null ],
    [ "dp_ps128", "da/d83/NonSSE_8cpp.html#ac44b3e7633bc0a30f2e7660e318ee462", null ],
    [ "getMinIndex128", "da/d83/NonSSE_8cpp.html#ab2b78e60e368d08781c0ad60bc92bd51", null ],
    [ "load_ps128", "da/d83/NonSSE_8cpp.html#ac26a421a97c43bf595cfbdbf9787ff21", null ],
    [ "loadu_ps128", "da/d83/NonSSE_8cpp.html#a4dd92156f91238fe7ab403343faec1e7", null ],
    [ "max128", "da/d83/NonSSE_8cpp.html#adc05659bcfcb8ce8db961b3b90e659c3", null ],
    [ "min128", "da/d83/NonSSE_8cpp.html#a966cae54e6df366828029bc9fee0e020", null ],
    [ "movemask128", "da/d83/NonSSE_8cpp.html#a481cdc6e191930367be1e88a4552ecc0", null ],
    [ "mult128", "da/d83/NonSSE_8cpp.html#a619db924b08156bc2b151f508eda2458", null ],
    [ "rsqrt_ss", "da/d83/NonSSE_8cpp.html#a4b79c4ad94883bd48c296b80e8e26de5", null ],
    [ "set1_ps128", "da/d83/NonSSE_8cpp.html#acf2710b6972e2abc192f0c69199ad20b", null ],
    [ "set_zero128", "da/d83/NonSSE_8cpp.html#ab4531d3e9e26ca936ad884144e95df2a", null ],
    [ "setr_ps128", "da/d83/NonSSE_8cpp.html#a430cd7992ec9bc747c491204fcaa35bb", null ],
    [ "shuffle128", "da/d83/NonSSE_8cpp.html#aceff7dc6693a53062b135118e893bafa", null ],
    [ "sqrt_ss128", "da/d83/NonSSE_8cpp.html#aa1bc8aed20026443dbabfa12c3c28e8b", null ],
    [ "sub128", "da/d83/NonSSE_8cpp.html#abc71ab2d6a2df4578c2adfa624aa96bc", null ]
];