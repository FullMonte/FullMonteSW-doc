var classSinCosCordicTraits =
[
    [ "input_container_type", "da/d62/classSinCosCordicTraits.html#a5bf29607626d3e20afe3fe165f4c3f5b", null ],
    [ "input_type", "da/d62/classSinCosCordicTraits.html#a81a2c2491388bf12d61f7f7da6646156", null ],
    [ "output_container_type", "da/d62/classSinCosCordicTraits.html#a1a2f7021e6dc67832a72eb85551b9751", null ],
    [ "output_type", "da/d62/classSinCosCordicTraits.html#a97db9f7ae56061a4036db0e0bc148bb6", null ],
    [ "packed_input_type", "da/d62/classSinCosCordicTraits.html#ad7674c520a3feb4983c2749e39508d50", null ],
    [ "packed_output_type", "da/d62/classSinCosCordicTraits.html#a818e393d91aaad7d611c1dd477952672", null ],
    [ "convertFromNativeType", "da/d62/classSinCosCordicTraits.html#a5f33b7cf4c9aa339a764caab023bbe8b", null ],
    [ "convertToNativeType", "da/d62/classSinCosCordicTraits.html#a5ef1be3a23ad0e9e75e699db95315804", null ],
    [ "convertToNativeType", "da/d62/classSinCosCordicTraits.html#a771597739788bf54fa00477318f95a1d", null ],
    [ "input_bits", "da/d62/classSinCosCordicTraits.html#a9794517ffc05ed23d6f159e06d3ace66", null ],
    [ "output_bits", "da/d62/classSinCosCordicTraits.html#a94cca8da7454883f246e6c5448f436ee", null ]
];