var hg__test_8m =
[
    [ "legend", "da/d77/hg__test_8m.html#a4b4782eb7560b5662dc73c40fe86b1f2", null ],
    [ "plot", "da/d77/hg__test_8m.html#ad2ba3e9214fac6def7d66f5dd2cd5796", null ],
    [ "plot", "da/d77/hg__test_8m.html#a655518d06e84e28e6a12a991c8c318fc", null ],
    [ "plot", "da/d77/hg__test_8m.html#aed0d2c390a3e9583a48c1593eba07196", null ],
    [ "plot", "da/d77/hg__test_8m.html#af34371b1bb100b5eebd5451f0c17fe52", null ],
    [ "title", "da/d77/hg__test_8m.html#af73722aff115ae6d6aab5a65d7d78c04", null ],
    [ "title", "da/d77/hg__test_8m.html#acf2d06dcb9030d386d2b4651d43598ee", null ],
    [ "xlabel", "da/d77/hg__test_8m.html#a9d5a355c61a494db4bd649ae127c8ed9", null ],
    [ "xlabel", "da/d77/hg__test_8m.html#a5101fae2c402d864d3f59fa03c095c3c", null ],
    [ "ylabel", "da/d77/hg__test_8m.html#a501364e7c64a4389174ce69a733f3ff8", null ],
    [ "ylabel", "da/d77/hg__test_8m.html#a1eeb72c10c9911b37766650ab17994a2", null ],
    [ "f", "da/d77/hg__test_8m.html#a633de4b0c14ca52ea2432a3c8a5c4c31", null ],
    [ "figure", "da/d77/hg__test_8m.html#a391e34f2de441d79152a7b3d6e4c9c86", null ],
    [ "graphically", "da/d77/hg__test_8m.html#a83995b7a1c6cb1c62eb299754092ae96", null ],
    [ "N", "da/d77/hg__test_8m.html#a8cc2e7240164328fdc3f0e5e21032c56", null ],
    [ "on", "da/d77/hg__test_8m.html#a58ab1fd68e97078232808206b850161b", null ],
    [ "xl", "da/d77/hg__test_8m.html#a3c53fbdcb4e4034433f7fb548430436d", null ]
];