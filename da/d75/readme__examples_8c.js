var readme__examples_8c =
[
    [ "create_monitor", "da/d75/readme__examples_8c.html#ab10b3b636031ab2ee2c74f1718760d03", null ],
    [ "create_monitor_should_create_a_monitor", "da/d75/readme__examples_8c.html#a88bbbc5bac5cc30255298f29cea9c6bd", null ],
    [ "create_monitor_with_helpers", "da/d75/readme__examples_8c.html#a3df44a6d83f425696bde276722b71b5f", null ],
    [ "create_monitor_with_helpers_should_create_a_monitor", "da/d75/readme__examples_8c.html#a9900a534cd3b107f082f495595b8a16b", null ],
    [ "main", "da/d75/readme__examples_8c.html#a9fd408b0fa7364c093bb678cb12f9b83", null ],
    [ "supports_full_hd", "da/d75/readme__examples_8c.html#aac8a48367d8eb50de4be9aa823415b34", null ],
    [ "supports_full_hd_should_check_for_full_hd_support", "da/d75/readme__examples_8c.html#a220e0c996a24a3bf16fc44c168bc789a", null ],
    [ "json", "da/d75/readme__examples_8c.html#a4dd85a1ffd4add568eb3b646e857eadc", null ]
];