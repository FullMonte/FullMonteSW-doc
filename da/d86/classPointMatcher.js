var classPointMatcher =
[
    [ "PointCoord", "da/d86/classPointMatcher.html#a79bcbb6199532fd157e763b7792b11e0", null ],
    [ "PointPair", "da/d86/classPointMatcher.html#a7af04d0c8a70959404e0e6bfcd61daf1", null ],
    [ "PointMatcher", "da/d86/classPointMatcher.html#aefdc62c8c5edf3618de943305688e974", null ],
    [ "~PointMatcher", "da/d86/classPointMatcher.html#ac21de0cb26f9aafd91744a45bc021086", null ],
    [ "createRTree", "da/d86/classPointMatcher.html#a562861dc8be45835972f8cb8a1948e82", null ],
    [ "match", "da/d86/classPointMatcher.html#afbe1a2f06e5c5ad08fc9f9d099d3e5de", null ],
    [ "query", "da/d86/classPointMatcher.html#adf28b73c1f4b765cf951a0dd67f76359", null ],
    [ "reference", "da/d86/classPointMatcher.html#a3ae967518dafe4ee4db882753abc4d1b", null ],
    [ "result", "da/d86/classPointMatcher.html#a48c593f8181b3a8ddc0cc43897f2cd58", null ],
    [ "tolerance", "da/d86/classPointMatcher.html#a13dd98321839fb171f1fa9c37e980ea8", null ],
    [ "unmatchedCount", "da/d86/classPointMatcher.html#a6323d5cc7ac77dd55837ae8f29746455", null ],
    [ "update", "da/d86/classPointMatcher.html#a7a8b7895fa7b0b220b15f0325808f2bb", null ],
    [ "m_eps", "da/d86/classPointMatcher.html#a7b881af63e46c74ff1c76e3b1bb24f41", null ],
    [ "m_permutation", "da/d86/classPointMatcher.html#a491b6618fbae87966a275afa8d801157", null ],
    [ "m_pointIndex", "da/d86/classPointMatcher.html#abf4870b89b6fcf51d76909e0aadc1557", null ],
    [ "m_query", "da/d86/classPointMatcher.html#a8973f3cac31447b7b87c5b7f89303af5", null ],
    [ "m_reference", "da/d86/classPointMatcher.html#a4d626e1048c7a490cdc44d0f4134cf96", null ],
    [ "m_unmatched", "da/d86/classPointMatcher.html#ab028f3926cf9002b6e387ecb1cda762b", null ],
    [ "s_maxElementsPerNode", "da/d86/classPointMatcher.html#a52ba90dfbf0bd40d2a0fa9d17e2b23fc", null ]
];