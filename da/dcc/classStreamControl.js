var classStreamControl =
[
    [ "Progress", "d5/df9/structStreamControl_1_1Progress.html", "d5/df9/structStreamControl_1_1Progress" ],
    [ "StreamControl", "da/dcc/classStreamControl.html#ace7cbc8a3a1472452d24f882cedd9f30", null ],
    [ "~StreamControl", "da/dcc/classStreamControl.html#a55e28600f317866245a7bca000186058", null ],
    [ "address", "da/dcc/classStreamControl.html#ae2d2b0b9486caa0e4b674d281cd48f40", null ],
    [ "bytes", "da/dcc/classStreamControl.html#aa3430399f25ab4be2a8ba0e45384d225", null ],
    [ "done", "da/dcc/classStreamControl.html#ad4a2a46285d851fca05d87e5477f5008", null ],
    [ "progress", "da/dcc/classStreamControl.html#ae4e9f9baeffb01d762b0819f19b67f40", null ],
    [ "m_addressMMIO", "da/dcc/classStreamControl.html#a7a7013553d73c1e0278b0963a818046b", null ],
    [ "m_afu", "da/dcc/classStreamControl.html#a477f103c0ebc577821d46a068828419a", null ],
    [ "m_size", "da/dcc/classStreamControl.html#ae1c2f298aff70239d5e6ecd76981151c", null ],
    [ "m_sizeMMIO", "da/dcc/classStreamControl.html#a45a4f5b8d3221530e708aa67bd1dec67", null ],
    [ "m_statusMMIO", "da/dcc/classStreamControl.html#abc1d4c6e87b77babfdd762fd7529d798", null ]
];