var checkLog_8m =
[
    [ "checkLog", "da/d65/checkLog_8m.html#ae776b1e1b45f6d77adde12561b141436", null ],
    [ "hist", "da/d65/checkLog_8m.html#af38d16c5c515d377a4f0a1aa99eb4e51", null ],
    [ "plot", "da/d65/checkLog_8m.html#ad41df02956618e6213e74a8003010780", null ],
    [ "printf", "da/d65/checkLog_8m.html#ac6dcb6680b689773f4a2b2e4b65e245c", null ],
    [ "set", "da/d65/checkLog_8m.html#a7ec2ea8d8a0b199904c8c8122eb26e21", null ],
    [ "stem", "da/d65/checkLog_8m.html#a5ff857aa5c651d6249f050a2c2314487", null ],
    [ "stem", "da/d65/checkLog_8m.html#a8a6f56debf154375c32b4304860ec2a4", null ],
    [ "title", "da/d65/checkLog_8m.html#ae928c89ca42339894b857fae4c0542ff", null ],
    [ "title", "da/d65/checkLog_8m.html#ac3937e72e82857e09900f291c60aca7a", null ],
    [ "title", "da/d65/checkLog_8m.html#adb6e66993fa391dfff0a5958662238a0", null ],
    [ "title", "da/d65/checkLog_8m.html#a021671fac4c39390900f34aad0f53d03", null ],
    [ "xlabel", "da/d65/checkLog_8m.html#acbeb4d8f283f17409dc7839f59170f0c", null ],
    [ "ylabel", "da/d65/checkLog_8m.html#a217cabd1f4d82a089702c59b6c984120", null ],
    [ "ylabel", "da/d65/checkLog_8m.html#a090a1d7ec00c1aa40f699e0a33380dea", null ],
    [ "ylabel", "da/d65/checkLog_8m.html#a52ee4c41f5a06da260d2196c03faf671", null ],
    [ "abs_err", "da/d65/checkLog_8m.html#a187d48dd55af71a238814497ed9ea8d3", null ],
    [ "ei", "da/d65/checkLog_8m.html#acf1a44eb4893e6b8322f59155fa4a118", null ],
    [ "figure", "da/d65/checkLog_8m.html#a391e34f2de441d79152a7b3d6e4c9c86", null ],
    [ "i", "da/d65/checkLog_8m.html#a6f6ccfcf58b31cb6412107d9d5281426", null ],
    [ "x", "da/d65/checkLog_8m.html#a9336ebf25087d91c818ee6e9ec29f8c1", null ],
    [ "xi", "da/d65/checkLog_8m.html#ade156e39c8481df78050021b1ffcd425", null ],
    [ "y_out", "da/d65/checkLog_8m.html#a92a746263988c6e70952572d6e5f2ae7", null ],
    [ "y_ref", "da/d65/checkLog_8m.html#a65793fb33de4d0f26c6c62e2c4b6c2f3", null ]
];