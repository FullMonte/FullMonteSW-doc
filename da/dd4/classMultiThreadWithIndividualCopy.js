var classMultiThreadWithIndividualCopy =
[
    [ "Logger", "dd/dea/classMultiThreadWithIndividualCopy_1_1Logger.html", "dd/dea/classMultiThreadWithIndividualCopy_1_1Logger" ],
    [ "SingleThreadLogger", "da/dd4/classMultiThreadWithIndividualCopy.html#a2483ca81622b92f6d1a1efb13315cc0d", null ],
    [ "State", "da/dd4/classMultiThreadWithIndividualCopy.html#a921039de7dfacdd94aa6898cf17691ba", null ],
    [ "MultiThreadWithIndividualCopy", "da/dd4/classMultiThreadWithIndividualCopy.html#af9debe4ec046cc32a05be7c7f820724a", null ],
    [ "clear", "da/dd4/classMultiThreadWithIndividualCopy.html#afae5d9e3ad79adb9c6e80d22af6f5a24", null ],
    [ "createLogger", "da/dd4/classMultiThreadWithIndividualCopy.html#a1446b76da0bbc822d1deaba549d03513", null ],
    [ "merge", "da/dd4/classMultiThreadWithIndividualCopy.html#a6cc5523228b2f3504e2ab2238f2f5525", null ],
    [ "postResults", "da/dd4/classMultiThreadWithIndividualCopy.html#a80f93ca8b0d937c97add0b6d7deccf1a", null ],
    [ "prepare", "da/dd4/classMultiThreadWithIndividualCopy.html#ac6adeb38d617d2aeeb13d43f030d3454", null ],
    [ "m_mutex", "da/dd4/classMultiThreadWithIndividualCopy.html#ac10201d0c83b3ef29eb00c8563efaffe", null ],
    [ "m_prototype", "da/dd4/classMultiThreadWithIndividualCopy.html#a02033d0bc11efea182c935cf6fdf5bde", null ],
    [ "m_state", "da/dd4/classMultiThreadWithIndividualCopy.html#ad068e49c4733114c9492b9517835e182", null ]
];