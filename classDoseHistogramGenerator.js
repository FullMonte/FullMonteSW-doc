var classDoseHistogramGenerator =
[
    [ "ElRegion", "structDoseHistogramGenerator_1_1ElRegion.html", "structDoseHistogramGenerator_1_1ElRegion" ],
    [ "DoseHistogramGenerator", "classDoseHistogramGenerator.html#ab8dc338da6b4855fedb3604b2872d548", null ],
    [ "~DoseHistogramGenerator", "classDoseHistogramGenerator.html#a903c1be5c99a6c2dab79278ddee63500", null ],
    [ "dose", "classDoseHistogramGenerator.html#afb2b03586e1f2fbd55fd1269c8e49d07", null ],
    [ "dose", "classDoseHistogramGenerator.html#a0810d66345abb1af1d1cba37349b1968", null ],
    [ "getDose", "classDoseHistogramGenerator.html#a14c91b2196b5383b249c79699af7e4e4", null ],
    [ "getMeasures", "classDoseHistogramGenerator.html#a8e58fbe773a88ec7e8ecb6e04a0c8755", null ],
    [ "getPartition", "classDoseHistogramGenerator.html#a449274f355aa80c52f0250456d8ad171", null ],
    [ "mesh", "classDoseHistogramGenerator.html#a9a66a84279014b4cead8876ea2f47e4f", null ],
    [ "mesh", "classDoseHistogramGenerator.html#a2002c53b820e7151aa41f5fb4da06614", null ],
    [ "result", "classDoseHistogramGenerator.html#aa94ec5089f5aa2ed2658b0b348c831a3", null ],
    [ "update", "classDoseHistogramGenerator.html#a4c5184625b3d399fea77157ee3b91c99", null ]
];