var structTIMOS_1_1GenericSource =
[
    [ "Type", "structTIMOS_1_1GenericSource.html#ab404607d323d0e497a3e745f67110308", [
      [ "Point", "structTIMOS_1_1GenericSource.html#ab404607d323d0e497a3e745f67110308a3f556423cbb878b753bb1c80c3c06f49", null ],
      [ "Volume", "structTIMOS_1_1GenericSource.html#ab404607d323d0e497a3e745f67110308a27ab7fa5b2ef0f0d50f83b937042c88e", null ],
      [ "PencilBeam", "structTIMOS_1_1GenericSource.html#ab404607d323d0e497a3e745f67110308a05c0e4e08f7c3c3186acf1a5d640e9d0", null ],
      [ "Face", "structTIMOS_1_1GenericSource.html#ab404607d323d0e497a3e745f67110308a6d1f6acc61397ffc16b40b50ea64b8a3", null ]
    ] ],
    [ "details", "structTIMOS_1_1GenericSource.html#ac8e2d0a4c5e3a318ef7603ca4641d187", null ],
    [ "face", "structTIMOS_1_1GenericSource.html#a2b3961b8bd1ac71935407b8efa095c3e", null ],
    [ "pencil", "structTIMOS_1_1GenericSource.html#ac2f343f8faff1e68160b5463c1d1e88a", null ],
    [ "point", "structTIMOS_1_1GenericSource.html#ada217fc46a9e10a563c0de56690e8aab", null ],
    [ "tetra", "structTIMOS_1_1GenericSource.html#ae2dd6c01d1a0419cca53c696d67142c0", null ],
    [ "type", "structTIMOS_1_1GenericSource.html#afcd6427fed582eaa8c10820db84155f7", null ],
    [ "w", "structTIMOS_1_1GenericSource.html#aba8d1bd0b477eb6b77d364c8814a1fef", null ]
];