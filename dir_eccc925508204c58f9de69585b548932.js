var dir_eccc925508204c58f9de69585b548932 =
[
    [ "Modifiers", "dir_f2c6e97cab290a61da05524c222bb2d3.html", "dir_f2c6e97cab290a61da05524c222bb2d3" ],
    [ "Placement", "dir_66d9e2270921ecfc59faa8a5469b8158.html", "dir_66d9e2270921ecfc59faa8a5469b8158" ],
    [ "Predicates", "dir_4e29a68041e7e2a80c68b505f5deb000.html", "dir_4e29a68041e7e2a80c68b505f5deb000" ],
    [ "Queries", "dir_de077b8dfe89127e83dbf0fb8f95892c.html", "dir_de077b8dfe89127e83dbf0fb8f95892c" ],
    [ "Sources", "dir_84a7657c91fec02cb1ab72364c3ef45e.html", "dir_84a7657c91fec02cb1ab72364c3ef45e" ],
    [ "Basis.cpp", "d3/d51/Basis_8cpp.html", null ],
    [ "Basis.hpp", "d0/db4/Basis_8hpp.html", "d0/db4/Basis_8hpp" ],
    [ "BasisIO.hpp", "dc/db0/BasisIO_8hpp.html", "dc/db0/BasisIO_8hpp" ],
    [ "BoundingBox.hpp", "d9/db6/BoundingBox_8hpp.html", [
      [ "OrthoBoundingBox", "d4/dea/classOrthoBoundingBox.html", "d4/dea/classOrthoBoundingBox" ]
    ] ],
    [ "Builder.hpp", "db/d12/Builder_8hpp.html", null ],
    [ "Cells.hpp", "d7/d41/Cells_8hpp.html", "d7/d41/Cells_8hpp" ],
    [ "ErrorChecker.cpp", "d5/d14/ErrorChecker_8cpp.html", null ],
    [ "ErrorChecker.hpp", "d9/d4f/ErrorChecker_8hpp.html", [
      [ "Error", "d9/dd6/classError.html", "d9/dd6/classError" ],
      [ "ErrorChecker", "da/d70/classErrorChecker.html", "da/d70/classErrorChecker" ],
      [ "MeshErrorChecker", "d2/d8b/classMeshErrorChecker.html", "d2/d8b/classMeshErrorChecker" ],
      [ "TetraOrientationChecker", "d6/dc7/classTetraOrientationChecker.html", "d6/dc7/classTetraOrientationChecker" ]
    ] ],
    [ "Face.cpp", "d6/d3e/Face_8cpp.html", null ],
    [ "FaceLinks.hpp", "de/d9f/FaceLinks_8hpp.html", "de/d9f/FaceLinks_8hpp" ],
    [ "Geometry.cpp", "d7/def/Geometry_8cpp.html", null ],
    [ "Geometry.hpp", "de/d49/Geometry_8hpp.html", [
      [ "SpatialMap", "df/d80/classSpatialMap.html", "df/d80/classSpatialMap" ],
      [ "Geometry", "d9/df2/classGeometry.html", "d9/df2/classGeometry" ]
    ] ],
    [ "ImplicitPlane.hpp", "df/de3/ImplicitPlane_8hpp.html", [
      [ "ImplicitPlane", "d7/de7/classImplicitPlane.html", "d7/de7/classImplicitPlane" ]
    ] ],
    [ "Layer.cpp", "d1/d36/Layer_8cpp.html", null ],
    [ "Layer.hpp", "d2/d3b/Layer_8hpp.html", [
      [ "Layer", "d5/d8f/classLayer.html", "d5/d8f/classLayer" ]
    ] ],
    [ "Layered.cpp", "da/dc4/Layered_8cpp.html", "da/dc4/Layered_8cpp" ],
    [ "Layered.hpp", "d0/d46/Layered_8hpp.html", "d0/d46/Layered_8hpp" ],
    [ "Material.cpp", "d1/d5e/Geometry_2Material_8cpp.html", null ],
    [ "Material.hpp", "d3/d1d/Geometry_2Material_8hpp.html", [
      [ "Material", "d3/d89/classMaterial.html", "d3/d89/classMaterial" ]
    ] ],
    [ "MaterialSet.cpp", "d6/d34/MaterialSet_8cpp.html", null ],
    [ "MaterialSet.hpp", "d2/d08/MaterialSet_8hpp.html", [
      [ "MaterialSet", "d1/d43/classMaterialSet.html", "d1/d43/classMaterialSet" ]
    ] ],
    [ "NonSSE.cpp", "da/d83/NonSSE_8cpp.html", "da/d83/NonSSE_8cpp" ],
    [ "NonSSE.hpp", "d8/dbb/NonSSE_8hpp.html", "d8/dbb/NonSSE_8hpp" ],
    [ "OrthoBoundingBox.hpp", "da/d94/OrthoBoundingBox_8hpp.html", "da/d94/OrthoBoundingBox_8hpp" ],
    [ "Partition.cpp", "da/d5c/Partition_8cpp.html", null ],
    [ "Partition.hpp", "dc/d4d/Partition_8hpp.html", [
      [ "Partition", "d5/d03/classPartition.html", "d5/d03/classPartition" ]
    ] ],
    [ "Permutation.cpp", "d0/d8f/Permutation_8cpp.html", "d0/d8f/Permutation_8cpp" ],
    [ "Permutation.hpp", "d2/d47/Permutation_8hpp.html", [
      [ "Permutation", "d0/d38/classPermutation.html", "d0/d38/classPermutation" ]
    ] ],
    [ "PFTetraMeshBuilder.cpp", "d5/d96/PFTetraMeshBuilder_8cpp.html", null ],
    [ "PFTetraMeshBuilder.hpp", "df/d5e/PFTetraMeshBuilder_8hpp.html", [
      [ "PFTetraMeshBuilder", "dd/deb/classPFTetraMeshBuilder.html", "dd/deb/classPFTetraMeshBuilder" ]
    ] ],
    [ "Point.hpp", "d1/d46/Geometry_2Point_8hpp.html", "d1/d46/Geometry_2Point_8hpp" ],
    [ "Points.cpp", "db/d45/Points_8cpp.html", null ],
    [ "Points.hpp", "da/d87/Points_8hpp.html", [
      [ "Points", "d4/dd1/classPoints.html", "d4/dd1/classPoints" ]
    ] ],
    [ "PTTetraMeshBuilder.cpp", "d4/d74/PTTetraMeshBuilder_8cpp.html", null ],
    [ "PTTetraMeshBuilder.hpp", "d1/d36/PTTetraMeshBuilder_8hpp.html", [
      [ "PTTetraMeshBuilder", "dd/db5/classPTTetraMeshBuilder.html", "dd/db5/classPTTetraMeshBuilder" ]
    ] ],
    [ "Ray.hpp", "d4/db2/Ray_8hpp.html", "d4/db2/Ray_8hpp" ],
    [ "RayWalk.cpp", "da/dd2/RayWalk_8cpp.html", null ],
    [ "RayWalk.hpp", "d7/de3/RayWalk_8hpp.html", [
      [ "WalkFaceInfo", "d0/d1c/structWalkFaceInfo.html", "d0/d1c/structWalkFaceInfo" ],
      [ "WalkSegment", "d8/d9a/structWalkSegment.html", "d8/d9a/structWalkSegment" ],
      [ "PointIntersectionResult", "d3/dab/structPointIntersectionResult.html", "d3/dab/structPointIntersectionResult" ],
      [ "LineQuery", "dd/dd7/classLineQuery.html", "dd/dd7/classLineQuery" ],
      [ "RayWalkIterator", "d4/d0e/classRayWalkIterator.html", "d4/d0e/classRayWalkIterator" ]
    ] ],
    [ "RemapRegions.cpp", "d9/d2d/RemapRegions_8cpp.html", [
      [ "RemapTable", "d4/d38/classRemapTable.html", "d4/d38/classRemapTable" ]
    ] ],
    [ "RemapRegions.hpp", "dd/d01/RemapRegions_8hpp.html", [
      [ "RemapRegions", "df/dea/classRemapRegions.html", "df/dea/classRemapRegions" ]
    ] ],
    [ "StandardArrayKernel.hpp", "d4/dd9/StandardArrayKernel_8hpp.html", "d4/dd9/StandardArrayKernel_8hpp" ],
    [ "take_drop.hpp", "df/d79/take__drop_8hpp.html", "df/d79/take__drop_8hpp" ],
    [ "TetraMesh.cpp", "db/d52/TetraMesh_8cpp.html", "db/d52/TetraMesh_8cpp" ],
    [ "TetraMesh.hpp", "d6/d64/TetraMesh_8hpp.html", "d6/d64/TetraMesh_8hpp" ],
    [ "TetraMeshBuilder.cpp", "d9/d16/TetraMeshBuilder_8cpp.html", null ],
    [ "TetraMeshBuilder.hpp", "d9/dc9/TetraMeshBuilder_8hpp.html", [
      [ "TetraMeshBuilder", "dc/d7f/classTetraMeshBuilder.html", "dc/d7f/classTetraMeshBuilder" ],
      [ "FaceInfo", "d8/d7e/structTetraMeshBuilder_1_1FaceInfo.html", "d8/d7e/structTetraMeshBuilder_1_1FaceInfo" ],
      [ "FaceTetraInfo", "df/d68/structTetraMeshBuilder_1_1FaceInfo_1_1FaceTetraInfo.html", "df/d68/structTetraMeshBuilder_1_1FaceInfo_1_1FaceTetraInfo" ]
    ] ],
    [ "TetraOrientationChecker.cpp", "dc/d6e/TetraOrientationChecker_8cpp.html", null ],
    [ "UnitDimension.hpp", "d2/d9d/UnitDimension_8hpp.html", "d2/d9d/UnitDimension_8hpp" ],
    [ "UnitVector.hpp", "db/db1/UnitVector_8hpp.html", "db/db1/UnitVector_8hpp" ],
    [ "Vector.hpp", "de/dd9/Vector_8hpp.html", "de/dd9/Vector_8hpp" ],
    [ "VectorTest.hpp", "d1/d98/VectorTest_8hpp.html", "d1/d98/VectorTest_8hpp" ],
    [ "WrappedVector.hpp", "df/d2a/WrappedVector_8hpp.html", [
      [ "WrappedVector", "dc/def/classWrappedVector.html", "dc/def/classWrappedVector" ]
    ] ]
];