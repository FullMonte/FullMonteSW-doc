var classDoseHistogramCollection =
[
    [ "DoseHistogramCollection", "d3/db9/classDoseHistogramCollection.html#aa82b26671241b573aa7e0eccdd35946c", null ],
    [ "DoseHistogramCollection", "d3/db9/classDoseHistogramCollection.html#a05039ce39a73c25c16537420dcfdfb21", null ],
    [ "~DoseHistogramCollection", "d3/db9/classDoseHistogramCollection.html#abe5e64eee86b9ebdca07d6556fb94f05", null ],
    [ "add", "d3/db9/classDoseHistogramCollection.html#a45efd8ffaabcd8842639e8f6dba32997", null ],
    [ "clone", "d3/db9/classDoseHistogramCollection.html#ade2cf93f556ad18d329d60cd044af392", null ],
    [ "count", "d3/db9/classDoseHistogramCollection.html#aaee22187ba25b9308645527a0d9138db", null ],
    [ "get", "d3/db9/classDoseHistogramCollection.html#a2e479878cc1fe11184392d2729ddc6dd", null ],
    [ "remove", "d3/db9/classDoseHistogramCollection.html#a94144d6e9cd9d8227be626b011a36686", null ],
    [ "type", "d3/db9/classDoseHistogramCollection.html#afaa09eec963a9214a4ce9570b970de94", null ],
    [ "m_histograms", "d3/db9/classDoseHistogramCollection.html#a5fd3d255da56c06b62ef0d6ec96024cc", null ]
];