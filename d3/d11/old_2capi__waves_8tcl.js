var old_2capi__waves_8tcl =
[
    [ "capi_waves", "d3/d11/old_2capi__waves_8tcl.html#a577ebcbd42cea699a6f9f2f484ecb897", null ],
    [ "capi_waves_buffer", "d3/d11/old_2capi__waves_8tcl.html#a0414c5ab9ed015831d885b56e00d549a", null ],
    [ "capi_waves_buffer_read", "d3/d11/old_2capi__waves_8tcl.html#a10d217e77d7c816f7336be7bce575c5b", null ],
    [ "capi_waves_buffer_read_request", "d3/d11/old_2capi__waves_8tcl.html#a8eb27f2362c19b36c7d20c6f717b1d27", null ],
    [ "capi_waves_buffer_read_response", "d3/d11/old_2capi__waves_8tcl.html#a247d88e81cb18d78353764ba572b1655", null ],
    [ "capi_waves_buffer_write", "d3/d11/old_2capi__waves_8tcl.html#ada4bb40119e103f06a5258e1022019ac", null ],
    [ "capi_waves_command", "d3/d11/old_2capi__waves_8tcl.html#ae9939a95bc0745552b26c6824c1c07ae", null ],
    [ "capi_waves_command_request", "d3/d11/old_2capi__waves_8tcl.html#ab332f1465320f903a9dd211860ce3490", null ],
    [ "capi_waves_command_response", "d3/d11/old_2capi__waves_8tcl.html#adb639759ec3918555b4c94c722a0231a", null ],
    [ "capi_waves_control", "d3/d11/old_2capi__waves_8tcl.html#a45292f9eaf14d987369140f29c04ac73", null ],
    [ "capi_waves_mmio", "d3/d11/old_2capi__waves_8tcl.html#a1c4fb725b3fc666baec448a1da998480", null ],
    [ "capi_waves_mmio_request", "d3/d11/old_2capi__waves_8tcl.html#a4f117a6ddf6c0e8b997b6f0afe4cf46d", null ],
    [ "capi_waves_mmio_response", "d3/d11/old_2capi__waves_8tcl.html#a9144cda1aa4bae737622c5a6eb066907", null ],
    [ "capi_waves_status", "d3/d11/old_2capi__waves_8tcl.html#a9d335db92096b6dcf5a47d201c97503d", null ]
];