var classEventCountComparison =
[
    [ "EventCountComparison", "d3/d34/classEventCountComparison.html#a8e12c0d485416aadda5623be5367d372", null ],
    [ "~EventCountComparison", "d3/d34/classEventCountComparison.html#a12db0182e891ba32067b9b7913f069df", null ],
    [ "binomialCompare", "d3/d34/classEventCountComparison.html#a48f725b3848b67e79554c85fea23db87", null ],
    [ "binomialCompare", "d3/d34/classEventCountComparison.html#a839fd6b88e002e336b4debb10a42ec6e", null ],
    [ "left", "d3/d34/classEventCountComparison.html#a6e7c712722b13faa59265479e7522cce", null ],
    [ "poissonCompare", "d3/d34/classEventCountComparison.html#aa601b1a82bb5dba2f4d49ec564816781", null ],
    [ "poissonCompare", "d3/d34/classEventCountComparison.html#a2e230b6e31f17c86a0ce8780c46d4503", null ],
    [ "print", "d3/d34/classEventCountComparison.html#ad600eb2d792bf5298dd78826bb85aa38", null ],
    [ "right", "d3/d34/classEventCountComparison.html#a572386c54d143d8ebae51bbaaf49a926", null ],
    [ "update", "d3/d34/classEventCountComparison.html#ae0ecddd54153158a1d81c5818d7dab80", null ],
    [ "m_lhs", "d3/d34/classEventCountComparison.html#aeed76f3d4975ac0e06f92803bef6cd60", null ],
    [ "m_rhs", "d3/d34/classEventCountComparison.html#a2cbef2e33e61b929b929be269bfbfdbe", null ]
];