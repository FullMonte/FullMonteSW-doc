var classMaterial =
[
    [ "Material", "d3/d89/classMaterial.html#a137e987401b63eb7c6c27c3e38bc74b5", null ],
    [ "Material", "d3/d89/classMaterial.html#a824c9cf037bf0ab881b7d31ca1d16f9f", null ],
    [ "~Material", "d3/d89/classMaterial.html#a2c19452d71f54075df8f5405b03129f4", null ],
    [ "absorptionCoeff", "d3/d89/classMaterial.html#ac6c87b1b022957902e93ab44952606d6", null ],
    [ "absorptionCoeff", "d3/d89/classMaterial.html#aba80e73c56a2f1c90499bd4afe72d9d7", null ],
    [ "anisotropy", "d3/d89/classMaterial.html#aaa5ea324529a978f5f120d4f1a98ace7", null ],
    [ "anisotropy", "d3/d89/classMaterial.html#af2f12b37f63761d9728539b30366a4b4", null ],
    [ "getUnit", "d3/d89/classMaterial.html#ac57d524a699bde6aaf92bf3eb834c772", null ],
    [ "reducedScatteringCoeff", "d3/d89/classMaterial.html#a785551288a3ff63d9dee79a21ae81f4f", null ],
    [ "reducedScatteringCoeff", "d3/d89/classMaterial.html#a6136576de7d9ca6b768a45b2f3fb2099", null ],
    [ "reducedScatteringCoeffWithG", "d3/d89/classMaterial.html#a114d8d5e2b3b7485a17dce1ae3329e1d", null ],
    [ "refractiveIndex", "d3/d89/classMaterial.html#a1d086a56433324f66d276a95cec87132", null ],
    [ "refractiveIndex", "d3/d89/classMaterial.html#a78bf1c20372210f93d80fe9c29a1eea3", null ],
    [ "scatteringCoeff", "d3/d89/classMaterial.html#a5d4f305dc3e0a1f25ada38c5649067a7", null ],
    [ "scatteringCoeff", "d3/d89/classMaterial.html#a63bd8bdb00ef0ce6edeb0bac33689edb", null ],
    [ "unitDimension", "d3/d89/classMaterial.html#a501490423f09bdb117016fb1581fdb16", null ],
    [ "unitDimension", "d3/d89/classMaterial.html#a719803d9fbd04ad1512e814e039332ab", null ],
    [ "g", "d3/d89/classMaterial.html#a4b832bd11d2b3d00ab74a269f2f2e9e8", null ],
    [ "mu_a", "d3/d89/classMaterial.html#a5a881d45bd25aa32793e601b1b09058b", null ],
    [ "mu_s", "d3/d89/classMaterial.html#a6a1e81064b2621923ebd3737c1908845", null ],
    [ "n", "d3/d89/classMaterial.html#ad0530e1ba6d4d24f55258a0566ced52c", null ],
    [ "unit", "d3/d89/classMaterial.html#a66356d7a41e9e452b407cbaf681a9912", null ]
];