var classEventLogger =
[
    [ "eventAbnormal", "d3/de0/classEventLogger.html#a3ae5fa13a1be6bce4ae0a6b247fa6272", null ],
    [ "eventAbsorb", "d3/de0/classEventLogger.html#a7036d961faaa8e195c404038dcabf8db", null ],
    [ "eventBoundary", "d3/de0/classEventLogger.html#ab6975336fee493ea26284c11136a3351", null ],
    [ "eventDie", "d3/de0/classEventLogger.html#a8a1d48a01b8584c2e351f1464c579fbe", null ],
    [ "eventExit", "d3/de0/classEventLogger.html#a09dac05a24f98ab7ea12ed69873e4976", null ],
    [ "eventInterface", "d3/de0/classEventLogger.html#a0ef5da817f1d22259dd2649fa6e09c68", null ],
    [ "eventLaunch", "d3/de0/classEventLogger.html#abadee27f3828d9feace8d2b7d9e00686", null ],
    [ "eventReflectFresnel", "d3/de0/classEventLogger.html#a7925e0417b4dd6f2ddf36e5692dc6f90", null ],
    [ "eventReflectInternal", "d3/de0/classEventLogger.html#a076473db1ab217117075abacde8941bc", null ],
    [ "eventRefract", "d3/de0/classEventLogger.html#a92585b372bee01b6a019d132f99732f5", null ],
    [ "eventRouletteWin", "d3/de0/classEventLogger.html#a4ffc620093a59d2063d70c78c129f4f8", null ],
    [ "eventScatter", "d3/de0/classEventLogger.html#a8010e13574580a052c7ee377deb06011", null ],
    [ "eventSpecialAbsorb", "d3/de0/classEventLogger.html#a386d4a01db89e1e0509ef597dced49b5", null ],
    [ "eventSpecialReflect", "d3/de0/classEventLogger.html#a1645986aa19fa8180d086bd06a3db6c8", null ],
    [ "eventSpecialTerminate", "d3/de0/classEventLogger.html#a8581dd19674c75b21d1d2b50dbeb4c39", null ],
    [ "eventSpecialTransmit", "d3/de0/classEventLogger.html#a03a08e8cc8225836065d43360c19650a", null ],
    [ "postResults", "d3/de0/classEventLogger.html#ab59cd2282569294edbc88e6b0fab4ac5", null ],
    [ "prepare", "d3/de0/classEventLogger.html#ad2a13052f3d7c9c0633bb8989af9d395", null ],
    [ "d", "d3/de0/classEventLogger.html#a12e69faa25428a39a708bd945041c574", null ],
    [ "events", "d3/de0/classEventLogger.html#ae3af3245c8ec5ac10b2bddc3b27885da", null ]
];