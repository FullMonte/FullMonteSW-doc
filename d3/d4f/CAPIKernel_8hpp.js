var CAPIKernel_8hpp =
[
    [ "CAPIKernel", "d7/de3/classCAPIKernel.html", "d7/de3/classCAPIKernel" ],
    [ "MemcopyWED", "d2/d54/structCAPIKernel_1_1MemcopyWED.html", "d2/d54/structCAPIKernel_1_1MemcopyWED" ],
    [ "DEFAULT", "d3/d4f/CAPIKernel_8hpp.html#a3da44afeba217135a680a7477b5e3ce3", null ],
    [ "DEVICE_STRING", "d3/d4f/CAPIKernel_8hpp.html#a0cc1f594a089044000af260be46f2309", null ],
    [ "HARDWARE", "d3/d4f/CAPIKernel_8hpp.html#ae63cc10269ee1bb0c967a74ac5a0d0d5", null ],
    [ "PI", "d3/d4f/CAPIKernel_8hpp.html#a598a3330b3c21701223ee0ca14316eca", null ],
    [ "STATUS_DONE", "d3/d4f/CAPIKernel_8hpp.html#a3c7966dcc2aec8e20459e036f5823fc2", null ],
    [ "STATUS_READY", "d3/d4f/CAPIKernel_8hpp.html#a64f314e17a1be8e6c8e09bb90b9ecb68", null ],
    [ "STATUS_RUNNING", "d3/d4f/CAPIKernel_8hpp.html#a799b04d03f49612e3e636b56c3972b80", null ],
    [ "STATUS_WAITING", "d3/d4f/CAPIKernel_8hpp.html#a4eb8ee21eeb7699dbe5fcee4c30d7b72", null ]
];