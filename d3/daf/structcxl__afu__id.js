var structcxl__afu__id =
[
    [ "afu_mode", "d3/daf/structcxl__afu__id.html#a3e3fb83d707ef1ce84c48d2762975403", null ],
    [ "afu_offset", "d3/daf/structcxl__afu__id.html#a34453c2540634c7437efb71295afd093", null ],
    [ "card_id", "d3/daf/structcxl__afu__id.html#afa469f48686d40c7a8d90bee065b8df9", null ],
    [ "flags", "d3/daf/structcxl__afu__id.html#aac0746357236b2d31197710dd94a2201", null ],
    [ "reserved1", "d3/daf/structcxl__afu__id.html#a777ee4c9f463e9026d9a4ed4f897048c", null ],
    [ "reserved2", "d3/daf/structcxl__afu__id.html#ac075d8403c0db375656929410fc5ee56", null ],
    [ "reserved3", "d3/daf/structcxl__afu__id.html#ab7fe7490c74f32939f892393804c0a9b", null ],
    [ "reserved4", "d3/daf/structcxl__afu__id.html#ab2e3bf6cd51c3a6de66c0fff9befa8ee", null ],
    [ "reserved5", "d3/daf/structcxl__afu__id.html#a6a613098a343bab8e4565d314115d18b", null ],
    [ "reserved6", "d3/daf/structcxl__afu__id.html#a0d1ced617929000ca76d253c2ed5bcc8", null ]
];