var classFloatUniformSinDistribution =
[
    [ "input_type", "d3/d7b/classFloatUniformSinDistribution.html#abe4946f16be5033f9692416eca2e408d", null ],
    [ "result_type", "d3/d7b/classFloatUniformSinDistribution.html#a72048f81596d9642f1be96bc8483397e", null ],
    [ "calculate", "d3/d7b/classFloatUniformSinDistribution.html#afa41ea30d569313d5a9081a936d1b17e", null ],
    [ "InputBlockSize", "d3/d7b/classFloatUniformSinDistribution.html#a1103eed852008e5c518f03656c78d1c6", null ],
    [ "OutputElementSize", "d3/d7b/classFloatUniformSinDistribution.html#aa1d975f7f8ef0733e0db067cac8d7c11", null ],
    [ "OutputsPerInputBlock", "d3/d7b/classFloatUniformSinDistribution.html#a39de89d305bc5b645d4fe78ad8b7ca6c", null ]
];