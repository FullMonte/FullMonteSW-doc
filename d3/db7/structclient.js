var structclient =
[
    [ "_next", "d3/db7/structclient.html#a1e1e515c1a550237ef872e16bebe1959", null ],
    [ "_prev", "d3/db7/structclient.html#aaf3453b9c62fa14690e71329fc4e6fea", null ],
    [ "abort", "d3/db7/structclient.html#a0b472da57b15745980a17f5af288114b", null ],
    [ "context", "d3/db7/structclient.html#a17b3252c99f2bfba1714b2e3f24781fd", null ],
    [ "fd", "d3/db7/structclient.html#aa9ee781ac58a0d10fa591a570e875473", null ],
    [ "flushing", "d3/db7/structclient.html#ad7668e81b61734dcea562635db9be320", null ],
    [ "idle_cycles", "d3/db7/structclient.html#a782720f32b1bcb04b0c74523ad89d45e", null ],
    [ "ip", "d3/db7/structclient.html#a8fc07a9bfe28b076a9c8a4d76d2a69f1", null ],
    [ "max_irqs", "d3/db7/structclient.html#ab90b9c43860748b9155d091e2701ed26", null ],
    [ "mem_access", "d3/db7/structclient.html#a14aef260ac3425eb59e345e998761f5a", null ],
    [ "mmio_access", "d3/db7/structclient.html#a954f11adaeaf351d5acb12ab4531fb03", null ],
    [ "mmio_offset", "d3/db7/structclient.html#a399e6bb918724a3e229705f0b0574c49", null ],
    [ "mmio_size", "d3/db7/structclient.html#a7eb4f78f95c7aef2b1306d6e56d3baff", null ],
    [ "pending", "d3/db7/structclient.html#a1a92201d4db40470ba4cd8d6c267c9fc", null ],
    [ "state", "d3/db7/structclient.html#afbcd337dee6f19e6079b69a71b97724b", null ],
    [ "thread", "d3/db7/structclient.html#a529fa20e309347262616a00ad0ad3d93", null ],
    [ "timeout", "d3/db7/structclient.html#af798db2c3ff70ef60d74348aad31c187", null ],
    [ "type", "d3/db7/structclient.html#a255266eb582e4fafcb8dc2049cd9fff1", null ],
    [ "wed", "d3/db7/structclient.html#a17e0b5c4264d882d3386685651316a8a", null ]
];