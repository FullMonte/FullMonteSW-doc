var classBlockRNGAdaptor =
[
    [ "result_type", "d3/df6/classBlockRNGAdaptor.html#a3936af0a907e41854471d4734f23bcf5", null ],
    [ "BlockRNGAdaptor", "d3/df6/classBlockRNGAdaptor.html#ac345096257037252312837b30f23919c", null ],
    [ "getBlock", "d3/df6/classBlockRNGAdaptor.html#a2df3193944e4df6a0712b1179faa4d20", null ],
    [ "rng", "d3/df6/classBlockRNGAdaptor.html#ace072f02e49ea33421243cb58c4f8ae0", null ],
    [ "seed", "d3/df6/classBlockRNGAdaptor.html#ac1b8fe0712f41e31e9b1be98b83e01cd", null ],
    [ "genBlockByteSize", "d3/df6/classBlockRNGAdaptor.html#a5d0b65605ca8c1ed9f2a0abcdab517fb", null ],
    [ "genBlockSize", "d3/df6/classBlockRNGAdaptor.html#a52db8c09ef6647e9170bd62637677489", null ],
    [ "m_buffer", "d3/df6/classBlockRNGAdaptor.html#a8547924ce6d95f4b7c6825ee0f8ce96d", null ],
    [ "m_pos", "d3/df6/classBlockRNGAdaptor.html#a2a52e37b719b4ac94749ca393ff713a2", null ],
    [ "m_rng", "d3/df6/classBlockRNGAdaptor.html#a8aa7a8dd585d54a170c81c7b9871fea8", null ],
    [ "returnBlockByteSize", "d3/df6/classBlockRNGAdaptor.html#a7a1564987e3500df07255711ec69b156", null ],
    [ "returnBlockSize", "d3/df6/classBlockRNGAdaptor.html#a47fb6f588d05c6e7a17976c0e355ba80", null ]
];