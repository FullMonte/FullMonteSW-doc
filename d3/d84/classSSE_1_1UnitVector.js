var classSSE_1_1UnitVector =
[
    [ "UnitVector", "d3/d84/classSSE_1_1UnitVector.html#a58e0ec8be3350b1c4fea13c4f4309a4d", null ],
    [ "UnitVector", "d3/d84/classSSE_1_1UnitVector.html#a56025fa85b494a494368f7305356ab78", null ],
    [ "UnitVector", "d3/d84/classSSE_1_1UnitVector.html#a8f7e83dd296f3c454636cc03e9aa783d", null ],
    [ "UnitVector", "d3/d84/classSSE_1_1UnitVector.html#a59d9e107a3e20c82c40d67191d60306f", null ],
    [ "UnitVector", "d3/d84/classSSE_1_1UnitVector.html#a0920ff7765ae12e5fcaea423ef99ca1e", null ],
    [ "UnitVector", "d3/d84/classSSE_1_1UnitVector.html#a5286a829bebdbe180172370c65534f84", null ],
    [ "UnitVector", "d3/d84/classSSE_1_1UnitVector.html#a435ee45d5e22a105eee8c4a10a9b3f31", null ],
    [ "basis", "d3/d84/classSSE_1_1UnitVector.html#abe144db0325c325446e8c430b6db66b1", null ],
    [ "basis", "d3/d84/classSSE_1_1UnitVector.html#aec7edb2462ac5c9d8c82305267e5352e", null ],
    [ "check", "d3/d84/classSSE_1_1UnitVector.html#a97530af385ef85473ea87c2ee45ae21a", null ],
    [ "normalize", "d3/d84/classSSE_1_1UnitVector.html#a48a980e045c18cec56fe7e56a6b1c3ca", null ],
    [ "normalize_approx", "d3/d84/classSSE_1_1UnitVector.html#ad5c8556aef452b729ece2c9718ef937a", null ]
];